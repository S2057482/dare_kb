#!/bin/sh

cd /opt/dare_kb/dare_kb

DATA_DIR='/opt/dare_kb/data'

cp DareKnowledgeBase.owl $DATA_DIR/

pipenv run dkb_manage init -d $DATA_DIR ex3KB &&
pipenv run dkb_server --host='0.0.0.0' -d $DATA_DIR ex3KB

