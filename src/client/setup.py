from setuptools import setup, find_packages

setup(
    name = "DKBlib",
    description = "Client library for DARE Knowledge Base",

    packages = find_packages(),
    include_package_data = True,
    platforms = "any",
    install_requires = [
        "requests",
        ],

    scripts = [],
    entry_points = {
        'console_scripts': [
        ]
    }
)
