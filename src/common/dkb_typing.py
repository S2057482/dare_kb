'''
This file contains the extra type hints, in addition to the standard typing module.
You may need to `import typing` as well.
'''

from typing import Dict

PID = str

ContextComm = Dict

ConceptComm = Dict
