# import DKB_functions as DKBm  # Use csbridge to replace direct calls
import DKBlib as DKB
import re
#import inst_mixer

proj_concept = {
    'name': lambda concept : concept.label,
    'prefix': lambda concept : concept.prefix,
    'pid': lambda concept : concept.identifier,
    'specialises': lambda concept : concept.sub_class,
    'description': lambda concept : concept.description,
    'state': lambda concept : concept.state,
    'timestamp': lambda concept : format.timestamp_serialise(concept.timestamp),
    'mutability': lambda concept : concept.mutability,
    'translation': lambda concept : concept.translation,
    'py_class': lambda concept : concept.py_class,
    'methods': lambda concept : concept.methods,
}

proj_instance = {
    'pid': lambda inst: inst.pid,
    'prefix': lambda inst: inst.prefix,
    'name': lambda inst: inst.name,
    'state': lambda inst: inst.state,
    'mutability': lambda inst: inst.mutability,
    'timestamp': lambda inst: format.timestamp_serialise(inst.timestamp)
    }
    
    
def main():
    
    dkb = DKB.login('ex3KB', 'al')
 
    c = dkb.new_context('formyconcept', 'A context to build my concepts')
    dkb.enter(c, 'W')
    
#    print(dkb.context_status())
    
    ## Now every concept created are created within formyconcept context
    cp1 = dkb.new_concept(precise_term = 'FirstOne')
    cp2 = dkb.new_concept(precise_term = 'ASpeOfFirstOne', specialises = cp1) ## Specialise by PID
    cp2 = dkb.new_concept(precise_term = 'ASecondSpe', specialises = 'FirstOne') ## You can specialise by name only
    cp3 = dkb.new_concept(precise_term = 'FirstWithAttribute', required = {'prop1': 'String'}) ## Property prop1 is a DataProperty
    cp4 = dkb.new_concept(precise_term = 'SecWithAtt', recommended = {'prop2': cp3}) ## Property prop2 is a ObjectProperty (defined by a PID, but can also be a name)
    cp5 = dkb.new_concept(precise_term = 'ConceptWithOptionalAtt', optional = {'prop3': 'Integer'})
    cp6 = dkb.new_concept(precise_term = 'ConceptWithTranslation', translation = {'name': 'skos:label'})
    cp7 = dkb.new_concept(precise_term = 'CWithDescription', description = 'This is a concept with a description')
    cp8 = dkb.new_concept(precise_term = 'CWithMethods', methods = {'OneMethod': 'thisOne'})
    cp9 = dkb.new_concept(precise_term = 'CWithPy_class', py_class = 'a_class.py')
    
#    print(dkb.context_status())
    
    dkb.close()

    dkb = DKB.login('ex3KB', 'al')
    ## Should be retrieving previous context ie: formyconcept
#    print(dkb.context_status())
    ## Create a new context for instance with formyconcept in its search_path
    c1 = dkb.new_context('instancesContext', 'A context for the instances', search_path = ['formyconcept'])
    
    dkb.enter('instancesContext', 'W')
    ins1 = dkb.new_instance('FirstOne', 'First_Instance')
    ins2 = dkb.new_instance('ASpeOfFirstOne', 'Instance_of_Snd')
    ins3 = dkb.new_instance('FirstWithAttribute', 'Instance_for_att', prop1 = 'la prop') ## prop1 is required
    ins4 = dkb.new_instance('FirstWithAttribute', 'Instance_for_att_2', prop1 = '') ## but can be fill with ''
    ins5 = dkb.new_instance('SecWithAtt', 'Instance_for_att_3', prop2 = ins4) ## prop2 can be filled or ommitted
    ins6 = dkb.new_instance('SecWithAtt', 'Instance_for_att_missing')
    ins7 = dkb.new_instance('ConceptWithOptionalAtt', 'Instance_for_opt')
    ins8 = dkb.new_instance('ConceptWithOptionalAtt', 'Instance_for_opt_2', prop3 = 3)
    
#    print(dkb.context_status())
    
    dkb.close()
    
    dkb = DKB.login('ex3KB', 'al')
    ## Should be retrieving previous context ie: instancesContext
    print(dkb.context_status(), '\n')
    
    c = dkb.new_concept('NewOne', description = 'c is the PID')
    
    print(dkb.get('First_Instance'), '\n') ## get of an Instance by name
    print(dkb.get('FirstOne'), '\n') ## get of a Concept by name
    print(dkb.get('FirstWithAttribute', only_these = ['pid', 'name']), '\n')
    print(dkb.get('FirstWithAttribute', only_these = []), '\n')
    
    print(dkb.get('instancesContext:Instance_for_opt'), '\n') ## context:name
    print(dkb.get('formyconcept:ConceptWithOptionalAtt'), '\n')
    print(dkb.get(c)) ## get by pid
    
    ## Let us discard the instancesContext
    dkb.context_reset()
    
    ## now in kb
    print(dkb.context_status(), '\n')
    try:
        print(dkb.get('instancesContext:Instance_for_att_missing'), '\n')
    except:
        print('This instance does not exist\n')
    print(dkb.get('instancesContext:Instance_for_att_missing', ignore_discarded = True))
    
    dkb.close()
    
    dkb = DKB.login('ex3KB', 'al')
    
    ## search for all instances/concepts in the context instancesContext
    q = ('==', 'prefix', 'instancesContext')
    res = dkb.find(q)
    print(res)
                                            
                                            
                                            


    

if __name__ == "__main__":
    main()
