from datetime import datetime

from . import setting
from .internal import (
        datatype as dt,
        name_spec,
        )
from .instance import Instance
from .internal.consts import instance_state_list
from .internal.consts import property_types
from .property import MyDataProperty, MyObjectProperty

from ..common import Exceptions as E
from ..common.dkb_typing import PID

from typing import Union


class Concept:

    def __init__(self, context: str, name, sub_class, mutab, state, required={}, recommended={}, optional={}, translation={}, identifier=None, timestamp: datetime=None, description='', methods={}, py_class=None):
        self.label = name
        self.sub_class = sub_class
        self.prefix = context
        self.methods = methods
        self.py_class = py_class
        self.description = description
        self.identifier = identifier
        self.mutability = mutab
        self.state = state or instance_state_list[0]
        self.timestamp = timestamp or datetime.now()

        # Properties
        ## For faster lookup when creating owl Concept
        self.object_properties = {} ## dictionnary of the new properties between Object
        self.data_properties = {} ## dictionnary of the new properties linked to data

        ## All properties used during runtime lookup
        self.properties = {}

        self.translation = translation
#        self.list_of_instances = {}  # name : PID

    def __str__(self):
        return f'Concept \n identif {self.identifier}\n label ({self.label} (subclass-of {self.sub_class}))\n in {self.prefix}\n mutab {self.mutability}\n state {self.state}\n timestamp {self.timestamp}\n {self.description}, {self.my_concept}'

    def populate_inherited_properties(self, parent):
        self.properties['indirect'] = {
                'required': parent.get_properties('required', direct_only=False),
                'recommended': parent.get_properties('recommended', direct_only=False),
                'optional': parent.get_properties('optional', direct_only=False),
                }

    def get_properties(self, p_type=property_types, direct_only=True):
        if not isinstance(p_type, set):
            p_type = {p_type}

        res = {}
        for pt in property_types:
            if pt in p_type:
                res.update(self.properties['direct'][pt])
        if not direct_only:
            for pt in property_types:
                if pt in p_type:
                    res.update(self.properties['indirect'][pt])
        return res

    def add_dproperties(self, name, dp):
        self.data_properties[name] = dp
    
    def add_oproperties(self, name, op):
        self.object_properties[name] = op
        
    def add_properties(self, prop):
        self.properties = prop
    
    def new_instance(self, context: str, name:str , pid:PID, **kwargs):
        ## need to verify that kwargs are at least the same amount as required properties
        inst = Instance(name, context, pid, self.identifier, None, None, None, **kwargs)
        return inst

    def reset(self):
        self.state = instance_state_list[4]
        
    def update_properties(self, w, dict_prop):
        direct = self.properties['direct'][w]
        for p in dict_prop:
            print("new prop?", p)
        
    def has_properties(self, prop):
        direct = self.properties['direct']
        for type in direct:
            for p in direct[type]:
                if prop == p:
                    return direct[type][p]
        return None
    
    def update_property(self, prop, new_range):
        if dt.has_str(new_range):
            prop.range = dt.from_str(new_range)
        else:
            pass
            
    def update_instance(self, inst: Instance, **kwargs):
        ## check les prop
        direct_ = self.properties['direct']
        has_been_up = {}
        values_ = []
        for prop in direct_:
            for ep in direct_[prop]:
                if (ep in kwargs):
                    values_.append(ep)
                    new_value = kwargs[ep]
                    la_prop = direct_[prop][ep]
                    if isinstance(la_prop,MyDataProperty):
                        le_type = la_prop.range
                        if (isinstance(new_value, le_type)):
                            inst.update_value(ep, new_value)
                            has_been_up[ep] = new_value
                        else:
                            raise E.PropertyRangeNotMatchingTypeError(la_prop.name, le_type, type(new_value))
                    else:
                        le_concept = la_prop.range
                        inst.update_value(ep, new_value)
                        has_been_up[ep] = (le_concept,new_value)
        inst.state = 'updated'
        if 'mutability' in kwargs:
            values_.append('mutability')
            inst.update_mutab(kwargs['mutability'])
        if len(kwargs) != len(values_):
            p = [i for i in list(kwargs.keys()) + values_ if i not in list(kwargs.keys()) or i not in values_]
            raise E.NamePropertyDoesNotExistError(p)
#        print(has_been_up)
        return has_been_up
            

    def update(self, **kwargs):
        modif_prop = []
        for key in kwargs:
            if (key in ['required', 'recommended', 'optional']):
                print("New prop to add")
                self.update_properties(key, kwargs[key])
            elif (key in ['py_class', 'description', 'mutability']):
                setattr(self, key, kwargs[key])
            elif key == 'translation':
                print('update translation')
            elif key == 'methods':
                print('update methods')
            else:
                prop = self.has_properties(key)
                if (prop is not None):
                    print("Knwon property to update")
                    modif_prop.append(self.update_property(prop, kwargs[key]))
                else:
                    print("Does not exist")
        return modif_prop
