import functools

import warnings
from datetime import datetime
from owlready2 import *

from ..common.Exceptions import *
from . import setting
from .internal import (
        name_spec,
        datatype as dt,
        pid as pid_util,
        )
from .internal.consts import context_state_list
from .internal.consts import instance_state_list, proj_concept, proj_instance

from .internal.inst_mixer import MixType
from .Profile import *
from .MyContext import *
from .Concept import *
from .instance import Instance
from .storage import Storage

from typing import Dict, Iterable, Tuple, Optional
from ..common.dkb_typing import PID
from dare_kb.common import format

import logging
logger = logging.getLogger('DareKB')

# print("File 'DareKB.py'.")

class UserHandle:

    def __require_writting(f):
        '''
        Indicates and checks the function {f} requires the 'W' option
        '''
        @functools.wraps(f)
        def wrapper(self, *args, **kwargs):
            if self.current_context_prefix() != 'kb' and self.current_context().mode != 'W':
                raise WritingPermissionDeniedError(f.__name__)
            ret = f(self, *args, **kwargs)
            return ret
        return wrapper

    def __kb_context_prevention(f):
        '''
        Checks the fucntion {f} is not in 'kb' context
        '''
        @functools.wraps(f)
        def wrapper(self, *args, **kwargs):
            if self.current_context_prefix() == 'kb':
                raise WritingPermissionDeniedError("kb context does not allowed this action to be performed: "+str(f.__name__))
            ret = f(self, *args, **kwargs)
            return ret
        return wrapper

    def __kb_active_discard(f):
        '''
        Checks the fucntion {f} is not in 'kb' context
        '''
        @functools.wraps(f)
        def wrapper(self, *args, **kwargs):
            if self.current_context().state == context_state_list[4]:
                self.current_context().state = context_state_list[1]
                self.dkb._storage.update_state(self.current_context())
            ret = f(self, *args, **kwargs)
            return ret
        return wrapper

    def __init__(self, dkb, session, infos):
        # print("File 'DareKB.py', Class 'UserHandle'.")
        self.dkb = dkb
        self.user = session.user
        self.session_id = session.session_id
        self._home = self.dkb.get_last_context(self.user)  # home (default) context for the user
        if self.current_context_prefix() != self.dkb.default_context():
            self.current_context().add_user(self.user)
            self.dkb._storage.update_users(self.current_context())
            self.dkb._storage.update_mode(self.current_context())
        ## create session
        self.dkb._storage.register_session(session)

    def close(self):
        if self.current_context_prefix() != self.dkb.default_context():
            self.dkb.set_last_context(self.user, self.current_context_prefix(), self.current_context().mode)

            self.current_context().remove_user(self.user)
            self.dkb._storage.update_users(self.current_context())

            if (self.current_context().c_users == []):
                self.current_context().mode = None
                self.dkb._storage.update_mode(self.current_context())
        self.dkb.close_handle(self.session_id)

    def status(self):
        kb_status = {}
        kb_status["session_id"] = self.session_id
        kb_status["current_context"] = self.current_context_prefix()
        kb_status["username"] = self.user
        kb_status.update(self.dkb.status())
        return kb_status

    def context_status(self, prefix=None):
        prefix = prefix or self.current_context_prefix()
        return self.dkb.context_status(prefix)

    def current_context(self):
        # FIXME: what if _home is None?
        if self._home is None:
            self._home = self.dkb.get_context(self.dkb.default_context())
        return self._home

    def current_context_prefix(self):
        return self.current_context().prefix

    @__kb_active_discard
    def new_context(self, prefix: str, title: Optional[str] = None, search_path: Optional[List[str]] = None, owner: Optional[str] = None) -> str:
        #### Verify the regex of the prefix and title
        if owner is None:
            owner = self.user
        if ((not name_spec.my_regex_for_identifiers(prefix)) or prefix == ""):
            raise IdentifierWrongError(prefix, "new_context")
        if title is None:
            title = "Context "+str(prefix)+" for "+str(owner)+" created in session: "+str(self.session_id)+" at "+format.timestamp_serialise(datetime.now())
        else:
            if (title == ""):
                raise IdentifierWrongError(title, "new_context")
        if search_path is None:
            search_path = [self.current_context_prefix()]

        return self.dkb.new_context(self.session_id, prefix, title, search_path, owner)

#    def new_user(self, username, prefer_context, credentials, email):
#        return self.dkb.new_user(username, prefer_context, credentials, email)

    @__kb_context_prevention
    @__require_writting
    def reset_context(self, prefix: str = None) -> None:
        prefix = prefix or self.current_context_prefix()
        self.dkb.reset_context(prefix)
#        self.dkb._storage.set_last_context(self.user, None, None)
#        self._home = None

    @__kb_context_prevention
    @__require_writting
    def freeze_context(self, prefix: str = None) -> None:
        prefix = prefix or self.current_context_prefix()
        self.dkb.freeze_context(prefix)

    @__kb_context_prevention
    @__require_writting
    def deprecate_context(self, prefix: str = None) -> None:
        prefix = prefix or self.current_context_prefix()
        self.dkb.deprecate_context(prefix)

    @__require_writting
    @__kb_active_discard
    def new_concept(self, *args, **kwargs):
        return self.dkb.new_concept(self.session_id, self.current_context_prefix(), *args, **kwargs)

    def get_search_path(self):
        return self.dkb.get_search_path(self.current_context_prefix())

    @__kb_context_prevention
    @__require_writting
    @__kb_active_discard
    def set_search_path(self, new_search_path):
        self.dkb.set_search_path(self.current_context_prefix(), new_search_path)

    def enter(self, prefix, rw):
        if (not isinstance(prefix, str)):
            raise TypeError("The specified context should be a String. Argument was of type ", type(prefix))
        if (rw != 'R' and rw != 'W'):
            raise TypeError("Reading or writing option is not one the two following tokens 'R' or 'W'")

        self._home, ret = self.dkb.on_open_context(self.user, prefix, rw, self.current_context())
        logger.info("Context is now set for "+self._home.title+" with prefix "+self._home.prefix+" in "+self._home.mode+" mode.")
        return ret

    def leave(self):
        if self.current_context_prefix() != self.dkb.default_context():
            self.current_context().leave_context(self.user)
            self.dkb._storage.update_users(self.current_context())
            self.dkb._storage.update_mode(self.current_context())
        self._home = None


    @__require_writting
    @__kb_active_discard
    def new_instance(self, *args, **kwargs):
        return self.dkb.new_instance(self.session_id, self.current_context_prefix(), *args, **kwargs)

    @__kb_context_prevention
    @__require_writting
    def update(self, id, **kwargs):
        return self.dkb.update(self.current_context_prefix(), id, **kwargs)

    def get(self, identity, ignore_discarded):
        return self.dkb.get(self.current_context_prefix(), identity, ignore_discarded)

    def find(self, query, ignore_discarded):
        return self.dkb.find2(self.current_context_prefix(), query, ignore_discarded)


class DareKB:
    username = None
    ontology_file = ""
    state = True
    _namespace = None

    def __require_open_dkb(f):
        '''
        Indicates and checks the function {f} requires a open DKB session
        '''
        @functools.wraps(f)
        def wrapper(self, *args, **kwargs):
            if (self.state == False):
                raise DKBclosedError(f.__name__)
            ret = f(self, *args, **kwargs)
            return ret
        return wrapper


    def __init__(self, site_name=None, base_dir='', database_path=None, base_ontology_file=None):
        # print("File 'DareKB.py', Class 'DareKB'.")
        self._storage = Storage(self, base_dir, base_ontology_file, database_path)
        self._sessions = {}
        self.site_identifier = site_name
        self.python_contexts = {}
        self.pid_concepts = {}
        self.pid_instances = {}
        self._default_context = 'kb'

    def __del__(self):
        if self.state:
            self.shutdown()

    def shutdown(self):
        handles = list(self._sessions.values())
        for handle in handles:
            handle.close()
        self._storage.shutdown()
        logger.info("The DKB is now closed")

    def _add_default_context(self):  # TODO: replace with proper initialisation (through OWL file?)?
        '''
        Initialise a default Context (used when creating the DKB)
        '''
        c = self._new_context(self.default_context(), "knowledge base", [], "root", None)
        c1 = self._new_context('dare', 'the dare context', ['kb'], "root", None)
        c2 = self._new_context('seismo', 'specific to seismo community', ['dare'], "root", None)
        c.activate_context()
        c1.activate_context()
        c2.activate_context()
        self._storage.update_state(c)
        self._storage.update_state(c1)
        self._storage.update_state(c2)

    def default_context(self) -> str:
        return self._default_context

    def new_handle(self, session, infos):
        handle = UserHandle(self, session, infos)
        self._sessions[session.session_id] = handle
        return handle

    def get_handle(self, session_id):
        return self._sessions[session_id]

    def close_handle(self, session_id):
#        last_context = self.get_last_context(self.username)
#        if last_context is not None:
#             self._storage.set_last_context(last_context)
        del self._sessions[session_id]
        self._storage.finish_session(session_id)
        self._storage.flush()

    @__require_open_dkb
    def status(self):
        kb_status = {}
        kb_status["site_name"] = self.site_identifier
        kb_status["contexts_available"] = self._storage.context_list()
        return kb_status

    @__require_open_dkb
    def context_status(self, prefix):
        conc = self._storage.get_concepts_for_context(prefix)
        inst = self._storage.get_instances_for_context(prefix)
        return self.get_context(prefix).context_status(conc, inst)

### Context functions ###
    def _load_python_context_from_storage(self, prefix: str):
        new_c_kb = self._storage.get_context(prefix)
        self._cache_context(new_c_kb)

    def _context_loaded(self, prefix):
        return prefix in self.python_contexts

    def _cache_context(self, context: MyContext):
        self.python_contexts[context.prefix] = context

    @__require_open_dkb
    def new_context(self, session_id: str, prefix: str, title: str, search_path: List[str], owner: str = None):

        if self._storage.has_context(prefix):
            raise ExistingContextError(prefix, "new_context")

        for c in search_path:
            if not self._storage.has_context(c):
                raise ContextNotFoundError(c, "new_context")
            else:
                if not self._context_loaded(c):
                    self._load_python_context_from_storage(c)
        c = self._new_context(prefix, title, search_path, owner, session_id)
        return c.prefix

    @__require_open_dkb
    def new_user(self, **infos):
        ## Here we create a context for username, we create a user with the infos
        username = infos['username']
        context_prefix = username+'_init'
        c = self.new_context(None, context_prefix, title = 'Initial context for user '+username, search_path = ['dare'], owner = username)
        infos['prefer_context'] = c
        infos['mode'] = 'W'
        return self._storage.new_user(**infos)

    def get_user(self, username):
        return self._storage.get_user(username)

    def _new_context(self, prefix: str, title: str, search_path: List[str], owner: str, session_id: str) -> MyContext:
        '''
        (Internal) Create the Context object (Python) and individual (OWL)
        '''
        identifier = self.site_identifier + ":" + prefix
        c = MyContext(prefix, title, search_path, owner, identifier = identifier)
        self._storage.on_new_context(c, session_id)
        self._cache_context(c)
        return c

    def get_context(self, prefix: str) -> MyContext:
        if not self._context_loaded(prefix):
            self._load_python_context_from_storage(prefix)
        return self.python_contexts[prefix]

    def reset_context(self, cx_prefix: str) -> None:
        context = self.get_context(cx_prefix)
        _concepts = context.get_my_concepts()
        for pid in _concepts.values():
            _the_concept = self.get_concept(pid, False)
            _the_concept.reset()
        _instances = context.get_my_instances()
        for pid in _instances.values():
            _the_instance = self.get_instance(pid, False)
            _the_instance.reset()
        context.reset()
        self._storage.update_state(context)
        self._storage.discard_context(cx_prefix)

    def deprecate_context(self, cx_prefix: str = None) -> None:
        context = self.get_context(cx_prefix)
        context.deprecate()
        self._storage.update_state(context)

    def freeze_context(self, cx_prefix:str = None) -> None:
        context = self.get_context(cx_prefix)
        context.freeze()
        self._storage.update_state(context)
        self._storage.update_mode(context)

    @__require_open_dkb
    def get_search_path(self, prefix: str) -> List[str]:
        return self.get_context(prefix).get_search_path()

    @__require_open_dkb
    def set_search_path(self, prefix: str, new_search_path: List[str]):
        for c in new_search_path:
            if not self._storage.has_context(c):
                raise ContextNotFoundError(c, "set_search_path")
            if (not self._context_loaded(c)):
                self._load_python_context_from_storage(c)
        self.get_context(prefix).set_search_path(new_search_path)
        self._storage.update_search_path(prefix, new_search_path)

    @__require_open_dkb
    def on_open_context(self, user: str, a_prefix: str, rw: str, curr: MyContext) -> MyContext:
        if not self._storage.has_context(a_prefix):
            raise ContextNotFoundError(a_prefix, "on_open_context")

        context_new = self.get_context(a_prefix)
        if (context_new.state == context_state_list[2] and rw == 'W'):
            raise WritingPermissionDeniedError("This context is frozen, you are not permitted to enter for writting try enter(_,'R').")

        if (context_new != curr):
            if ((context_new.mode == 'W') and (rw == 'R')):
                raise MultiUserError(rw, a_prefix, context_new.c_users, context_new.mode)

            if ((context_new.mode == 'R' or context_new.mode == 'W') and (rw == 'W')):
                raise MultiUserError(rw, a_prefix, context_new.c_users, context_new.mode)

        if curr.prefix != self.default_context():
            self.on_close_context(user, curr, a_prefix)

        b, t = context_new.throw_warning()
        if b:
            to_return = t
        else:
            to_return = ""

        context_new.activate_context()
        context_new.enter_context(rw, user)

        self._storage.update_users(context_new)
        self._storage.update_mode(context_new)
        self._storage.update_state(context_new)

        return context_new, to_return

    def on_close_context(self, user: str, context: MyContext, a_prefix: str) -> None:
        if not self._storage.has_context(a_prefix):
            raise ContextNotFoundError(a_prefix, "on_close_context")
        context.leave_context(user)
        self._storage.update_users(context)
        self._storage.update_mode(context)

    @__require_open_dkb
    def get_last_context(self, user: str) -> MyContext:
        r = self._storage.get_last_context(user)
        if r is not None:
            if not self._context_loaded(r[0]):
                self._load_python_context_from_storage(r[0])
            self.python_contexts[r[0]].mode = r[1]
            return self.python_contexts[r[0]]
        return None

    @__require_open_dkb
    def set_last_context(self, user: str, a_prefix: Optional[str], mode: str):
        if a_prefix:
            if not self._storage.has_context(a_prefix):
                raise ContextNotFoundError(a_prefix, "enter")
            ### Find the new context to enter
            if (not self._context_loaded(a_prefix)):
                ### We add it to the python_context
                self._load_python_context_from_storage(a_prefix)
        self._storage.set_last_context(user, a_prefix, mode)

### Concept functions ###
    def _load_python_concept_from_storage(self, pid: str):
        concept_kb = self._storage.get_concept_by_pid(pid)
        self._cache_concept(concept_kb)

    def _concept_loaded_pid(self, pid):
        return pid in self.pid_concepts.keys()

    def _cache_concept(self, concept: Concept):
        self.pid_concepts[concept.identifier] = concept

    def id_for_root_concept(self) -> PID:
        return self._storage.id_for_root_concept()

    def _has_concept(self, prefix, precise_term) -> bool:
        return self._storage.has_concept(prefix, precise_term)

    def verify_name_is_concept(self, pid, context) :
        return self.resolve(pid, context, type=MixType.CONCEPT)

    @__require_open_dkb
    def new_concept(self, session_id, prefix, precise_term, specialises = None, mutability = "mutable", required = dict(), recommended = dict(), optional = dict(), translation = dict(), description = None, methods = dict(), py_class = None) -> PID:

        def handle_properties(context, dic, p_type, conc):
            properties = {}
            for name, p_range in dic.items():
                if (not name_spec.my_regex_for_identifiers(name) or name == ""):
                    raise E.IdentifierWrongError(name, "Properties")
                if (self._storage.prop_existed(name)):
                    print("property already exist", name)
                if dt.has_str(p_range):
                    dp = MyDataProperty(name, conc.identifier, dt.from_str(p_range), True, p_type)
                    conc.add_dproperties(name, dp)
                elif (p_range == precise_term):
                    dp = MyObjectProperty(name, conc.identifier, conc.identifier, True, p_type)
                    conc.add_oproperties(name, dp)
                elif (self.verify_name_is_concept(p_range, context)):
                    pid = self.resolve(p_range, context, type=MixType.CONCEPT)
                    dp = MyObjectProperty(name, conc.identifier, pid, True, p_type)
                    conc.add_oproperties(name, dp)
                else:
                    raise E.NotAConceptError(name, "new_concept")
                properties[name] = dp
            return properties

        if (not name_spec.my_regex_for_identifiers(precise_term) or precise_term == ""):
            raise IdentifierWrongError(precise_term, "new_concept")

        context = self.get_context(prefix)
        if self._has_concept(prefix, precise_term):
            raise NameInUseError(precise_term, context.get_concept(precise_term), "new_concept")

        if specialises is None:
            pid_parent = self._storage.id_for_root_concept()
        else:
            pid_parent = self.resolve(specialises, prefix, type=MixType.CONCEPT)

        c = context.new_concept(precise_term, pid_parent, mutability, required, recommended, optional, translation, description, methods, py_class)

        c_properties = {
                'direct': {
                    'required': handle_properties(context, required, 'required', c),
                    'recommended': handle_properties(context, recommended, 'recommended', c),
                    'optional': handle_properties(context, optional, 'optional', c),
                    },
                'indirect': {
                    'required': {},
                    'recommended': {},
                    'optional': {},
                    }
                }
        c.add_properties(c_properties)
        if (c.sub_class != self.id_for_root_concept()):
            parent = self.get_concept(c.sub_class, False)
            c.populate_inherited_properties(parent)

        self._cache_concept(c)

        self._storage.update_last_marker(context.prefix, context.last_marker)
        self._storage.on_new_concept(c, session_id)
        return c.identifier


### Instance functions

    def _load_python_instace_from_storage(self, pid: PID):
        # inst_kb = self._storage.get_instance(pid)         ## Error: 'TypeError: ("get_instance() missing 1 required positional argument: 'name'", []),'
                                                            ##         when using the 'get_instance' method with PID
        inst_kb = self._storage.get_instance_by_pid(pid)    ## Correction: New method 'get_instance_by_pid'
        self._cache_instance(inst_kb)

    def _instance_loaded_pid(self, pid):
        return pid in self.pid_instances.keys()

    def _cache_instance(self, inst: Instance):
        self.pid_instances[inst.pid] = inst

    @__require_open_dkb
    def new_instance(self, session_id, prefix: str, cls: str, name: str, **kwargs):
        if (not name_spec.my_regex_for_identifiers(name) or name == ""):
            raise IdentifierWrongError(name, "new_instance")

        if self._storage.has_instance(prefix, name):
            raise NameInUseError(name, name, 'new_instance')

        ## Find the context
        c = self.get_context(prefix)
        ## Find the concept
        pid = self.resolve(cls, prefix, type=MixType.CONCEPT)
        conc = self.get_concept(pid, False)

        inst = c.new_instance(conc, name, **kwargs)
        self._cache_instance(inst)

        self._storage.update_last_marker(c.prefix, c.last_marker)
        self._storage.on_new_instance(inst, session_id)
        return inst.pid

### get and find and update functions
    @__require_open_dkb
    def get_concept(self, pid: PID, ignore_discarded) -> Concept:
        if (not self._concept_loaded_pid(pid)):
            self._load_python_concept_from_storage(pid)
        # conc = self.pid_concepts[pid]                     ## OLD code

        ## NEW code beginning: Resolving the get() concept issue (Part 1, due to AttributeError: 'strftime' is not a language code)
            conc = self.pid_concepts[pid]
            # Retrieved timestamp is a list after the concept has been flushed to the DB
            # Thus, additional check whether timestamp is of type 'IndividualValueList'
            if isinstance(conc.timestamp, owlready2.prop.IndividualValueList):
                # In this case: Take list element
                conc.timestamp = conc.timestamp[0]
        # Issue does not occur if concept is not loaded from the DB, thus code as before
        else: conc = self.pid_concepts[pid]
        ## NEW code end

        if (conc.state == instance_state_list[4] and not ignore_discarded):
            raise InstanceNotFoundError(pid, 'get')
        return conc

    @__require_open_dkb
    def get_instance(self, pid: PID, ignore_discarded) -> Instance:
        if (not self._instance_loaded_pid(pid)):
            self._load_python_instace_from_storage(pid)
        inst = self.pid_instances[pid]
        if (inst.state == instance_state_list[4] and not ignore_discarded):
            raise InstanceNotFoundError(pid, 'get')
        return inst

    @__require_open_dkb
    def get(self, base_prefix, identity, ignore_discarded):
        try:
            pid, type = self.resolve(identity, base=base_prefix)
            if type == MixType.CONCEPT:
                return self.get_concept(pid, ignore_discarded)
            else:
                return self.get_instance(pid, ignore_discarded)
        except InstanceNotFoundError:
            raise InstanceNotFoundError(identity, 'get')

    @__require_open_dkb
    def update(self, base_prefix, name, **kwargs):
        to_be_updated = self.get(base_prefix, name, False)
        if (isinstance(to_be_updated, Concept)):
            pid = to_be_updated.identifier
            prop = to_be_updated.update(**kwargs)
            pass
        else:
            context_ = self.get_context(base_prefix)
            predec_pid = to_be_updated.pid
            concept_of_inst = self.get_concept(to_be_updated.cls, False)
            inst = context_.new_instance(concept_of_inst, to_be_updated.name, **to_be_updated.extras)
            inst.add_prede(predec_pid)
            self._cache_instance(inst)

            self._storage.update_last_marker(context_.prefix, context_.last_marker)
            self._storage.on_new_instance(inst)

            to_be_updated.add_succ(inst.pid)
            prop = concept_of_inst.update_instance(inst, **kwargs)
        ## don't forget to store
        self._storage.update(inst, prop, to_be_updated)
        return inst.pid

    @__require_open_dkb
    def find2(self, base_prefix: str, query, ignore_discarded):

        def give_rep(ope):

            def find_if_instance_or_concept_or_both(prop):
                ## Return the type MixType.INSTANCE CONCEPT or BOTH
                if (prop in proj_concept):
                    if (prop in proj_instance):
                        type = MixType.BOTH
                    else:
                        type = MixType.CONCEPT
                elif (prop in proj_instance):
                    type = MixType.INSTANCE
                else:
                 ## it is a property
                    type = MixType.BOTH
                return type

            operand = ope[0]
            prop = ope[1]

            ret = set({})
            if (operand == 'isa'):
                concept_pid = self.resolve(prop, base_prefix, type=MixType.CONCEPT)
                pids = self._storage.find_isa(concept_pid)
                for pid in pids:
                    ret.add(pid)
                return ret
            elif (operand == 'isa_exactly'):
                concept_pid = self.resolve(prop, base_prefix, type=MixType.CONCEPT)
                pids = self._storage.find_isa_exactly(concept_pid)
                for pid in pids:
                    ret.add(pid)
                return ret
            elif (operand == '=='):
                value = ope[2]
                type = find_if_instance_or_concept_or_both(prop)
                pids = self._storage.find_tuple(ope, type)
                for pid in pids:
                    ret.add(pid)
                return ret
            elif (operand == '!='):
                # Use 'find_if_instance_or_concept_or_both' function to check whether attribute belongs to Concepts, Instances or both
                type = find_if_instance_or_concept_or_both(prop)
                # Find the PIDs of those entries, which equal the unequality criteria
                equal_pids = self._storage.find_tuple(ope, type)
                # Create a list of all available PIDs (only Concepts and Instances have a context prefix;
                # Thus, search with PID and prefix to avoid User or Session results e.g.)
                all_pids = self._storage.find_all_tuples(type, 'pid', 'prefix')
                # all_pids = self._storage.find_all_tuples(type)
                # Iterate through all available PIDs
                for pid in all_pids:
                    # Check whether the PID does not match the unequality criteria
                    if pid not in equal_pids:
                        # If not, add the PID to the result list
                        ret.add(pid)
                return ret
            # Check for range (greater/ smaller than) operators
            elif ( (operand == '>=') | (operand == '=>') | (operand == '>') | (operand == '<=') | (operand == '=<') | (operand == '<') ):
                result_set = set({})
                # Correct user inputs if necessary
                if (ope[0] == '=>'):
                    ope[0] = '>='
                elif (ope[0] == '=<'):
                    ope[0] = '<='
                # Use the 'find_if_instance_or_concept_or_both' function to check whether the property occurs solely in
                # concepts, instances or both
                type = find_if_instance_or_concept_or_both(prop)
                # Call the storage.py function 'find_tuples_inRange' to retrieve the PIDs fulfilling the query criteria
                range_pids = self._storage.find_tuples_inRange(ope, type)

                # Iterate through all selected PIDs
                for pid in range_pids:
                    # And add them to the result list
                    result_set.add(pid)

                return result_set

            else:
                print("Again oops")

        def handle_query(query, result):
            def is_tuple(query):
                # return query[0] in ['isa', 'isa_exactly', '==']
                return query[0] in ['isa', 'isa_exactly', '==', '!=', '>=', '=>', '<=', '=<', '>', '<']
            if (is_tuple(query)):
                return give_rep(query)
            else:
                if (query[0] == 'AND'):
                    list_of_next_query = query[1]
                    for ind in range(len(list_of_next_query)):
                        if ind == 0:
                            result = result.union(handle_query(list_of_next_query[ind], result))
                        else:
#                            test =
                            result = result.intersection(handle_query(list_of_next_query[ind], result))
                    return result
                elif (query[0] == 'OR'):
                    list_of_next_query = query[1]
                    for q in list_of_next_query:
                        result = result.union(handle_query(q, result))
                    return result
                elif (query[0] == 'NOT'):
                    my_query = query[1]
                    return result
                else:
                    print("OOPS")
                    return
                    ## Unknown operation?
        res = set({})
        res = handle_query(query, res)

        python_res = []
        for r in res:
            r_ = self.get(None, r, ignore_discarded=True)
            if r_.state != instance_state_list[4] or ignore_discarded:
                python_res.append(self.get(None, r, ignore_discarded))
        return python_res

    def resolve(self, e_id, base=None, type=MixType.BOTH) -> Union[PID, Tuple[PID, str]]:
        '''
        This function implements the Entry search algorithm.
        Currently, only Concept exists (as subclass of Entry), so this function only looks for Concept.

        @param e_id: The Entry ID
        @param base: Optional context (prefix) to start the resolve if e_id does not have a prefix
        @param type: Optionally specify the type (Concept or Instance) to look up, represented using the MixType enum
        @return: The full PID of the found Entry if `type` is specified, or (PID, type)
        @throws ConceptNotFoundError: When the Concept can't be found and `type == MixType.CONCEPT`
        @throws InstanceNotFoundError: When the Instance can't be found and `type == MixType.INSTANCE` or `type == MixType.BOTH`
        '''
        def look_in_context(prefix, name):
            if type.has_concept():
                context = self.get_context(prefix)
                pid = context.get_concept(name)
                if pid is None:
                    if not type.has_instance():
                        raise ConceptNotFoundError(f'{prefix}:{name}', 'look_in_context')
                else:
                    return type.with_type_if_needed(pid, MixType.CONCEPT)

            if type.has_instance():
                context = self.get_context(prefix)
                pid = context.get_instance(name)
                if pid is None:
                    raise InstanceNotFoundError(f'{prefix}:{name}', 'look_in_context')
                else:
                    return type.with_type_if_needed(pid, MixType.INSTANCE)
        if e_id == 'Concept':
            return type.with_type_if_needed(self._storage.id_for_root_concept(), MixType.CONCEPT)
        if e_id == 'Session':
            return type.with_type_if_needed(self._storage.id_for_session(), MixType.CONCEPT)
        if e_id == 'User':
            return type.with_type_if_needed(self._storage.id_for_user(), MixType.CONCEPT)
        if pid_util.is_pid(e_id):
            return self._storage.lookup_pid(e_id, type)
        parts, b = pid_util.is_specific_update(e_id)
        if b:
            return self._storage.lookup_update(*parts, type)
        parts, b = pid_util.is_context_specified(e_id)
        if b:
            return look_in_context(*parts)
        assert base
        if isinstance(base, MyContext):
            base = base.prefix
        to_do = [base]
        visited = set()
        while to_do:
            pref, to_do = to_do[0], to_do[1:]
            if pref in visited:
                continue
            visited.add(pref)
            to_do += self.get_context(pref).get_search_path()
            try:
                return look_in_context(pref, e_id)
            except InstanceNotFoundError:
                continue
            except ConceptNotFoundError:
                continue
        raise InstanceNotFoundError(e_id, 'resolve')
