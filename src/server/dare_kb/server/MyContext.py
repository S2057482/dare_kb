from .Concept import Concept
from .instance import Instance
from .internal import (
        name_spec,
        pid as pid_util,
        )
from .internal.consts import context_state_list
from ..common import Exceptions as E

from typing import List, Optional
from ..common.dkb_typing import PID

class MyContext:

    readers = []
    writers = []
    history = [] ### all 3 attributes are not used yet

    def __init__(self, prefix, title, search_path, owner, identifier, last_marker=0):

        self.mode = None
        self.title = title
        self.prefix = prefix
        self.identifier = identifier
        self.search_path = search_path
        self.last_marker = last_marker
        self.owner = owner
        self.state = context_state_list[0]
        self.changes = {}
        self.list_of_concepts = {}  # dict {label, PID}
        self.list_of_instances = {}  # dict {label, PID}
        self.c_users = [] # List of usernames that are reading simultanuously


    def remove_user(self, user):
        self.c_users.remove(user)
        
    def add_user(self, user):
        self.c_users.append(user)

    def enter_context(self, mode, user):
        self.mode = mode
        self.add_user(user)
    
    def leave_context(self, user):
        self.remove_user(user)
        self.mode = None

    def activate_context(self):
        if (self.state == context_state_list[0] or self.state == None):
            self.state = context_state_list[1]

    def next_marker(self):
        self.last_marker += 1

    def get_search_path(self):
        return self.search_path

    def set_search_path(self, new_search_path):
        self.search_path = [sp for sp in new_search_path]

    def new_concept(self, precise_term, specialises, mutability = "mutable", required = dict(), recommended = dict(), optional = dict(), translation = dict(), description = None, methods = dict(), py_class = None) -> PID:

        if (description is None):
            desc = ""
        else:
            desc = description
        pid = "{}:{}:{}".format(self.identifier, self.last_marker, precise_term)
        c = Concept(self.prefix, precise_term, specialises, mutability, "new", required, recommended, optional, translation, pid, None, desc, methods, py_class)
        self.next_marker()
        self.add_concept(c)
        return c
        
    def new_instance(self, cls: Concept, name: str, **kwargs):
        pid = "{}:{}:{}".format(self.identifier, self.last_marker, name)
        inst = cls.new_instance(self.prefix, name, pid, **kwargs)
        self.next_marker()
        self.add_instance(inst)
        return inst
    
    def add_concept(self, c):
        self.list_of_concepts[c.label] = c.identifier
        
    def add_instance(self, i: Instance):
        self.list_of_instances[i.name] = i.pid
        
    def get_concept(self, identity) -> PID:  # TODO 6: add version
        if (identity in self.list_of_concepts.keys()):
            return self.list_of_concepts[identity]
        else:
            return None
            
    def get_instance(self, identity) -> PID:
        if (identity in self.list_of_instances.keys()):
            return self.list_of_instances[identity]
        else:
            return None

    def context_status(self, concepts, instances):
        cx_status = {}
        cx_status["title"] = self.title
        cx_status["prefix"] = self.prefix
        cx_status["identifier"] = self.identifier
        cx_status["search_path"] = self.search_path
        cx_status["owner"] = self.owner
        cx_status["mode"] = self.mode
        cx_status["state"] = self.state
        cx_status["concepts"] = concepts
        cx_status["instances"] = instances
        cx_status["users"] = self.c_users
        return cx_status

    def get_my_concepts(self):
        return self.list_of_concepts
    
    def get_my_instances(self):
        return self.list_of_instances

    def reset(self):
        self.state = context_state_list[4]
            
    def freeze(self):
        self.state = context_state_list[2]
        self.mode = 'R'
        
    def deprecate(self):
        self.state = context_state_list[3]

    def throw_warning(self):
        if self.state == context_state_list[3]:
            return True, "WARNING: The context "+self.prefix+" is deprecated. It is likely to be discarded soon."
        return False, None
