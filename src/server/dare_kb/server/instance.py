from datetime import datetime

from .internal.consts import instance_state_list
from ..common.dkb_typing import PID

class Instance:

    def __init__(self, name: str, prefix: str, pid: str, cls: PID, state: str, mutability: str=None, timestamp: datetime =None, **kwargs):
        self.name = name  # name represented as a string
        self.prefix = prefix  # prefix (context name) represented as a string
        self.pid = pid  # full pid represented as a string
        self.cls = cls  # class; the pid of the class (a Concept) that this Instance is
        self.state = state or instance_state_list[0]
        self.mutability = mutability or 'mutable'
        self.timestamp = timestamp or datetime.now()
        self.extras = kwargs or {}
        self.predecessor = None
        self.successor = None

    def __str__(self):
        return f'Instance {self.pid} ({self.name}) of {self.cls} in {self.prefix} (created: {self.timestamp})'
        
    def reset(self):
        self.state = instance_state_list[4]

    def update_mutab(self, value):
        self.mutability = value

    def update_value(self, i, value):
        self.extras[i] = value

    def add_prede(self, pid):
        self.predecessor = pid
    
    def add_succ(self, pid):
        self.successor = pid
        
