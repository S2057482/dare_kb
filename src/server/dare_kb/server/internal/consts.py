property_types = {
        'required',
        'recommended',
        'optional',
        }

context_state_list = ['new', 'active', 'frozen', 'deprecated', 'discarded']
instance_state_list = ['new', 'active', 'frozen', 'deprecated', 'discarded']

proj_concept = ['name',
            'prefix',
            'pid',
            'description',
            'state',
            'timestamp',
            'mutability',
            'translation',
            'py_class',
            'methods']
    
proj_instance = ['pid',
            'prefix',
            'name',
            'state',
            'mutability',
            'timestamp']
