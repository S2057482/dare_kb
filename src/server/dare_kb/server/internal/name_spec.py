import re

def my_regex_for_identifiers(id):
    return id.isidentifier()

def my_regex_for_title(id):
    result = re.search("([a-zA-Z0-9 .,:;_])*", id)
    if (result.group() == id):
        return True
    return False


