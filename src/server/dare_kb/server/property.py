
class Property:
    ## class for defining properties
    pass


class MyDataProperty(Property):

    def __init__(self, name, domain, range, is_functional, type):
        self.name = name
        self.domain = domain ## PID
        self.range = range
        self.type = type
        self.is_functional = is_functional


class MyObjectProperty(Property):

    def __init__(self, name, domain, range, is_functional, type):
        self.name = name
        self.domain = domain ## PID
        self.range = range ## PID
        self.type = type
        self.is_functional = is_functional
