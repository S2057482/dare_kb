## This file contains all the functions necessary to the development and use of the DKB

import functools

from owlready2 import *
from .DareKB import *
from .Profile import *

import os.path
import uuid

from . import setting
from .internal import datatype as dt
from .internal import consts
from . import server_state as ss
from .session import SessionManager

from dare_kb.common import format

from typing import List, Optional
from ..common.dkb_typing import (
        PID,
        ConceptComm,
        )

session_manager = SessionManager()

def with_session(sessionID):
    global session_manager
    if not session_manager.has_session(sessionID):
        raise DKBclosedError(f"The session {sessionID} is not valid")
    return ss.DKB.get_handle(sessionID)


def open_dkb(site_name: Optional[str]=None, base_dir: Optional[str]=None, database_path: Optional[str]=None, ontology_path: Optional[str]=None) -> DareKB:
    '''
    Function to open DKB.
    This function will be replaced with fine-grained server application (which has appropriate API calls)
    '''
    site_name = site_name or setting.site_name
    base_dir = base_dir or setting.base_dir
    myfilename = database_path or setting.database_path
    ontology_path = ontology_path or setting.ontology_path

    previous_dkb = DareKB(site_name, base_dir, database_path=myfilename, base_ontology_file=ontology_path)


    ### NEED TO FILL THE DKB WITH KNOWN CONTEXTS
    # previous_dkb._load_contexts()
    #print(ss.DKB._contexts)
    #print()
    #print()

    ss.DKB = previous_dkb
    ss.site_name = site_name

    return ss.DKB  # TODO 10: Used for tests. May not be necessary


def login(site_name: str, username: str, session_id: str = None, **infos) -> str:
    global session_manager
    if ss.DKB:
        if site_name != ss.DKB.site_identifier: # Temporary "fix" for trying to login with a second site_name
            raise DKBnotFoundError(site_name, "login")

    ## verify if user exist:
    if not ss.DKB._storage.is_registered_user(username):
        print('hello')
        if infos == {}:
            infos['user_id'] = username
            infos['email'] = ''
            infos['full_name'] = ''
        
        infos['username'] = username
        us_pid = ss.DKB.new_user(**infos)
    else:
        us_pid = ss.DKB.get_user(username)

    if session_manager.already_login(us_pid):
        raise AlreadyLoggedInError(username)
    session = session_manager.verify_and_register(us_pid, session_id) if session_id is not None else session_manager.new_session(us_pid)

    ss.DKB.new_handle(session, infos)
    return session.session_id

def close(session_id: str) -> None:
    with_session(session_id).close()
    session_manager.remove_session(session_id)

def enter(session_id: str, a_prefix: str, rw: str) -> str:
    return with_session(session_id).enter(a_prefix, rw)

def leave(session_id) -> None:
    with_session(session_id).leave()

def get_search_path(session_id) -> List[str]:
    return with_session(session_id).get_search_path()

def set_search_path(session_id, new_search_path: List[str]) -> None:
    with_session(session_id).set_search_path(new_search_path)

def new_context(session_id, prefix, title=None, search_path=None, owner = None) -> str:
    return with_session(session_id).new_context(prefix, title, search_path, owner=owner)

def new_user(session_id, username, prefer_context, credentials, email) -> str:
    return with_session(session_id).new_user(username, prefer_context, credentials, email)

def context_reset(session_id, prefix=None) -> Dict:
    return with_session(session_id).reset_context(prefix)
    
def context_deprecate(session_id, prefix=None) -> Dict:
    return with_session(session_id).deprecate_context(prefix)

def context_freeze(session_id, prefix=None) -> Dict:
    return with_session(session_id).freeze_context(prefix)

def status(session_id) -> Dict:
    return with_session(session_id).status()

def context_status(session_id, prefix=None) -> Dict:
    return with_session(session_id).context_status(prefix)

def new_concept(session_id, precise_term, specialises = None, mutability = "mutable", required = dict(), recommended = dict(), optional = dict(), translation = dict(), description = None, methods = dict(), py_class = None) -> PID:
    return with_session(session_id).new_concept(precise_term, specialises, mutability, required, recommended, optional, translation, description, methods, py_class)

def serialise_concept(concept, only_these):
    def handle_properties(key, properties):
        p_dict = {}
        if properties:
            for k, p in properties.items():
                if isinstance(p, MyObjectProperty):
                    p_dict[k] = p.range
                else:
                    p_dict[k] = dt.to_str(p.range)
        props[key] = p_dict

    props = {
            # "identity": concept.label,
            'name': concept.label,
            'prefix': concept.prefix,
            'pid': concept.identifier,
            'specialises': concept.sub_class,
            'instance_of': ss.DKB.id_for_root_concept(),
            'description': concept.description,
            'state': concept.state,
            'timestamp': format.timestamp_serialise(concept.timestamp),
            'mutability': concept.mutability,
            'translation': concept.translation,
            'py_class': concept.py_class,
            'methods': concept.methods,
            }
    for p_type in consts.property_types:
        handle_properties(p_type, concept.get_properties(p_type=p_type, direct_only=False))
        
    if only_these is not None:
        o_props = {}
        for p in only_these:
            if p in props.keys():
                o_props[p] = props[p]
        return o_props
    return props

def new_instance(session_id, cls: str, name: str, **kwargs):
    return with_session(session_id).new_instance(cls, name, **kwargs)

def serialise_instance(instance, only_these):
    def handle_extras(extras):
        for k, v in extras.items():
            if (v is None):
                v = ''
            props[k] = v
    props = {
            'name': instance.name,
            'prefix': instance.prefix,
            'instance_of': instance.cls,
            'pid': instance.pid,
            'state': instance.state,
            'mutability': instance.mutability,
            'timestamp': format.timestamp_serialise(instance.timestamp),
#            'predecessor': instance.predecessor,
#            'successor': instance.successor,
            }
    if instance.predecessor:
        props['predecessor'] = instance.predecessor
    if instance.successor:
        props['successor'] = instance.successor
 
    handle_extras(instance.extras)
    if only_these is not None:
        o_props = {}
        for p in only_these:
            if p in props.keys():
                o_props[p] = props[p]
        return o_props
    return props


def get(session_id, identity, only_these, ignore_discarded):
    entry = with_session(session_id).get(identity, ignore_discarded)
    if isinstance(entry, Concept):
        return serialise_concept(entry, only_these)
    else:
        return serialise_instance(entry, only_these)

def find(session_id, query, pid_only, only_these, ignore_discarded):
    results = with_session(session_id).find(query, ignore_discarded)
    res = []
    if pid_only and only_these is None:
        for r in results:
            if isinstance(r, Concept):
                res.append(r.identifier)
            else:
                res.append(r.pid)
    else:
        for r in results:
            if isinstance(r, Concept):
                res.append(serialise_concept(r, only_these))
            else:
                res.append(serialise_instance(r, only_these))
    return res


def update(session_id, id, **kwargs):
    return with_session(session_id).update(id, **kwargs)
