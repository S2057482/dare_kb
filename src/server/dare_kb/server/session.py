import uuid
from datetime import datetime
from ..common.format import *

class InvalidSessionException(Exception):
    def __init__(self, session_id):
        super().__init__(f"The session {session_id} is not a valid session")


class Session:

    def __init__(self, user_id, session_id = None):
        self.user = user_id
        self.token = session_id or str(uuid.uuid1())
        self.start = datetime.now()
        self.session_id = self.user+'_'+pid_timestamp_serialise(self.start)
        self.stop = None
        self.contexts = [] ## List[str]

class SessionManager:

    def __init__(self):
        self.sessions = {}

    def new_session(self, user) -> Session:
        session = Session(user)
        self.sessions[user] = session
        return session

    def verify_and_register(self, user, session_id) -> Session:
        session = Session(user, session_id)
        self.sessions[user] = session
        return session

    def _get_session(self, session_id):
        for k, v in self.sessions.items():
            if v.session_id == session_id:
                return v
        return None

    def get_session(self, session_id):
        s = self._get_session(session_id)
        if s is None:
            raise InvalidSessionException(session_id)
        return s

    def has_session(self, session_id) -> bool:
        return self._get_session(session_id) is not None

    def already_login(self, user) -> bool:
        return user in self.sessions

    def remove_session(self, session_id):
        session = self.get_session(session_id)
        del self.sessions[session.user]
