'''
The storage backend for DareKB.
It handles the data(-base) storage.
'''
from owlready2 import *
from owlready2 import issubclass_owlready
from owlready2.class_construct import Restriction
from owlready2 import owl
import rdflib

import json
from pathlib import Path
from datetime import datetime

from ..common.Exceptions import ConceptNotFoundError, ContextNotFoundError, InstanceNotFoundError, PropertyRangeNotMatchingTypeError

from .internal.inst_mixer import MixType
from .internal.consts import instance_state_list, context_state_list
from .internal.pid import is_pid
from . import setting

from .MyContext import MyContext
from .Concept import Concept
from .instance import Instance
from .property import MyDataProperty, MyObjectProperty

from typing import List, Optional, Tuple, Union, Dict
from ..common.dkb_typing import PID

import logging
logger = logging.getLogger('Storage')
logger.setLevel(logging.DEBUG)

# print("File 'storage.py'.")

class Storage:

    def __init__(self, dkb, base_dir=None, base_ontology_file=None, world_db_filename=None):
        # print("File 'storage.py', Class 'Storage'.")
        base_dir = base_dir or setting.base_dir
        base_ontology_file = str(Path(base_dir) / (base_ontology_file or setting.ontology_path))
        world_db_filename = str(Path(base_dir) / (world_db_filename or setting.database_path))
        logger.info(f"base_dir: {base_dir}; ontology_file: {base_ontology_file}; database_path: {world_db_filename}")
        self._dkb = dkb
        self._base_dir = base_dir
        self._my_world = World(filename = world_db_filename)
        self._ontology = self._my_world.get_ontology(base_ontology_file).load()
        self.namespace_label = "http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36"
        self._namespace = self._ontology.get_namespace(self.namespace_label)
        self._contexts = {}
        self._users = {}
        self._concepts = {}
        self._instances = {}
        self._load_contexts()
        self._load_users()
        self._state = True

        self._proj_concept = {
            'name': 'class_name',
            'prefix': 'class_prefix',
            'pid': 'class_pid',
            'description': 'class_description',
            'state': 'class_state',
            'timestamp': 'class_timestamp',
            'mutability': 'class_mutability',
            'translation': 'class_translation',
            'py_class': 'class_py_class',
            'methods': 'class_methods',
        }

        self._proj_instance = {
            'pid': 'pid',
            'prefix': 'prefix',
            'name': 'name_dkb',
            'state': 'state',
            'mutability': 'mutability',
            # 'timestandamp': 'timestamp',       ## Typo, resulting in empty find() results for concept timestamp search
            'timestamp': 'timestamp',            ## Correction
            }


    def _load_contexts(self) -> None:
        for i in self._namespace.Context.instances():
            self._cache_context(i.prefix, i)

    def _load_users(self) -> None:
        for i in self._namespace.User.instances():
            self._cache_user(i.public_name, i)

    def __del__(self):
        if self._state:
            try:
                self.shutdown()
            except Exception:
                pass

    def shutdown(self):
        if self._state:
            self._state = False
            self.flush()
            self._my_world.close()

    def flush(self):
        self._ontology.save(str(Path(self._base_dir) / "dkb.owl"))
        self._my_world.save()

    def _normalise_name(self, name: str) -> str:
        return name.replace(':', '__')

    def _get_owl_entry_by_pid(self, pid: str):
        res_concept = self._ontology.search(class_pid = pid)
        if res_concept:
            assert len(res_concept) == 1
            return res_concept[0]
        res_instance = self._ontology.search(pid = pid)
        if res_instance:
            assert len(res_instance) == 1
            return res_instance[0]
        raise InstanceNotFoundError(pid, "_get_owl_entry_by_pid")

    def _instantiate_python_context(self, acontext) -> MyContext:
        '''
        @param acontext: a context object from ontology
        '''
        context = MyContext(acontext.prefix, acontext.title, [sp.prefix for sp in acontext.search_path], acontext.owner, identifier=acontext.identifier, last_marker=int(acontext.last_marker))
#        context.my_context = acontext
        context.state = acontext.state
        context.mode = acontext.mode
        context.c_users = [us for us in acontext.users]

        ## add all pids of concepts:
        context.list_of_concepts = self.get_concepts_for_context(context.prefix)
        context.list_of_instances = self.get_instances_for_context(context.prefix)
        return context

    def _cache_context(self, prefix: str, owl_context) -> None:
        self._contexts[prefix] = owl_context

    def _cache_user(self, name: str, owl_user) -> None:
        self._users[name] = owl_user

    def _cache_concept(self, pid: str, owl_concept) -> None:
        self._concepts[pid] = owl_concept

    def has_context(self, prefix: str) -> bool:
#        if prefix in self._contexts and self._contexts.get(prefix).state == context_state_list[4]:
#            return False
        return prefix in self._contexts

    def get_context(self, prefix: str) -> MyContext:
        def create_python_context_given_prefix(c):
            owl_c = self._contexts[c]
            new_c_kb = self._instantiate_python_context(owl_c)
            return new_c_kb
        if not self.has_context(prefix):
            raise ContextNotFoundError(prefix, 'get_context')
        return create_python_context_given_prefix(prefix)

    def context_list(self) -> List:
        return list(self._contexts.keys())

    def on_new_context(self, c: MyContext, session: str) -> None:
        '''
        Create the Context individual (OWL)
        '''
        def create_owl_context(c, session):
            my_context = self._namespace.Context(self._normalise_name(c.identifier))
            my_context.prefix = c.prefix
            my_context.title = c.title
            my_context.last_marker = str(c.last_marker)
            my_context.owner = c.owner
            my_context.identifier = c.identifier
            my_context.state = c.state
            my_context.mode = c.mode
            my_context.search_path = [self._contexts[sp] for sp in c.search_path]
            my_context.users = [sp for sp in c.c_users]
            if session is not None:
                my_context.session = self._ontology.search(pid = session)[0]
            return my_context

        owl_context = create_owl_context(c, session)
        self._cache_context(c.prefix, owl_context)

    def update_search_path(self, context: str, search_path: List[str]) -> None:
        owl_context = self._contexts[context]
        owl_context.search_path = [self._contexts[sp] for sp in search_path]

    def update_last_marker(self, prefix: str, marker: int) -> None:
        self._contexts[prefix].context_last_marker = str(marker)

    def lookup_pid(self, pid, type=MixType.BOTH) -> Union[PID, Tuple[PID, MixType]]:
        '''
        Checks if the given PID exists
        '''
        if type.has_concept():
            if pid == self._namespace.Concept.class_pid[0]:  # TODO 9: this is a hack. See lookup_pid().
                return type.with_type_if_needed(pid, MixType.CONCEPT)
            # This function can't find the right PID, because PID is not the same as class/entity id in the ontology -- PID is in the concept_identifier property. This way of storing information is unlikely right. Consulting Amelie is needed.
            my_label = "*" + pid
            res = self._ontology.search(class_pid = my_label)
            if (res != []):
                assert len(res) == 1
                return type.with_type_if_needed(res[0].class_pid[0], MixType.CONCEPT)
            else:
                if not type.has_instance():
                    raise ConceptNotFoundError(pid, 'lookup_pid')
        if type.has_instance():
            res = self._ontology.search(pid = pid)
            if (res != []):
                assert len(res) == 1
                return type.with_type_if_needed(res[0].pid, MixType.INSTANCE)
            else:
                raise InstanceNotFoundError(pid, 'lookup_pid')
        raise RuntimeError("IllegalAccess: type must fall into either Concept or Instance")

    def lookup_update(self, c, marker, name, type=MixType.BOTH) -> Union[PID, Tuple[PID, MixType]]:
        my_label = "*:" + c + ":" + marker + ":" + name
        if type.has_concept():
            res = self._ontology.search(class_pid = my_label)
            if (res != []):
                return type.with_type_if_needed(res[0].class_pid[0], MixType.CONCEPT)
            raise ConceptNotFoundError(pid, 'lookup_update')
        if type.has_instance():
            res = self._ontology.search(pid = my_label)
            if (res != []):
                return type.with_type_if_needed(res[0].pid[0], MixType.INSTANCE)
            raise ConceptNotFoundError(pid, 'lookup_update')
        raise RuntimeError("IllegalAccess: type must fall into either Concept or Instance")

    def id_for_root_concept(self) -> PID:
        return self._namespace.Concept.class_pid[0]

    def id_for_session(self) -> PID:
        return self._namespace.Session.class_pid[0]

    def id_for_user(self) -> PID:
        return self._namespace.User.class_pid[0]

    def owl_root_concept(self):
        return self._namespace.Concept

    def concept_list(self, prefix) -> Dict:
        _concepts_current_context = {}
        c = self._namespace.Concept
        res = self._ontology.search(is_a = c)
        for c in res:
            if c.class_prefix and c.class_prefix[0] == prefix:
                _concepts_current_context[c.class_name[0]] = c
        return _concepts_current_context

    def has_concept(self, prefix, name) -> bool:
        if (name in self.concept_list(prefix) and self.concept_list(prefix).get(name).class_state[0] == instance_state_list[4]):
            return False
        return name in self.concept_list(prefix)

    def is_concept_given_pid(self, pid: PID) -> bool:
        return len(self._ontology.search(pid = pid)) == 1

    def get_concept_by_pid(self, pid: PID) -> Concept:
        owl_concept = self._get_owl_entry_by_pid(pid)
        subclass = owl_concept.is_a[0]
#        assert self.owl_root_concept() in subclass.ancestors(), f"{pid} is not a regular Concept as its super class is not its first type. {owl_concept.is_a}"  # TODO: Improve the retrieval of super Concept
        if subclass == self.owl_root_concept():
            subclass = self.id_for_root_concept()
        else:
            subclass = subclass.class_pid

        required = {}
        recommended = {}
        optional = {}

        # New lists for storing the 'required', 'recommended' and 'optional' attributes as DataProperties
        requ = {}
        recomm = {}
        opt = {}

        # Only the direct constraints are retrieved here. Indirect ones are dealt with in MyContext
        with self._ontology as onto:
            # If 'with ... as' variable assignment is not working
            if onto is None:
                onto = self._ontology
            for sup in onto.get_parents_of(owl_concept):
                if isinstance(sup, Restriction):
                    ### OLD code beginning
                    # if issubclass_owlready(sup.property, onto.dkb_data_property):
                    #     p_name = sup.property.name
                    #     p_range = sup.value
                    # elif issubclass_owlready(sup.property, onto.dkb_object_property):
                    #     p_name = sup.property.name
                    #     p_range = sup.value.pid
                    # else:
                    #     continue  # Unknown. Probably other subclass-of restrictions, so we ignore them.
                    # if sup.type == 26: # exactly
                    #     required[p_name] = p_range
                    # elif sup.type == 28: # max
                    #     if ('optional' in sup.property.comment):
                    #         optional[p_name] = p_range
                    #     if ('recommended' in sup.property.comment):
                    #         recommended[p_name] = p_range
                    ### OLD code end


                    ### NEW code beginning: Resolving the get() concept issue (Part 2, due to empty 'self.properties')
                    ## Approach similar to instances:
                    # First of all check whether attribute is object property
                    if issubclass_owlready(sup.property, owl.ObjectProperty): # Check for object property
                        p_name = sup.property.name
                        p_range = sup.value.pid
                    # Else it is a data property
                    else:
                        p_name = sup.property.name
                        p_range = sup.value

                    ## Check for attribute type
                    # Required == 26
                    if sup.type == 26: # exactly
                        # Dictionary containing the 'required' values, used for Concept creation
                        required[p_name] = p_range
                        # A new 'MyDataProperty' object for storing the 'required' information
                        requ[p_name] = MyDataProperty(p_name, owl_concept.class_pid[0], p_range, True, sup.property.comment[0])
                    # Optional and recommended == 28
                    elif sup.type == 28: # max
                        # Distinguish between 'optional' and 'recommended' attributes ('property.comment' stores information)
                        if ('optional' in sup.property.comment):
                            # Dictionary containing the 'optional' values, used for Concept creation
                            optional[p_name] = p_range
                            # A new 'MyDataProperty' object for storing the 'optional' information
                            opt[p_name] = MyDataProperty(p_name, owl_concept.class_pid[0], p_range, True, sup.property.comment[0])
                        if ('recommended' in sup.property.comment):
                            # Dictionary containing the 'recommended' values, used for Concept creation
                            recommended[p_name] = p_range
                            # A new 'MyDataProperty' object for storing the 'recommended' information
                            recomm[p_name] = MyDataProperty(p_name, owl_concept.class_pid[0], p_range, True, sup.property.comment[0])
                ## Move 'else' (belonging to restriction check: 'if isinstance(sup, Restriction)') to appropriate part
                else:
                    continue  # Unknown. Probably other subclass-of restrictions, so we ignore them.
                ### NEW code end

        translation = owl_concept.class_translation
        if translation:
            translation = json.loads(translation[0])       ## Type  <class 'owlready2.prop.IndividualValueList'>
        else:
            translation = {}

        methods = owl_concept.class_methods
        if methods:
            methods = json.loads(methods[0])             ## Type  <class 'owlready2.prop.IndividualValueList'>
        else:
            methods = {}

        concept = Concept(owl_concept.class_prefix[0], owl_concept.class_name[0], subclass, owl_concept.class_mutability[0], owl_concept.class_state[0],
                required = required,
                recommended = recommended,
                optional = optional,
                translation = translation,
                identifier = owl_concept.class_pid[0],
                timestamp = owl_concept.class_timestamp,
                methods = methods,
                py_class = owl_concept.class_py_class)
        concept.my_concept = owl_concept

        ### NEW code beginning
        # Dictionary of dictionaries containing 'direct' and 'indirect' DataProperties
        concept_properties = {
                'direct': {
                    'required': requ,
                    'recommended': recomm,
                    'optional': opt,
                    },
                'indirect': {
                    'required': {},
                    'recommended': {},
                    'optional': {},
                    }
                }
        # Adding the above created DataProperty dictionary to the previously created concept
        concept.add_properties(concept_properties)
        ### NEW code end

        return concept


    def get_concept(self, prefix, name) -> Concept:
        '''
        Strictly finds Concept (name) in Context (prefix) without looking up the search path
        '''
        if name == 'Concept':  # TODO: this is likely a hack. Needs further checks
            owl_concept = self.owl_root_concept()
            subclass = None
            return None
        else:
            try:
                owl_concept = self.concept_list(prefix)[name]
            except KeyError:
                raise ConceptNotFoundError(name, 'get_concept')
            return self.get_concept_by_pid(owl_concept.class_pid[0])

    def on_new_concept(self, c: Concept, session) -> None:
        def create_owl_concept_basic(c, session):
            with self._namespace:
                paren_concept = self._get_owl_entry_by_pid(c.sub_class)
                my_concept = types.new_class(self._normalise_name(c.identifier), (paren_concept,))

                my_concept.class_description=c.description
                my_concept.class_pid=c.identifier
                my_concept.class_name=c.label
                my_concept.class_prefix=c.prefix
                my_concept.class_state=c.state
                my_concept.class_mutability=c.mutability
                my_concept.class_timestamp = c.timestamp
                my_concept.class_methods = json.dumps(c.methods)
                my_concept.class_translation = json.dumps(c.translation)
                my_concept.class_py_class=c.py_class
                if (session is not None):
                    #my_concept.session = self._ontology.search(pid = session)    ## ERROR, causing instance creation issue
                    my_concept.session = self._ontology.search(pid = session)[0]  ## Correction, take list element
                return my_concept

        def create_owl_object_property(dp, owl_concept):
            with self._namespace:
                my_property = types.new_class(dp.name, (self._namespace.dkb_object_property, FunctionalProperty))
                my_property.domain.append(owl_concept)
                conc = self._get_owl_entry_by_pid(dp.range)
                my_property.range =[conc]
                my_property.label = [dp.name]
                my_property.comment = [dp.type]
                if dp.type == 'required':
                    c.my_concept.is_a.append(my_property.exactly(1, conc))
                else:
                    c.my_concept.is_a.append(my_property.max(1, conc))
                return my_property

        def create_owl_data_property(dp, owl_concept):
            with self._namespace:
                my_property = types.new_class(dp.name, (self._namespace.dkb_data_property, FunctionalProperty))
                my_property.domain.append(owl_concept)
                my_property.range = [dp.range]
                my_property.label = [dp.name]
                my_property.comment = [dp.type]
                if dp.type == 'required':
                    c.my_concept.is_a.append(my_property.exactly(1, dp.range))
                else:
                    c.my_concept.is_a.append(my_property.max(1, dp.range))
                return my_property

        my_concept = create_owl_concept_basic(c, session)
        c.my_concept = my_concept
        for dp in c.data_properties.values():
            my_property = create_owl_data_property(dp, my_concept)
            dp.my_property = my_property
        for dp in c.object_properties.values():
            my_property = create_owl_object_property(dp, my_concept)
            dp.my_property = my_property

    def ancestor_concepts(self, prefix: str, concept: str) -> List[PID]:
        concept = self.get_concept(prefix, concept)
        owl_concept = concept.my_concept
        return [ancestor.class_pid for ancestor in owl_concept.ancestors() & self.owl_root_concept().descendants()]

    def descendant_concepts(self, prefix: str, concept: str) -> List[PID]:
        concept = self.get_concept(prefix, concept)
        owl_concept = concept.my_concept
        return [descendant.class_pid[0] for descendant in owl_concept.descendants()]

    # Instance
    def has_instance(self, prefix: str, name: str):
        res = self._ontology.search(pid = f'*:{prefix}:*:{name}')
        if len(res) != 0 and res[0].state == instance_state_list[4]:
            return False
        return len(res) != 0

    def _instantiate_python_instance(self, owl_inst):
        '''
        Converts a OWL Instance to Python Instance
        '''
        cls = owl_inst.is_a[0] # TODO: better implementation
        o_dp = self._namespace.dkb_data_property
        o_op = self._namespace.dkb_object_property
        properties = {}
        for p in owl_inst.get_properties():
            if issubclass_owlready(p, o_dp):
                p_name = p.name
                dp = owl_inst.__getattr__(p_name)
                properties[p_name] = dp
            # elif o_op in p.is_a:
            elif issubclass_owlready(p, o_op):
                p_name = p.name
                op = owl_inst.__getattr__(p_name)
                comm = comment[owl_inst, p, op]
                if (len(comm) != 0 and comm[0] == 'pid'):
                    properties[p_name] = op.pid  # TODO: Python representation of the object (Instance?)
                if (len(comm) != 0 and comm[0] == 'name'):
                    properties[p_name] = op.name_dkb
        instance = Instance(owl_inst.name_dkb, owl_inst.prefix, owl_inst.pid, cls.class_pid[0], owl_inst.state, owl_inst.mutability, owl_inst.timestamp, **properties) #, extras)
        instance.my_instance = owl_inst
        return instance

    def get_instance(self, prefix: str, name: str):
        res = self._ontology.search(pid = f'*:{prefix}:*:{name}')
        if not res:
            raise InstanceNotFoundError(f'{prefix}:{name}', 'get_instance')
        owl_inst = res[0]
        return self._instantiate_python_instance(owl_inst)

    ### NEW code beginning
    ## New method to get instance by PID, resolving 'TypeError: ("get_instance() missing 1 required positional argument: 'name'", []),'
    ## one gets one using the above 'get_instance' method with PID
    def get_instance_by_pid(self, pid: PID):
        # Get instance with PID
        res = self._get_owl_entry_by_pid(pid)
        # If no instance for given PID was found, an 'InstanceNotFoundError' is raised
        if not res:
            raise InstanceNotFoundError(f'{pid}', 'get_instance')
        # Return the instantiated instance
        return self._instantiate_python_instance(res)
    ### NEW code end

    def on_new_instance(self, instance: Instance, session):
        def create_owl_instance(ins, session):
            with self._namespace as ns:
                owl_instance = self._get_owl_entry_by_pid(ins.cls)(self._normalise_name(ins.pid))
                #owl_instance = self._namespace.Instance(self._normalise_name(ins.pid))     ## Alternative
                owl_instance.name_dkb = ins.name
                owl_instance.prefix = ins.prefix
                owl_instance.pid = ins.pid
                owl_instance.state = ins.state
                owl_instance.mutability = ins.mutability
                owl_instance.timestamp = ins.timestamp
                owl_instance.predecessor = ins.predecessor
                owl_instance.successor = ins.successor
                owl_instance.instance_last_updated = 1
                #owl_instance.session = self._ontology.search(pid = session)       ## ERROR, causing instance creation issue
                owl_instance.session = self._ontology.search(pid = session)[0]     ## Correction, take list element
                for k, v in ins.extras.items():
                    owl_k = ns[k]  # FIXME: what if name conflict
                    if not owl_k:  # If the property doesn't exist (not owl_k), ignore it. TODO: Improve this behaviour
                        continue
                    if v is None:# If the value is null (`v == None`, thus `not v`), do not store it because owlready doesn't allow to store a null value. This behaviour is needed for future update (e.g. circular dependencies), so it should be no harm
                        continue
                    if issubclass_owlready(owl_k, owl.ObjectProperty):
                        if (v == ''):
                            ## to be set later
                            owl_instance.__setattr__(k, ns._nil)
                            comment[owl_instance, owl_k, ns._nil] = ['pid']
                            continue
                        ## check if v is a pid
                        if (not is_pid(v)):
                            owl_obj = self._find_pid_given_name(v)
                            comment[owl_instance, owl_k, owl_obj] = ['name']
                        else:
                            owl_obj = self._get_owl_entry_by_pid(v)  # v is the PID of the Instance
                            comment[owl_instance, owl_k, owl_obj] = ['pid']
                        owl_instance.__setattr__(k, owl_obj)
                    else:  # It is a data property
                        owl_instance.__setattr__(k, v)
                return owl_instance
        owl_instance = create_owl_instance(instance, session)
        instance.my_instance = owl_instance

    def _find_pid_given_name(self, name: str) -> PID:
        res = self._ontology.search(name_dkb = f'{name}')
        if len(res) == 0:
            return None
        else:
            return res[0]

    def instance_list(self):
        concepts = set(self.owl_root_concept().descendants())
        individuals = []
        for individual in self._ontology.individuals():
            if set(individual.is_a) & concepts:
                individuals.append(individual)
        return [self._instantiate_python_instance(individual) for individual in individuals]

    ### System information

    def set_last_context(self, user: str, context: str, mode: str) -> None:
        for ctx in self._contexts.values():
            if user in ctx.dkb_context_lastset:
                index = ctx.dkb_context_lastset.index(user)
                ctx.dkb_context_lastset.remove(user)
                ctx.dkb_mode_lastset.pop(index)
        if context:
            self._contexts[context].dkb_context_lastset.append(user)
            self._contexts[context].dkb_mode_lastset.append(mode)
        self._users.get(user).context = context
        self._users.get(user).mode = mode


    def get_last_context(self, user: str) -> Optional[str]:
        return (self._users.get(user).context, self._users.get(user).mode)

    def update_state(self, a_context):
        self._contexts[a_context.prefix].state = a_context.state

    def update_mode(self, a_context):
        self._contexts[a_context.prefix].mode = a_context.mode

    def update_users(self, a_context):
        self._contexts[a_context.prefix].users = [sp for sp in a_context.c_users]

    def get_instances_for_context(self, a_context: str) -> Dict:
        _instances = {}
        res = self._ontology.search(prefix = a_context)
        for i in res:
            if (i.state != instance_state_list[4]):
                if (i.name_dkb):
                    _instances[i.name_dkb] = i.pid
        return _instances

    def get_concepts_for_context(self, a_context: str) -> Dict:
        _concepts = {}
        res = self._ontology.search(class_prefix = a_context)
        for i in res:
            if i.class_state[0] != instance_state_list[4]:
                _concepts[i.class_name[0]] = i.class_pid[0]
        return _concepts

    def del_entity(self, pid):
        destroy_entity(pid)

    def discard_context(self, cx_prefix: str) -> None:
        res = self._ontology.search(class_prefix = cx_prefix)
        for c in res:
            c.class_state = instance_state_list[4]
        res = self._ontology.search(prefix = cx_prefix)
        for i in res:
            i.state = instance_state_list[4]

    def print_context(self):
        for i in self._contexts:
            print(i, " and ", self._contexts.get(i).dkb_context_lastset)
            print(i, " and ", self._contexts.get(i).dkb_mode_lastset)

    def find_tuple(self, t, type):
        op = t[0]
        prop = t[1]
        value = t[2]
        les_pids = []

        if type == MixType.CONCEPT:
            prop = self._proj_concept[prop]
            pids = self._ontology.search(** {prop : value})
            # pids = self._ontology.search(** {prop : str(value)})
            for pid in pids:
                ### OLD code beginning
                # if (pid.class_pid is not None):
                #     les_pids.append(pid.class_pid)       ## Causing TypeError: unhashable type: 'IndividualValueList
                ### OLD code end

                ### NEW code beginning
                # Check if '.class_pid' returns a list (contains elements)
                if (pid.class_pid[0] is not None):
                    # If so, add the element the final list 'les_pids'
                    les_pids.append(pid.class_pid[0])
                # Else check whether '.class_pid' contains an entry
                elif (pid.class_pid is not None):
                    # If so, add it to the final list 'les_pids'
                    les_pids.append(pid.class_pid)
                ### NEW code end

        elif type == MixType.INSTANCE:
            prop = self._proj_instance[prop]
            pids = self._ontology.search(** {prop : value})
            for pid in pids:
                if (pid.pid is not None and pid.instance_last_updated == 1):
                    les_pids.append(pid.pid)

        else:
            if (prop in self._proj_instance):
                prop_i = self._proj_instance.get(prop)
                prop_c = self._proj_concept[prop]
                pids = self._ontology.search(** {prop_i : value})
                for pid in pids:
                    if (pid.pid is not None and pid.instance_last_updated == 1):
                        les_pids.append(pid.pid)
                pids = self._ontology.search(** {prop_c : value})
                for pid in pids:
                    ### OLD code beginning
                    # if (pid.class_pid is not None):
                    #     les_pids.extend(pid.class_pid)
                    ### OLD code end

                    ### NEW code beginning
                    # Check if '.class_pid' returns a list (contains elements)
                    if (pid.class_pid[0] is not None):
                        # If so, add the element the final list 'les_pids'
                        les_pids.append(pid.class_pid[0])
                    # Else check whether '.class_pid' contains an entry
                    elif (pid.class_pid is not None):
                        # If so, add it to the final list 'les_pids'
                        les_pids.append(pid.class_pid)
                    ### NEW code end

            else:
                if (is_pid(str(value))):
                    owl_val = self._ontology.search(pid = value)
                    pids = self._ontology.search(** {prop : owl_val})
                else:
                    new_val = value
                    pids = self._ontology.search(** {prop : value})
                for pid in pids:
                    if (pid.pid is not None):
                        les_pids.append(pid.pid)

        return les_pids

    def find_all_tuples(self, o_type, *args):
        '''
        Method to find all tuples with certain parameters (e.g. name or PID);
        The parameter values themselves are irrelevant, all tuples which contain
        the given parameters ('*args') will be returned

        Input:
            o_type (MixType): Object type, indicating whether the parameter belongs
                                to an Instance, a Concept or both
            *args ( (list of) String(s)): The given parameter(s) the tuple should contain

        Output:
            all_pids (list): A list containing the (concept/ instance) PIDs of all
                                tuples fulfilling the criteria
        '''
        # Initialize required variables
        pids_concept = None
        pids_instance = None
        all_pids = []
        dict_concept = {}
        dict_instance = {}

        if o_type == MixType.CONCEPT:
            ## Dynamic search parameters
            for lookup_arg in args:
                # Map each search criteria to its concept search name
                lookup_arg = self._proj_concept[lookup_arg]
                # Add the mapped search criteria and its value ('*' = all) to a dictionary
                dict_concept[lookup_arg] = '*'
            # Lookup the concept PIDs based on the search criteria and values
            pids_concept = self._ontology.search(** dict_concept)

            ## Hardcoded search parameters
            # pids_concept = self._ontology.search(class_pid ='*', class_prefix = '*')

        elif o_type == MixType.INSTANCE:
            ## Dynamic search parameters
            for lookup_arg in args:
                # Map each search criteria to its instance search name
                lookup_arg = self._proj_instance[lookup_arg]
                # Add the mapped search criteria and its value ('*' = all) to a dictionary
                dict_instance[lookup_arg] = '*'
            # Lookup the instance PIDs based on the search criteria and values
            pids_instance = self._ontology.search(** dict_instance)

            ## Hardcoded search parameters
            # pids_instance = self._ontology.search(pid ='*', prefix = '*')

        else:
            ## Dynamic search parameters
            for lookup_arg in args:
                # Map each search criteria to its concept search name
                lookup_arg_conc = self._proj_concept[lookup_arg]
                # Add the mapped search criteria and its value ('*' = all) to a dictionary
                dict_concept[lookup_arg_conc] = '*'
                # Map each search criteria to its instance search name
                lookup_arg_inst = self._proj_instance[lookup_arg]
                # Add the mapped search criteria and its value ('*' = all) to a dictionary
                dict_instance[lookup_arg_inst] = '*'
            # Lookup the concept PIDs based on the search criteria and values
            pids_concept = self._ontology.search(** dict_concept)
            # Lookup the instance PIDs based on the search criteria and values
            pids_instance = self._ontology.search(** dict_instance)

            ## Hardcoded search parameters
            # pids_concept = self._ontology.search(class_pid ='*', class_prefix = '*')
            # pids_instance = self._ontology.search(pid ='*', prefix = '*')

        if (pids_concept is not None):
            for pid in pids_concept:
                # Check if '.class_pid' returns a list (contains elements)
                if (pid.class_pid[0] is not None):
                    # If so, add the element the final list 'all_pids'
                    all_pids.append(pid.class_pid[0])
                # Else check whether '.class_pid' contains an entry
                elif (pid.class_pid is not None):
                    # If so, add it to the final list 'all_pids'
                    all_pids.append(pid.class_pid)
        if (pids_instance is not None):
            for pid in pids_instance:
                all_pids.append(pid.pid)

        return all_pids


    def find_tuples_inRange(self, user_query, o_type):
        '''
        Method to find all tuples within a certain range

        Input:
            user_query (list): The given user query, consisting of
                                - Operator (e.g. '<=')
                                - Search parameter (e.g. 'name') and
                                - Search value
            o_type (MixType): Object type, indicating whether the parameter belongs
                                to an Instance, a Concept or both

        Output:
            range_pids (list): A list containing the (concept/ instance) PIDs of all
                                tuples fulfilling the search criteria
        '''
        ### ONLY required when RDFlib to execute SPARQL queries, NOT required with native SPARQL engine
        # # Create a RFDlib graph object, ob which the SPARQL query will be executed
        # graph = self._ontology.world.as_rdflib_graph()

        # Extract operator, property and value values from the given query
        operand = user_query[0]
        prop = user_query[1]
        value = user_query[2]
        # From dictionary search entries, e.g. {"name": "String"}: Remove all "; Most efficient way: translate() function
        translation = value.maketrans({replacement: "" for replacement in ['"']})
        value = value.translate(translation)

        # Initialize necessary variables
        prop_concept = ''
        prop_instance = ''
        range_pids = []
        range_pids_concepts = []
        range_pids_instances = []
        range_refs = []
        range_refs_concept = []
        range_refs_instance = []

        # Exchange the user input property (e.g. 'name') with the according DKB property
        # (e.g. 'class_name' for concepts, 'dkb_name' for instances)
        if o_type == MixType.CONCEPT:
            prop_concept = self._proj_concept[prop]
        elif o_type == MixType.INSTANCE:
            prop_instance = self._proj_instance[prop]
        else:
            prop_instance = self._proj_instance.get(prop)
            prop_concept = self._proj_concept[prop]

        ### ONLY required when RDFlib to execute SPARQL queries, NOT required with native SPARQL engine
        # # Create the URI reference for binding the property value
        # # self.namespace_label: 'http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36'
        # # E.g., class_name: 'http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#class_name'
        # var_binding_concept = self.namespace_label + '#' + prop_concept                                                                  # RDFlib
        # var_binding_instance = self.namespace_label + '#' + prop_instance                                                                # RDFlib
        # # Set the variable datatype used for the query FILTER
        # # Default string (PIDs, name, prefix, description, state, mutability, translation, py_class and methods are all <class 'str'>)
        # d_type = 'xsd:string'                                                                                                            # RDFlib
        # # Solely timestamp is of <class 'datetime.datetime'>
        # if (prop == 'timestamp'):                                                                                                        # RDFlib
        #     d_type = 'xsd:dateTime'                                                                                                      # RDFlib

        # Link to the used ontology (onto_link) and how this ontology should be referred to (abbreviated) in the SPARQL queries (onto_pref)
        onto_pref = 'onto36'
        onto_link = '<http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#>'
        ## Create a dynamic SPARQL query, which extracts solely the concept/ instance reference and FITLERS according to the query criteria AND
        ## Execute the SPARQL query
        # RDFlib to execute SPARQL queries on the previously created graph object; use initBindings for the lookup property
        # sparql_query = 'select ?ref where {?ref ?prop ?value filter ( ' + d_type + '(?value) ' + operand + d_type + '("' + value + '")  ) }'  # RDFlib
        # range_refs_concept = list(graph.query_owlready(sparql_query, initBindings = {'prop': rdflib.URIRef(var_binding_concept)} ) )          # RDFlib
        # range_refs_instance = list(graph.query_owlready(sparql_query, initBindings = {'prop': rdflib.URIRef(var_binding_instance)} ) )        # RDFlib
        # Quicker native Sparql engine
        if prop_concept != '':
            sparql_query_concept = 'PREFIX ' + onto_pref  + ': ' + onto_link + ' select ?ref where {?ref ' + onto_pref + ':' + prop_concept + ' ?value filter ( ?value ' + operand + ' "' + value + '"  ) }'
            # sparql_query_concept = 'PREFIX onto36: <http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#> select ?ref where {?ref onto36:' + prop_concept + ' ?value filter ( ?value ' + operand + ' "' + value + '"  ) }'
            range_refs_concept = list(self._ontology.world.sparql(sparql_query_concept, error_on_undefined_entities=False) )
        if prop_instance != '':
            sparql_query_instance = 'PREFIX ' + onto_pref  + ': ' + onto_link + ' select ?ref where {?ref ' + onto_pref + ':' + prop_instance + ' ?value filter ( ?value ' + operand + ' "' + value + '"  ) }'
            # sparql_query_instance = 'PREFIX onto36: <http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#> select ?ref where {?ref onto36:' + prop_instance + ' ?value filter ( ?value ' + operand + ' "' + value + '"  ) }'
            range_refs_instance = list(self._ontology.world.sparql(sparql_query_instance, error_on_undefined_entities=False) )

        ## Unpack the two lists (range_refs_concept, range_refs_instance) with '*'; Accumulate all list entries in one final list
        range_refs = [y for x in (*range_refs_concept, *range_refs_instance) for y in x]
        # range_refs will be the FILTER criteria for the following SPARQL query and therefore requires preparation
        # 1. Convert the substring range_refs to a string
        # 2. Remove the prefix "untitled-ontology-36." (e.g. [untitled-ontology-36.Concept] with 'split' and keep the element after the '.' deliminator
        # 3. Add the ontology prefix as the new prefix
        onto_refs = [(onto_pref + ':' + str(ref).split(".")[1]) for ref in range_refs]
        # For the dynamic query creation: Convert the onto_refs list into a string
        onto_refs_string = ", ".join(str(elem) for elem in onto_refs)

        ## Map the identified reference to the according PIDs
        ## Create a dynamic SPARQL query, which extracts the PIDS for the concept/ instance reference AND
        ## Execute the SPARQL query
        # RDFlib to execute SPARQL queries on the previously created graph object; use initBindings for the lookup property
        # sparql_query_pids = 'select ?pid where {?ref ?prop ?pid filter ( ?ref IN (' + onto_refs_string + ')  ) }'             # RDFlib
        # range_pids_concepts = list(graph.query_owlready(sparql_query_pids, initBindings = {'prop': rdflib.URIRef(<http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#class_pid>)} ) )    # RDFlib
        # range_pids_instances = list(graph.query_owlready(sparql_query_pids, initBindings = {'prop': rdflib.URIRef(<http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#pid>)} ) )         # RDFlib
        if prop_concept != '':
            sparql_query_pids_concept = 'PREFIX ' + onto_pref  + ': ' + onto_link + ' select ?pid where {?ref onto36:class_pid ?pid filter ( ?ref IN (' + onto_refs_string + ')  ) }'
            # sparql_query_pids_concept = 'PREFIX onto36: <http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#> select ?pid where {?ref onto36:class_pid ?pid filter ( ?ref IN (' + onto_refs_string + ')  ) }'
            # Quicker native Sparql engine
            range_pids_concepts = list(self._ontology.world.sparql(sparql_query_pids_concept, error_on_undefined_entities=False) )
        if prop_instance != '':
            sparql_query_pids_instances = 'PREFIX ' + onto_pref  + ': ' + onto_link + ' select ?pid where {?ref onto36:pid ?pid filter ( ?ref IN (' + onto_refs_string + ')  ) }'
            # sparql_query_pids_instances = 'PREFIX onto36: <http://www.semanticweb.org/ryey/ontologies/2020/5/untitled-ontology-36#> select ?pid where {?ref onto36:pid ?pid filter ( ?ref IN (' + onto_refs_string + ')  ) }'
            # Quicker native Sparql engine
            range_pids_instances = list(self._ontology.world.sparql(sparql_query_pids_instances, error_on_undefined_entities=False) )

        ## Unpack the two lists (range_pids_concepts, range_pids_instance) with '*'; Accumulate all list entries in one final list
        range_pids = [y for x in (*range_pids_concepts, *range_pids_instances) for y in x]

        return range_pids


    def find_isa(self, name):
        class_ = self._get_owl_entry_by_pid(name)
        les_pids = []
        pids = self._ontology.search(is_a = class_)
        if (class_.class_name[0] == 'Concept'):
            for pid in pids:
                if (pid.pid is not None):
                    les_pids.append(pid.pid)
                elif (pid.class_pid is not None):
                    les_pids.extend(pid.class_pid)
        else:
            for pid in pids:
                if (pid.pid is not None):
                    les_pids.append(pid.pid)
        return les_pids

    def find_isa_exactly(self, name):
        class_ = self._get_owl_entry_by_pid(name)
        les_pids = []
        pids = self._ontology.search(is_a = class_)
        if (class_.class_name[0] == 'Concept'):
            for pid in pids:
                if (pid.pid is not None and pid.is_a[0] == class_):
                    les_pids.append(pid.pid)
                elif (pid.class_pid is not None):
                    les_pids.extend(pid.class_pid)
        else:
            for pid in pids:
                if (pid.pid is not None and pid.is_a[0] == class_):
                    les_pids.append(pid.pid)
        return les_pids

    def prop_existed(self, name):
        with self._namespace as ns:
            return ns[name] is not None

    def update(self, to_update, dic_prop, is_up):
        if (isinstance(to_update, Concept)):
            owl_concept = to_update.my_concept
            owl_concept.class_py_class = to_update.py_class
            owl_concept.class_description = to_update.description
            owl_concept.class_mutability = to_update.mutability
            owl_concept.class_methods = json.dumps(to_update.methods)
            owl_concept.class_translation = json.dumps(to_update.translation)
            if dic_prop:
                print("there is some", dic_prop)
        else:
            owl_instance = to_update.my_instance
            owl_2 = is_up.my_instance
            owl_2.instance_last_updated = 0
            owl_instance.state = 'updated'
            owl_2.successor = to_update.pid
            owl_instance.mutability = to_update.mutability
            if dic_prop:
                for p in dic_prop:
                    the_prop = self._namespace[p]
                    if issubclass_owlready(the_prop, owl.ObjectProperty):
                        v = dic_prop[p][1]
                        pid_class = self._get_owl_entry_by_pid(dic_prop[p][0])
                        if (v == ''):
                            ## to be set later
                            owl_instance.__setattr__(p, self._namespace._nil)
                            comment[owl_instance, the_prop, self._namespace._nil] = ['pid']
                            continue
                        ## check if v is a pid
                        if (not is_pid(v)):
                            owl_obj = self._find_pid_given_name(v)
                            comment[owl_instance, the_prop, owl_obj] = ['name']
                        else:
                            owl_obj = self._get_owl_entry_by_pid(v)  # v is the PID of the Instance
                            comment[owl_instance, the_prop, owl_obj] = ['pid']
                        if (not owl_obj.is_a[0] == pid_class):
                            raise PropertyRangeNotMatchingTypeError(the_prop.label, pid_class, owl_obj.is_a[0])
                        else:
                            owl_instance.__setattr__(p, owl_obj)
                    else:  # It is a data property
                        v = dic_prop[p]
                        owl_instance.__setattr__(p, v)

    def new_user(self, username, user_id, prefer_context, full_name, email, mode):
        '''
        Create the User individual (OWL)
        '''
        ## username is the platform user/name
        ## credential would be keycloak username and pass (eventhough not a good idea of storing pass)
        owl_user = self._namespace.User(user_id)
        owl_user.pid = user_id
        owl_user.context = prefer_context
        owl_user.email = email
        owl_user.public_name = username
        owl_user.full_name = full_name
        owl_user.mode = mode

        self._cache_user(username, owl_user)
        return user_id

    def is_registered_user(self, id):
        return len(self._ontology.search(public_name = id)) > 0

    def get_user(self, us):
        return self._users.get(us).pid

    def register_session(self, session):
        owl_session = self._namespace.Session(session.session_id)
        owl_session.session_user = self._ontology.search(pid = session.user)[0]
        owl_session.starts = session.start
        owl_session.token = session.token
        owl_session.pid = session.session_id


    def finish_session(self, session_id):
        owl_session = self._ontology.search(pid = session_id)[0]
        owl_session.ends = datetime.now()
