from setuptools import setup, find_packages

setup(
    name = "dare_kb",
    description = "DARE Knowledge Base server",
    long_description = "Knowledge Base system for EU DARE project",

    packages = find_packages(),
    include_package_data = True,
    platforms = "any",
    install_requires = [
        "rdflib",
        "owlready2",
        "flask",
        "flask-restful",
        ],

    scripts = [],
    entry_points = {
        'console_scripts': [
            'dkb_server = dare_kb.__main__:main',
            'dkb_manage = dare_kb.server.dkb_manage:main',
        ]
    }
)
