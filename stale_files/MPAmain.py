import DKB_functions as DKB


#global onto
def main():
    session = DKB.connectToPlatform("Amelie","Levray") ## or identifier
    dkb = DKB.initialiseDKB(session)
    print('DKB has been initialised')

    myConcept1 = DKB.newConcept(dkb, "Test");

    ## test the method getConcept
    print(DKB.getConcept(dkb, myConcept1))
    print(DKB.getConcept(dkb,"*Thermometer"))



#print(onto.search(iri = "*Thermometer"))

if __name__ == "__main__":
    main()
