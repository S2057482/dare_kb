

from owlready2 import *
import setting

def main():
    myfilename = "./backgroundKB.sqlite3"
    default_world.set_backend(filename = myfilename)
    my_world = default_world
    _ontology = my_world.get_ontology(setting.back_ontology_path).load()
    namespace_label = "urn:absolute:DareKnowledgeBase"
    _namespace = _ontology.get_namespace(namespace_label)
    
    _ontology.save("savedbackgroundKB.owl")


    
if __name__ == "__main__":
    main()
