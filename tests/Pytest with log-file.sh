#!/bin/sh

cd Test_files
#https://docs.pytest.org/en/latest/how-to/output.html

#pytest ex3_all_test.py  
#pytest ex3_all_test.py -q --report-log=Pytest_logfiles/log.json
#pytest ex3_all_test.py --tb=long --report-log=Pytest_logfiles/test_long.txt
pytest ex3_all_test.py --tb=line --report-log=Pytest_logfiles/test_line.txt
#pytest ex3_all_test.py -rpP --report-log=Pytest_logfiles/test_fail_succRes.txt
#pytest ex3_all_test.py --resultlog=Pytest_logfiles/log.txt
#pytest ex3_retained_test.py
# completed testing

