
import DKBlib as DKB

dkb = DKB.login('ex3KB', username = 'root')

def create_concept():
    dkb.new_concept('Workflow', required = {            # one instance per version of a workflow
    'name': 'String',               # user's name for it
    'version': 'String',
    'source': 'String',             # link to the script that will be used when this is run
                                        # may have other common attributes here
    })

    dkb.new_concept('D4pWorkflow', specialises = 'Workflow', required = {            # one instance per version of a workflow ** slightly different name?
    'nodes': 'Integer',
     'no_processes' : 'Integer',
     'target' : 'String',
     'reqs_path': 'String',
    'inputdata' : 'String',
    'workspace_id' : 'Integer',
     'impl_id' : 'Integer',
    },
    optional = {
     'iterations' : 'Integer',
     'prev_run_id' : 'String',
     'image': 'String',
                    })

    dkb.new_concept('Run', required = {        # one generated, each time run is performed on a wf
    'name': 'String',           # an optionally supplied name from the user, otherwise generate it, e.g., wf.name+str(run_count)
    'wf': 'Workflow',           # the wf being run
    'start': 'DateTime',      # when this run started
    'user': 'User',               # who to save going via Session perhaps?
    'workspace_id': 'String',  # where its output files will be found (other such things returned by dm calls)
    'trace_id': 'String',      # the id returned from the provenance system to find traces for this run (may not be possible now)
    'context': 'String',        # where it updates reside in the dkb
    'run_dir': 'String',
    'run_id': 'String',
    'job_name': 'String',
    'impl_id': 'String',
    })

def create_instance():
    i = dkb.new_instance('D4pWorkflow', name = 'dispel4py_CMT', version = '',
        source =  'https://gitlab.com/project-dare/WP6_EPOS/-/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_CMTSOLUTION_json.py',
        nodes = 1,
        no_processes = 1,
        target = "simple",
        reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
        inputdata = '{"json_organizer":[{"file_url": STAGED_DATA+"/Input.json", "out_url": STAGED_DATA+"/"}]}', workspace_id = '', impl_id = '')

    j = dkb.new_instance('D4pWorkflow', name = 'pyflex_pycmt3d', version = '',
        source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_pyflex_pycmt3d.py',
        nodes = 1,
        no_processes = 1,
        iterations = 1,
        target = "simple",
        image = 'mt3d',
        reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
        inputdata = '{"producer": [ {"input_data":"interp", "prep_output": "output_pr", "outdir": "results", "json_input": INPUT_FLD+"/Input.json", "out_url": INPUT_FLD}]}', workspace_id = '', impl_id = '')

    k = dkb.new_instance('D4pWorkflow', name = 'dispel4py_download', version = '',
        source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_download.py',
        nodes = 1,
        no_processes = 1,
        iterations = 1,
        target = "simple",
        reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
        inputdata = '{"ReadSpecfem3d" : [{"json_file":input_path+"/Input.json","par_files_path":input_path,"mesh_vel_path":"https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/DATA/mesh_vel.zip","output":"download_test.json"}', workspace_id = '', impl_id = '')


if __name__ == "__main__":
    dkb.enter('seismo', 'W')
    create_concept()
    create_instance()

dkb.close()
