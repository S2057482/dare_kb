import DKBlib as DKB

dkb = DKB.login('ex3KB', username = 'root')
seis_context = dkb.new_context('seismo', 'Seismology context')
dkb.enter('seismo', 'W')

############################
## Concept declaration
############################
### Workflow
dkb.new_concept('Workflow', required = {            # One instance per version of a workflow
  'name': 'String',                                 # The user's name for it
  'version': 'String',
  'source': 'String',                               # link to the script that will be used when this is run
                                                    # may have other common attributes here
  })

### Dispel4py Workflow
dkb.new_concept('D4pWorkflow', specialises = 'Workflow', required = {           # One instance per version of a workflow
  'nodes': 'Integer',
   'no_processes' : 'Integer',
   'target' : 'String',
   'reqs_path': 'String',
  'inputdata' : 'String',
  'workspace_id' : 'Integer',
   'impl_id' : 'Integer',
  },
  optional = {
   'iterations' : 'Integer',
   'prev_run_id' : 'String',
   'image': 'String',
                  })

dkb.new_concept('Run', required = {        # One generated, each time run is performed on a wf
  'name': 'String',                        # An optionally supplied name from the user, otherwise generate it,
                                           # e.g., wf.name+str(run_count)
  'wf': 'Workflow',                        # The wf being run
  'start': 'String',                       # Point in time when this run started
  'user': 'String',                        # who to save going via Session perhaps?
  'workspace_id': 'String',                # Where its output files will be found (other such things returned by dm calls)
  'trace_id': 'String',                    # The ID returned from the provenance system to find traces for this run
  'context': 'String',                     # Where it updates reside in the dkb
  'run_dir': 'String',
  'run_id': 'String',
  'job_name': 'String',
  'impl_id': 'String',
  })


### Docker
dkb.new_concept('Docker', required = {            # one instance per version of a workflow ** slightly different name?
   'docker_name': 'String',
   'docker_tag': 'String',
   'docker_url': 'String',
   'script_names': 'String',
   'script_path': 'String'
  }
)

### CWL Manager
dkb.new_concept('CWLManager', required = {            # one instance per version of a workflow ** slightly different name?
  'cwl_url': 'String',
  'username': 'String',
  'token': 'String'},
  optional = {
     'docker_name': 'String',
     'docker_tag': 'String',
     'script_names': 'String',
     'docker_folder': 'String'
     }
)

### CWL Workflow
dkb.new_concept('CWLWorkflow', specialises = {'Workflow', 'Docker'}, required = {
  'spec_name': 'String',
  'path_to_cwls': 'String',
  'register_docker': 'Boolean',           ##################### DEFAULT VALUES?###################
  }
)
### CWL Workflow
dkb.new_concept('Test', specialises = 'Docker', required = {
  'spec_name': 'String',
  'path_to_cwls': 'String',
  'register_docker': 'bool' 'Boolean' 'Bool',           ##################### DEFAULT VALUES?###################
  }
)


############################
## Instance creation: workflows
############################
### MT3D
############################
### username = os.environ['USERNAME']
### rundir = os.environ["RUN_DIR"]
### path = "/home/mpiuser/sfs/{}/runs/{}".format(username,rundir)
try:
    i = dkb.new_instance('D4pWorkflow', name = 'dispel4py_CMT', version = '',
          source =  'https://gitlab.com/project-dare/WP6_EPOS/-/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_CMTSOLUTION_json.py',
          nodes = 1,
          no_processes = 1,
          target = "simple",
          reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
          inputdata = '{"json_organizer":[{"file_url": STAGED_DATA+"/Input.json", "out_url": STAGED_DATA+"/"}]}', workspace_id = '', impl_id = '')
except:
    pass

try:
    j = dkb.new_instance('D4pWorkflow', name = 'pyflex_pycmt3d', version = '',
          source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_pyflex_pycmt3d.py',
          nodes = 1,
          no_processes = 1,
          iterations = 1,
          target = "simple",
          image = 'mt3d',
          reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
          inputdata = '{"producer": [ {"input_data":"interp", "prep_output": "output_pr", "outdir": "results", "json_input": INPUT_FLD+"/Input.json", "out_url": INPUT_FLD}]}', workspace_id = '', impl_id = '')
except:
    pass

try:
    k = dkb.new_instance('D4pWorkflow', name = 'dispel4py_download', version = '',
          source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_download.py',
          nodes = 1,
          no_processes = 1,
          iterations = 1,
          target = "simple",
          reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
          inputdata = '{"ReadSpecfem3d" : [{"json_file":input_path+"/Input.json","par_files_path":input_path,"mesh_vel_path":"https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/DATA/mesh_vel.zip","output":"download_test.json"}]}', workspace_id = '', impl_id = '')
except:
    pass

try:
    l = dkb.new_instance('D4pWorkflow', name = 'dispel4py_preproc', version = '',
           source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_preproc.py',
           nodes = 1,
           no_processes = 1,
           iterations = 1,
           target = "simple",
           reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
           inputdata = '{"data" : [{"input":input_path+"/Input.json"}]}', workspace_id = '', impl_id = '')
except:
    pass

### without username = os.environ['USERNAME']
### without rundir = os.environ["RUN_DIR"]
### without path = "/home/mpiuser/sfs/{}/runs/{}".format(username,rundir)
try:
    m = dkb.new_instance('D4pWorkflow', name = 'dispel4py_download_RF', version = '',
          source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/MT-3D/workflow/dispel4py_download_RF.py',
          nodes = 1,
          no_processes = 1,
          iterations = 1,
          target = "simple",
          reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_MT3D.txt',
          inputdata = '{"ReadSpecfem3d" : [{"json_file":input_path+"/Input.json","par_files_path":input_path,"mesh_vel_path":"https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/DATA/mesh_vel.zip","output":"download_test.json"}]}', workspace_id = '', impl_id = '')
except:
    pass

############################
### RA
############################
try:
    n = dkb.new_instance('D4pWorkflow', name = 'dispel4py_RA_pgm_story_real', version = '',
           source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/RA/dispel4py_RA.pgm_story.py',
           nodes = 1,
           no_processes = 1,
           iterations = 1,
           target = "simple",
           reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_RA.txt',
           inputdata = '{"streamProducerReal" : [{"input":input_path+"/Input.json"}]', workspace_id = '', impl_id = '')
except:
    pass

try:
    m = dkb.new_instance('D4pWorkflow', name = 'dispel4py_RA_pgm_story_synth', version = '',
           source = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/RA/dispel4py_RA.pgm_story.py',
           nodes = 1,
           no_processes = 1,
           iterations = 1,
           target = "simple",
           reqs_path = 'https://gitlab.com/project-dare/dare-examples/-/raw/master/wp6/requirements/requirements_RA.txt',
           inputdata = '{"streamProducerSynth" : [{"input":input_path+"/Input.json"}]', workspace_id = '', impl_id = '')
except:
    pass

############################
## Instance creation: runs
############################
### MT3D
############################
try:
    o = dkb.new_instance( 'Run', name = 'dispel4py_CMT_01', wf= 'dispel4py_CMT', start= '01062021T080000',
                          user= 'root', workspace_id= '01', trace_id= '12345', #context= 'seismo',
                          run_dir= 'seismo_dir',
                          run_id= '01', job_name= 'job01', impl_id= '12345' )
except:
    pass

try:
    p = dkb.new_instance('Run', name = 'dispel4py_CMT_02', wf= 'dispel4py_CMT', start= '01062021T093000',
                          user = 'root', workspace_id = '01', trace_id = '6789', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '02', job_name = 'job02', impl_id = '6789')
except:
    pass

try:
    q = dkb.new_instance('Run', name = 'dispel4py_CMT_03', wf = 'dispel4py_CMT', start = '01062021T103000',
                          user = 'root', workspace_id = '01', trace_id = '1011', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '03', job_name = 'job03', impl_id = '1011')
except:
    pass

try:
    r = dkb.new_instance( 'Run', name = 'pyflex_pycmt3d_01', wf = 'pyflex_pycmt3d', start = '01062021T080000',
                          user = 'root', workspace_id = '02', trace_id = '12345', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '04', job_name = 'job04', impl_id = '12345' )
except:
    pass

try:
    s = dkb.new_instance('Run', name = 'pyflex_pycmt3d_02', wf = 'pyflex_pycmt3d', start = '01062021T093000',
                          user = 'root', workspace_id = '02', trace_id = '6789', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '05', job_name = 'job05', impl_id = '6789')
except:
    pass

try:
    t = dkb.new_instance('Run', name = 'pyflex_pycmt3d_03', wf = 'pyflex_pycmt3d', start = '01062021T103000',
                          user = 'root', workspace_id = '02', trace_id = '1011', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '06', job_name = 'job06', impl_id = '1011')
except:
    pass

try:
    u = dkb.new_instance( 'Run', name = 'dispel4py_download_01', wf = 'dispel4py_download', start = '01062021T080000',
                          user = 'root', workspace_id = '03', trace_id = '12345', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '07', job_name = 'job07', impl_id = '12345' )
except:
    pass

try:
    v = dkb.new_instance('Run', name = 'dispel4py_download_02', wf = 'dispel4py_download', start = '01062021T093000',
                          user = 'root', workspace_id = '03', trace_id = '6789', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '08', job_name = 'job08', impl_id = '6789')
except:
    pass

try:
    w = dkb.new_instance('Run', name = 'dispel4py_download_03', wf = 'dispel4py_download', start = '01062021T103000',
                          user = 'root', workspace_id = '03', trace_id = '1011', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '09', job_name = 'job09', impl_id = '1011')
except:
    pass

try:
    x = dkb.new_instance( 'Run', name = 'dispel4py_preproc_01', wf = 'dispel4py_preproc', start = '01062021T080000',
                          user = 'root', workspace_id = '04', trace_id = '12345', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '10', job_name = 'job10', impl_id = '12345' )
except:
    pass

try:
    y = dkb.new_instance('Run', name = 'dispel4py_preproc_02', wf = 'dispel4py_preproc', start = '01062021T093000',
                          user = 'root', workspace_id = '04', trace_id = '6789', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '11', job_name = 'job11', impl_id = '6789')
except:
    pass

try:
    z = dkb.new_instance('Run', name = 'dispel4py_preproc_03', wf = 'dispel4py_preproc', start = '01062021T103000',
                          user = 'root', workspace_id = '04', trace_id = '1011', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '12', job_name = 'job12', impl_id = '1011')
except:
    pass

try:
    a = dkb.new_instance( 'Run', name = 'dispel4py_download_RF_01', wf = 'dispel4py_download_RF', start = '01062021T080000',
                          user = 'root', workspace_id = '05', trace_id = '12345', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '13', job_name = 'job13', impl_id = '12345' )
except:
    pass

try:
    b = dkb.new_instance('Run', name = 'dispel4py_download_RF_02', wf = 'dispel4py_download_RF', start = '01062021T093000',
                          user = 'root', workspace_id = '05', trace_id = '6789', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '14', job_name = 'job14', impl_id = '6789')
except:
    pass

try:
    c = dkb.new_instance('Run', name = 'dispel4py_download_RF_03', wf = 'dispel4py_download_RF', start = '01062021T103000',
                          user = 'root', workspace_id = '05', trace_id = '1011', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '15', job_name = 'job15', impl_id = '1011')
except:
    pass

############################
### RA
############################
try:
    d = dkb.new_instance( 'Run', name = 'dispel4py_RA_pgm_story_real_01', wf = 'dispel4py_RA_pgm_story_real', start = '01062021T080000',
                          user = 'root', workspace_id = '06', trace_id = '12345', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '16', job_name = 'job16', impl_id = '12345' )
except:
    pass

try:
    e = dkb.new_instance('Run', name = 'dispel4py_RA_pgm_story_real_02', wf = 'dispel4py_RA_pgm_story_real', start = '01062021T093000',
                          user = 'root', workspace_id = '06', trace_id = '6789', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '17', job_name = 'job17', impl_id = '6789')
except:
    pass

try:
    f = dkb.new_instance('Run', name = 'dispel4py_RA_pgm_story_real_03', wf = 'dispel4py_RA_pgm_story_real', start = '01062021T103000',
                          user = 'root', workspace_id = '06', trace_id = '1011', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '18', job_name = 'job18', impl_id = '1011')
except:
    pass

try:
    g = dkb.new_instance( 'Run', name = 'dispel4py_RA_pgm_story_synt_01', wf = 'dispel4py_RA_pgm_story_synt', start = '01062021T080000',
                          user = 'root', workspace_id = '07', trace_id = '12345', #context = 'seismo',
                          run_dir = 'seismo_dir', run_id = '19', job_name = 'job19', impl_id = '12345' )
except:
    pass

try:
    h = dkb.new_instance('Run', name = 'dispel4py_RA_pgm_story_synt_02', wf = 'dispel4py_RA_pgm_story_synt', start = '01062021T093000',
                          user = 'root', workspace_id = '07', trace_id = '6789', #context = 'seismo',
                          run_dir = 'seismo_dir',
                          run_id = '20', job_name = 'job20', impl_id = '6789')
except:
    pass


print(dkb.context_status()['concepts'])
print(dkb.context_status()['instances'])

dkb.close()
