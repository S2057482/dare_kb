## Import libraries
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
from time import process_time
import datetime as dt
import random
import pickle
import os

## Login to DKB
dkb = DKB.login('ex3KB', username = 'root')
#mpa_context = dkb.new_context('mpa', 'mpa context')sn
# work in the mpa Context
#dkb.enter('mpa', 'W')

#####################
## Set (user-specific) data creation parameters
#####################
# Specify the numbers of as well as  other required information for the contexts, concepts and instances to be created
_prefix_start = 'Context_prefix'    # Prefix for '_reps' many Contexts (name)
_a_name = 'Concept_prefix'          # Prefix for Concepts (name)
_ConceptPID = 'ex3KB:kb:1:Concept'  # Expected PID for the predefined Concept Concept in ex3KB installation

#_verbose = True
_reps = 3							# Number of Contexts to be created
                                    # (= times whole workload (concept/ instance creation) is repeated)
_number_of_Concepts = 100		# Each Context (workload) will specify this '_number_of_Concepts' many Concepts
_instance_pop = 100				# Number of instances per Concept

mt_dic = {}     # Occasions when an empty dictionary is needed
_reqs = 2		# Number of required properties
# Possible string values to choose from for the required attribute
_strings = ['a', 'serendipity', 'Kathy Zuckerman', 'Tamzyn Macfarlane', "Yarner Atkinson Annear",
            'b', 'James Bond', 'Meryl Streep', 'Rui Zhao', 'Yuexuan Li', 'Aurora Constantin',
            'c', 'Rosa Filgueira', 'Amélie Levray', 'Kirsteen Elizabeth Morwenna Atkinson Annear',
            'd', 'Janna Mairi Gordon Atkinson', 'Lila Guthrie', 'Moya Guthrie']
# Whether it is possible to add attributes such as state and mutability to a concept
_attributes_working = True
# Whether state and mutability shall be randomly chosen from the above possible values
_state_settable = True
# Possible states in case the state can be set (given _state_settable = True)
_states = ['new', 'updated', 'valid', 'authoritative', 'depricated', 'discarded']
# Possible mutability entries in case they can be set (given _state_settable = True)
_mutabilities = ['mutable', 'no hjiding', 'frozen']


#####################
## This is the code, which actually takes care of the synthetic data creation
## and which calls the according functions and methods
#####################
for i in range(_reps):
    # In case you already created x Contexts, you will have to add this counter to i to avoid
    # the error message 'Context already exists'
#    i = i + 3
    the_prefix = _prefix_start + str(i)
    print("Context = " + the_prefix)
    #name = 'workloadTestRep' + str(i)
    #print(name)
    #a_context = dkb.new_context( name, the_prefix )

    #a_context = dkb.new_context( 'workload test rep ' + str(i), the_prefix )
    a_context = dkb.new_context( the_prefix, 'ContextTestWorkload' + str(i) )
    print("the_prefix = " + the_prefix + " a_context = " + a_context)
    dkb.enter( the_prefix, 'W')
#    print(dkb.status())
    spec_Concepts(i)
#    print(dkb.context_status()['concepts'])

    populate_Concepts(i)
#    print(dkb.context_status()['instances'])

dkb.close()

#####################
## The following methods are helper functions, which will be called during the data creation
#####################
_cons = []                   # Array of Concepts
_Concept_names = []          # Names of Concepts
# Method for specifying the Concepts in the i-th Context
def spec_Concepts(i: int):
    for k in range(_number_of_Concepts):
        nom = _a_name + str(k*1)        # Concept name: Given prefix + multiple 'x'
                                        # (multiple (k) of '1' to be added = current concept number)
        # Alternative:
        #nom = _a_name + k*'x'           # Concept name: Given prefix + multiple 'x'
                                        # (number of 'x' to be added = current concept number)
        _Concept_names.append(nom)      # Name added to Concept name dictionary
        descr = "Test load Concept " + nom + " in iteration " + str(i)    # Concept description including its name and the
                                                                          # current Context (= iteration) number
        req_attribs = {}
        for j in range(k):
            req_attribs.update({'attrib' + str(j*1):              # Specify the name and type (String or Integer) for the
                                'String' if j&1 else 'Integer'})  # required attributes (name: attrib + counter)
            # Alternative:
            #req_attribs.update({'attrib' + j*'y':                 # Specify the name and type (String or Integer) for the
            #                    'String' if j&1 else 'Integer'})  # required attributes (name: attrib + counter)
#        print('newConcept( ' + nom + ', ' + str(req_attribs) + ', ' + descr + ' )' )
        con = dkb.new_concept( nom,                               # Create the Concept with the given specification
                      required = req_attribs,
                      description = descr )
        print("Concept creation: " + str(k))
        _cons.append(con)                                         # Add the Concept to the Concept array
        rememberConcept(nom, _ConceptPID, req_attribs, mt_dic, mt_dic, mt_dic, con, descr)  # Call method 'rememberConcept'

_newConceptsDict = dict()		# A dictionary mapping names to tuples (filled through method 'rememberConcept')
# _inst_dic = dict()
def populate_Concepts(i: int):
    for k in range(_number_of_Concepts):
        # Extract the current (k) Concept's specification parameter
        name = _Concept_names[k]                    ## 1. Name
        pid = _cons[k]                              ## 2. PID
        required = _newConceptsDict[name][_reqs]    ## 3. Required attributes
        inst_id_base = 'a' + name + '_' + str(i) + '_' + str(k) + '_'
        for inst_no in range(_instance_pop):        # Go through loop as many times as instances shall be created
            inst_name = inst_id_base + str(inst_no) # Instance name: inst_id_base + counter
            attributes = {'name': inst_name}        # Add instance name to attributes
            properties = {}                         # Temporary collector for attributes which currently can't be set
            _state_set = False                      # Indicator whether state is set
            for attr in required:                   # For each of the required attributes
                choose_no = (i*k*inst_no + random.randint(0, 10000)) % _str_len              # Randomly create a number
                value = choose_no if required[attr] == 'Integer' else choose_str(choose_no)  # If attribute type 'Integer': take random number
                                                                                             # Else: pick string with index according to random number
                properties.update({attr: value})   # Update temporary attribute collector
            if _state_settable and random.randint(0,10) > 8:
                universal_properties = {'state': choose_state(),            # Call methods 'choose_state' and 'choose_mutability'
                                        'mutability': choose_mutability()}  # for randomly setting state and mutability attributes
                properties.update(universal_properties)                     # Update temporary attribute collector
                _state_set = True                                           # Adjust state indicator to TRUE
            if _attributes_working:                          # In case attribute update works
                attributes.update(properties)                # Update attributes according to temporary attribute collector
            try:                                                   # To overcome 'Internal Server error' message
                print("Concept " + str(k) + ": Instance creation " + str(i))
                newInstPID = dkb.new_instance(pid, **attributes)   # Create the instance according to above specification
#                print(newInstPID)
            except:
                pass
            # _other_attributes2check = {
            #                            'instanceOf': pid}
            # if not _state_set:
            #     _other_attributes2check.update({'pid': newInstPID, 'state': 'new',
            #                            'prefix': _prefix_start + str(i)})
            # attributes.update(_other_attributes2check)
            # _inst_dic.update({inst_name: attributes})
            # if _verbose:
            #     print_instance_creation( inst_name,
            #                              pid,
            #                              newInstPID,
            #                              attributes,   # the ones used
            #                              properties )  # would be included if _attributes_working


_str_len = len(_strings)           # Number of possible strings
# Method for returning one of the i strings
def choose_str(i: int) -> str:
    return _strings[i]

# Method for randomly choosing a possible state
def choose_state() -> str:     # chooses a possible state
    return _states[random.randint(0, len(_states) - 1)]

# Method for randomly choosing a possible state
def choose_mutability() -> str:
    return _mutabilities[random.randint(0, len(_mutabilities) - 1)]

def rememberConcept(         # Method for storing all concepts and their sepcifications in the _newConceptsDict dictionary
                             # (required for method 'populate_Concepts')
  # Concept specifications
  nom: str,                  # String the user-supplied name
  superPID: str,             # PID of the Concept it specialises
  reqDic: dict,              # A dictionary of the required name: Concept
  recDic: dict,              # A dictionary of the recommended attributes
  optionalDic: dict,         # A dictionary of the optional attributes
  translation: dict,         # A dictionary of attribute name translations
  pid: str,                  # The identifier returned by new_concept
  descr: str = '',           # The concept description (if there is one)
  ):
    global _newConceptsLDict   # Accumulate the above 8-tuples
    tup = (nom, superPID, reqDic, recDic, optionalDic, translation, pid, descr)
    _newConceptsDict.update({nom: tup})
#    print(_newConceptsDict)
