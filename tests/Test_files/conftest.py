
def pytest_addoption(parser):
    parser.addoption("--vv", action="store_true", help="diagnostic prints to standard out switched on")
    parser.addoption("--reps", action="store", type = int, default = 10, 
                      help = 'number of times to repeat the test load' )
                      
