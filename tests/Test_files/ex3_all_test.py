# stand alone test for get, covering both concepts and instances
# creating populations of these and then immediately getting them
# see ex3_retained_get_test.py for a similar test after in a separate run
# the context testing still needs to choose a different set of prefixes if running concurrently
# assume pipenv run dkb_manage init --reset ex3KB
# and pipenv run dkb_server
# have been run in a concurrent terminal

import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import logging
import os
import datetime 
import time
import sys 
import random
import pickle
from typing import Any, Dict, List, Tuple, Union

Attributes = Dict[str, Any]
# current permitted version of a query in find 
Test = Union[                # currently supported conditions a returned instance must meet
     Tuple[str,               # dyadic operator, currently only '=='
           str,               # the attribute to which it is applied
           Any],              # the value against which it is tested
     Tuple[str,               # monadic operator, currently 'isa' and 'isa_exactly'
           str]]              # the Identity of the concept it is an instance of
Query = Union[Test, List[Union[str,       # the logical combiner of the following tests, currently only 'AND'
           List[Test]]]]
PID = str

_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept
_Nil = ''
_site_id = 'ex3KB'
_prefix_base = 'test_get'
_user = 'mpa'
_session_id = ''
_concurrency_pad = ''        # will be filled in to let this run simultaneously with other instances of this test - not working yet!

# controls switching off parts of testing
_get_working = True            # set True to test get, etc.
_find_working = True
_only_these_working = True
_find_working_for_concepts = True
_value_as_a_name_working = True
_empty_only_these_working = True
_freeze_by_prefix_working = False 
_context_reset_working = True
_updated_still_new = True

dkb = None						# global shared DKB instance variable set at start
Fish = None						# test identity from new_concept used in getConcept
Eels = None
mtDic = dict()
_the_context = None             # the name of the Context in which Concepts and instances are created
_upper = None                   # the context where base concepts are specified
_middle = None                  # the context where the next level of concepts are specified

# fns to test context functionality

def eq_list(list1: list,list2: list, ordered: bool = True) -> bool:
    assert len(list1) == len(list2),\
        'list not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if ordered:
        for i in range(len(list1)):
            assert list1[i] == list2[i], "element " + str(i) + ' differs'
    else:
        for elem1 in list1:
            assert elem1 in list2, str(elem1) + ' not found in list2'
    return True

def force_new_context(prefix: str, title: str = 'No title supplied',
                      search_path: str = None,
                      owner: str = None) -> str:
    # perform new_context but deal with existing context of that name to enable re-runs on same dkb site 
    try: 
        return dkb.new_context(prefix, title = title, search_path = search_path, owner = owner)
    except Exceptions.ExistingContextError:  # need to clear up after the previous run
        dkb.context_reset(prefix)            # for re-runs if the DKB not reset
        return dkb.new_context(prefix, title = title, search_path = search_path, owner = owner)

def check_new_context():
    global _concurrency_pad
    # check can make a new context
    for i in range(100):                 # up to 100 concurrent tests
        try:
            con1 = dkb.new_context( _concurrency_pad + _concurrency_pad + 'mpa', title = 'ex3 testing' )  # pour moi
            break
        except Exceptions.ExistingContextError:  # another process is using this prefix name
            _concurrency_pad = 'Z' + str(i) + '_'
                                             # search path should default to kb
    assert con1 == _concurrency_pad + 'mpa', 'Should return a str that is the prefix'
    
    con2 = force_new_context( _concurrency_pad + 'mal', title = 'More ex3 testing', # pour moi aussi
                search_path = [_concurrency_pad + 'mpa'] ) # with an explicit search path
    assert con2 == _concurrency_pad + 'mal', "Prefix returned was not \'mal\' which is tres mal"
    
    con3 = force_new_context( _concurrency_pad + 'malcolm',             # pour moi encore
                title = 'With explicit owner ex3 testing',
                search_path = [_concurrency_pad + 'mpa', _concurrency_pad + 'mal'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    assert con3 == _concurrency_pad + 'malcolm', "Prefix should be \'malcolm\' it is \'" + con3 + "\'"
    dkb.enter(_concurrency_pad + 'mal', 'W')            
    con4 = force_new_context( _concurrency_pad + 'malcolm2',           # pour moi encore
                title = 'With implicit search path ex3 testing',
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    assert con4 == _concurrency_pad + 'malcolm2', "Prefix should be \'malcolm2\' it is \'" + con4 + "\'"
    con5 = force_new_context( 'testSearchPath', search_path = [_concurrency_pad + 'mal', _concurrency_pad + 'mpa'])
    assert con5 == 'testSearchPath', 'Prefix should be \'testSearchPath\''
    dkb.enter(_concurrency_pad + 'mpa', 'R')             #test enter again in a different program, assume OK here
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa'
    assert eq_list(sp1, ['kb']), '''mpa's search path not [\'kb\']'''
    
    dkb.enter(_concurrency_pad + 'mal', 'R')             #check mal's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal'
    assert eq_list(sp2, [_concurrency_pad + 'mpa']), '''mal's search path not [\'mpa\']'''
    
    dkb.enter(_concurrency_pad + 'malcolm', 'R')         #check malcolm's search path
    sp3 = dkb.get_search_path()		  # get search_path for Context 'malcolm'
    assert eq_list(sp3, [_concurrency_pad + 'mpa', _concurrency_pad + 'mal']), '''malcolm's search path not ['mpa', 'mal']'''
    
    dkb.enter(_concurrency_pad + 'malcolm2', 'R')        # check malcolm2's search path
    sp4 = dkb.get_search_path()		  # get search_path for Context 'malcolm2'
    assert eq_list(sp4, [_concurrency_pad + 'mal']), '''malcolm2\'s search path not [\'mal\']'''
    
    dkb.enter('testSearchPath', 'R')
    sp5 = dkb.get_search_path()		  # get search_path for Context 'malcolm2'
    assert eq_list(sp5, [_concurrency_pad + 'mal', _concurrency_pad + 'mpa']), '''testSearchPath\'s search path not [\'mal\', \'mpa\']'''
                                      # Finished checking search paths'

def check_enter():
    con1 = force_new_context( _concurrency_pad + 'mpa5', title = 'ex3 testing for enter' )      # pour moi 
    con2 = force_new_context( _concurrency_pad + 'mal5', title = 'More ex3 testing for enter', # pour moi aussi
                search_path = [_concurrency_pad + 'mpa5'] ) # with an explicit search path
    con3 = force_new_context( _concurrency_pad + 'malcolm5',             # pour moi encore
                title = 'With explicit owner ex3 testing for enter',
                search_path = [_concurrency_pad + 'mpa5', _concurrency_pad + 'mal5'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' )    
    # check that the Contexts can be entered
    # this checks set_search_path
    dkb.enter(_concurrency_pad + 'mpa5', 'W')            #test enter in a different program, assume OK here
    dkb.set_search_path( [_concurrency_pad + 'malcolm5'] ) # set search_path for Context 'mpa5'
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa5'
    assert eq_list(sp1, [_concurrency_pad + 'malcolm5']), '''mpa5\'s search path not [\'malcolm5\']''' 
    dkb.enter(_concurrency_pad + 'mal5', 'R')            # set mal4's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal5'
    assert eq_list(sp2, [_concurrency_pad + 'mpa5']), '''mal5\'s search path not [\'mpa5\']'''   
    dkb.enter(_concurrency_pad + 'malcolm5', 'R')        #check malcolm5's search path
    sp3 = dkb.get_search_path()		  
    assert eq_list(sp3, [_concurrency_pad + 'mpa5', _concurrency_pad + 'mal5']), '''malcolm4\'s search path not [\'mpa5\', \'mal5\']'''
    # try a revisit in a different mode
    dkb.enter(_concurrency_pad + 'mpa5', 'R')            #test enter in a different program, assume OK here
    sp4 = dkb.get_search_path()		  # get search_path for Context 'mpa5'
    assert eq_list(sp4, [_concurrency_pad + 'malcolm5']),'''mpa5\'s search path not [\'malcolm5\']'''
    dkb.enter(_concurrency_pad + 'mpa5', 'W')            # same Context different mode
    sp4 = dkb.get_search_path()		  # get search_path for Context 'mpa5'
    assert eq_list(sp4, [_concurrency_pad + 'malcolm5']),'''mpa5\'s search path not [\'malcolm5\']'''

def check_leave():
    dkb.enter(_concurrency_pad + 'mpa', 'R')
    con1 = dkb.new_context( _concurrency_pad + 'mpa7', title = 'ex3 testing leave')      # pour moi
    dkb.enter(_concurrency_pad + 'mpa7', 'R')            # enter new Context to test its search path
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa7'
    assert eq_list(sp1, [_concurrency_pad + 'mpa']), '''mpa7\'s search path not [\'mpa\']'''    
    # now test leave
    dkb.leave()
    con2 = dkb.new_context( _concurrency_pad + 'mal7', title = 'More ex3 testing of leave' ) # pour moi aussi
    dkb.enter(_concurrency_pad + 'mal7', 'R')            # inspect mal7's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal7'
    assert eq_list(sp2, ['kb']), '''mal7\'s search path not [\'kb\']'''

def check_get_search_path():
    con1 = force_new_context( _concurrency_pad + 'mpa2', title = 'ex3 testing get_search_path' )      # pour moi    
    con2 = force_new_context( _concurrency_pad + 'mal2', # pour moi aussi
                title = 'More ex3 testing get_search_path',
                search_path = [_concurrency_pad + 'mpa2'] ) # with an explicit search path 
    con3 = force_new_context( _concurrency_pad + 'malcolm3',             # pour moi encore
                title = 'With explicit owner ex3 testing get_search_path',
                search_path = [_concurrency_pad + 'mpa2', _concurrency_pad + 'mal2'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    # check that the search paths are as expected
    # this also checks get_search_path
    dkb.enter(_concurrency_pad + 'mpa', 'R')             #from previous run
    sp1 = dkb.get_search_path()		  # get search_path for Context _concurrency_pad + 'mpa'
    assert eq_list(sp1, ['kb']), '''mpa\'s search path not [\'kb\']'''  
    dkb.enter(_concurrency_pad + 'mpa2', 'R')            #test enter in ex3entertest.py
    sp1 = dkb.get_search_path()		  # get search_path for Context mpa2
    assert eq_list(sp1, [_concurrency_pad + 'mal7']), '''mpa2\'s search path not [\'mal7\'] prevailing at close of ex3_leave_test.py''' 
    dkb.enter(_concurrency_pad + 'mal2', 'R')            #check mal's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal'
    assert eq_list(sp2, [_concurrency_pad + 'mpa2']), '''mal2\'s search path not [\'mpa2\']'''  
    dkb.enter(_concurrency_pad + 'malcolm3', 'R')        #check malcolm's search path
    sp3 = dkb.get_search_path()		  # get search_path for Context 'malcolm3'
    assert eq_list(sp3, [_concurrency_pad + 'mpa2', _concurrency_pad + 'mal2']), '''malcolm3\'s search path not [\'mpa2\', \'mal2']'''

def check_set_search_path():
     # Make some contexts to do the test with
    con1 = force_new_context( _concurrency_pad + 'mpa4', title = 'ex3 testing set_search_path')      # pour moi  
    con2 = force_new_context( _concurrency_pad + 'mal4', # pour moi aussi
                title = 'More ex3 testing set_search_path',
                search_path = [_concurrency_pad + 'mpa4'] ) # with an explicit search path
    con3 = force_new_context( _concurrency_pad + 'malcolm4',             # pour moi encore
                title = 'With explicit owner ex3 testing set_search_path',
                search_path = [_concurrency_pad + 'mpa4', _concurrency_pad + 'mal4'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' ) 
    # check that the search paths are as expected
    # this checks set_search_path
    dkb.enter(_concurrency_pad + 'mpa4', 'W')            #test enter in a different program, assume OK here
    dkb.set_search_path( [_concurrency_pad + 'malcolm4'] ) # set search_path for Context 'mpa4'
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa4'
    assert eq_list(sp1, [_concurrency_pad + 'malcolm4']), '''mpa2\'s search path not [\'malcolm4\']'''  
    dkb.enter(_concurrency_pad + 'mal4', 'W')            # set mal4's search path
    dkb.set_search_path(['kb', _concurrency_pad + 'malcolm4']) # set search_path for Context 'mal4'
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal4'
    assert eq_list(sp2, ['kb', _concurrency_pad + 'malcolm4']), '''mal4\'s search path not [\'kb\', \'malcolm4'\']'''
    dkb.enter(_concurrency_pad + 'malcolm4', 'W')        #check malcolm4's search path
    dkb.set_search_path([])		      # set empty search path for Context 'malcolm4'
    sp3 = dkb.get_search_path()		  # set malcolm4 to only look locally
    assert eq_list(sp3, []), '''malcolm4\'s search path not []'''

def valid_context_status( res: Dict[str, Any],
                          explain: str,
                          expected: Dict[str, Any]) -> bool:
    assert type(res) == dict, "returned result not a dictionary for " + explain
    if False:
        print_context_status(res)
    for k in expected:
        assert k in res,  "Expected entry for \'" + k + "\' not in result for " + explain
        if k == 'mode':             # mode None implies not entered the context in either mode
            assert res[k] == expected[k], "mode does not have expected value for " + explain
            continue
        if expected[k] != None:     # use None to signal don't check
            assert res[k] == expected[k], "Value for " + k + " not that expected for " + explain
    return True

def try_context(prefix: str, expected: Dict[str, Any]):
    res = dkb.context_status(prefix)
    assert valid_context_status( res,
                         'an explicit enquiry about the state of ' + prefix,
                         expected), 'problems with the context_status for ' + prefix

def check_context_status():
    dkb.enter(_concurrency_pad + 'mpa', 'R')
    dic_kb = dkb.context_status()      
    assert valid_context_status(dic_kb,
                        'status of current context which is mpa',
                        {'identifier': 'ex3KB:mpa',
                        'owner': 'mpa',
                        'prefix': _concurrency_pad + 'mpa',
                        'search_path': ["kb"],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': 'R'}), "problems with default as current context"
    try_context(_concurrency_pad + 'mpa2', 
                {'identifier': 'ex3KB:mpa2',
                        'owner': 'mpa',
                        'prefix': _concurrency_pad + 'mpa2',
                        'search_path': [_concurrency_pad + 'mal7'],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': None})
    dkb.enter(_concurrency_pad + 'mpa2', 'R')
    try_context(_concurrency_pad + 'mpa2',             # test mode setting
                {'identifier': 'ex3KB:mpa2',
                        'owner': 'mpa',
                        'prefix': _concurrency_pad + 'mpa2',
                        'search_path': [_concurrency_pad + 'mal7'],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': 'R'})
    dkb.enter(_concurrency_pad + 'mpa2', 'W')
    try_context(_concurrency_pad + 'mpa2',             # test mode setting
                {'identifier': 'ex3KB:mpa2',
                        'owner': 'mpa',
                        'prefix': _concurrency_pad + 'mpa2',
                        'search_path': [_concurrency_pad + 'mal7'],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': 'W'})
    dkb.enter('kb', 'R')
    try_context('kb',             # test with foundation context
                {'identifier': 'ex3KB:kb',
                        'owner': 'root',
                        'prefix': 'kb',
                        'search_path': [],
                        'title': None,
                        'concepts': {'Concept': 'ex3KB:kb:1:Concept'},
                        'instances': {},
                        'mode': 'R'})
    all_contexts = dkb.status()['contexts_available']
    for con in all_contexts:
        dkb.enter(con, 'R')
        try_context(con,
                {'identifier': 'ex3KB:' + con,
                        'owner': None,
                        'prefix': con,
                        'search_path': None,
                        'title': None,
                        'concepts': None,
                        'instances': {},
                        'mode': 'R'})

def check_context_freeze():
    dkb.enter(_concurrency_pad + 'mpa2', 'W')
    dkb.context_freeze()
    try_context(_concurrency_pad + 'mpa2',             # test mode setting
                {'identifier': 'ex3KB:mpa2',
                        'owner': 'mpa',
                        'prefix': _concurrency_pad + 'mpa2',
                        'search_path': [_concurrency_pad + 'mal7'],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': 'R',
                        'state': 'frozen'})
    # writes should now result in an exception.
    with pytest.raises(Exceptions.WritingPermissionDeniedError):
        dkb.set_search_path( [_concurrency_pad + 'malcolm4'] )
    dkb.enter(_concurrency_pad + 'malcolm4', 'W')
    if _freeze_by_prefix_working:
        dkb.context_freeze(_concurrency_pad + 'mal')
        with pytest.raises(Exceptions.WritingPermissionDeniedError):
            dkb.enter(_concurrency_pad + 'mal', 'W')                # may be caught here
            dkb.set_search_path( [_concurrency_pad + 'malcolm4'] )  # or caught here

def check_context_deprecate():
    dkb.enter(_concurrency_pad + 'malcolm4', 'W')
    dkb.context_deprecate()
    try_context(_concurrency_pad + 'malcolm4',
        {'identifier': 'ex3KB:malcolm4',
         'owner': 'Malcolm.Atkinson@ed.ac.uk',
         'search_path': [],
         'state': 'deprecated',
         'title': 'With explicit owner ex3 testing set_search_path',
         'mode': 'W',
         'concepts': {  },
         'instances': {  }})
    if _freeze_by_prefix_working:
        dkb.context_deprecate(_concurrency_pad + 'mpa4')
        try_context(_concurrency_pad + 'mpa4',
        {'identifier': 'ex3KB:mpa4',
        'owner': 'mpa',
        'search_path': [_concurrency_pad + 'malcolm4'],
        'state': 'deprecated',
        'title': 'ex3 testing set_search_path',
        'mode': 'R',
        'concepts': {  },
        'instances': {  }})

_reset_instances = None
_Test_Con = None
def create_and_populate(acontext:str):
    global _reset_instances, _Test_Con
    dkb.enter(acontext, 'W')
    dkb.set_search_path([_concurrency_pad + 'mpa7', _concurrency_pad + 'mal7', _concurrency_pad + 'malcolm5'])
    TestCon = dkb.new_concept('TestCon', required = {"x": 'String', 'y': 'Integer'})
    tc1 = dkb.new_instance(TestCon, **{'name': 'tc1', 'x': 'a string', 'y': 5})
    tc2 = dkb.new_instance(TestCon, **{'name': 'tc2', 'x': 'b string', 'y': 4})
    tc3 = dkb.new_instance(TestCon, **{'name': 'tc3', 'x': 'c string', 'y': 3})
    tc4 = dkb.new_instance(TestCon, **{'name': 'tc4', 'x': 'd string', 'y': 2})
    tc5 = dkb.new_instance(TestCon, **{'name': 'tc5', 'x': 'e string', 'y': 1})
    tc6 = dkb.new_instance(TestCon, **{'name': 'tc6', 'x': 'f string', 'y': 0})
    tc7 = dkb.new_instance(TestCon, **{'name': 'tc7', 'x': 'a string', 'y': -1})
    _Test_Con = TestCon
    _reset_instances = {'tc1': tc1,
                      'tc2': tc2,
                      'tc3': tc3,
                      'tc4': tc4,
                      'tc5': tc5,
                      'tc6': tc6,
                      'tc7': tc7,
                        }
                        
_test_context = None
def try_reset_get(expected: Dict[str, str],    # the set of expected names and PIDs
    ignore_discarded: bool = False):             # the parameter relevant after reset
    if False:
        print( 'in try_reset_get with expected = ' + str(expected) + '\n ignore_discarded = ' + str(ignore_discarded))
    if expected == None:					   # after reset and with ignore_discard False
        with pytest.raises(Exceptions.InstanceNotFoundError):
            if False:
                print('_test_Con = ' + _Test_Con)
            res = dkb.get(_Test_Con)			
        for nom in _reset_instances:
            with pytest.raises(Exceptions.InstanceNotFoundError):
                res = dkb.get(_reset_instances[nom])
    elif ignore_discarded:
        if False:
                print('_test_Con = ' + _Test_Con)
        res = dkb.get(_Test_Con, only_these = ['name', 'prefix', 'state'], ignore_discarded = True)
        assert len(res) == 3, 'expected three attributes for concept TestCon'
        assert res['name'] == 'TestCon', "Expected the concept name to be TestCon"
        assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context
        assert res['state'] == 'discarded', 'state not set to discarded for concept TestCon'			
        for nom in expected:
            res = dkb.get(expected[nom], only_these = ['name', 'prefix', 'state'], ignore_discarded = True)
            assert len(res) == 3, 'expected three attributes for instance ' + nom
            assert res['name'] == nom, "Expected the instance name to be " + nom
            assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context + ' for instance ' + nom
            assert res['state'] == 'discarded', 'state not set to discarded for instance ' + nom
    else:                    # here explicit set expected and ignore_discarded is False
        if False:
            print('_test_Con = ' + _Test_Con)
        res = dkb.get(_Test_Con, only_these = ['name', 'prefix', 'state'])
        assert len(res) == 3, 'expected three attributes for concept TestCon'
        assert res['name'] == 'TestCon', "Expected the concept name to be TestCon"
        assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context
        assert res['state'] == 'new', 'state not set to new for concept TestCon'			
        for nom in expected:
            res = dkb.get(expected[nom], only_these = ['name', 'prefix', 'state'])
            assert len(res) == 3, 'expected three attributes for instance ' + nom
            assert res['name'] == nom, "Expected the instance name to be " + nom
            assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context + ' for instance ' + nom
            assert res['state'] == 'new', 'state not set to new for instance ' + nom

def try_reset_find(expected: Dict[str, str],     # the set of expected names and PIDs
    ignore_discarded: bool = False):             # the parameter relevant after reset
    if False:
        print( 'in try_reset_find with expected = ' + str(expected) + '\n ignore_discarded = ' + str(ignore_discarded))
    if len(expected) == 0:             # here when reset and not ignore_discard so exception InstanceNotFoundError raised
        if False:
            print('_test_Con = ' + _Test_Con)
        query = ('==', 'pid', _Test_Con)
        if False:
            print('_test_Con = ' + _Test_Con)
        res = dkb.find(query, only_these = ['name', 'prefix', 'state'])	
        assert res == [], 	"context_reset should have made concept TestCon disappear"
        for nom in range(1, 8):
            nom = 'tc' + str(nom)
            query = ('==', 'name', nom)
            res = dkb.find(query, only_these = ['name', 'prefix', 'state', 'pid'])  
            assert res == [], "context_reset should have made these disappear but " + nom + " is there"  
    elif ignore_discarded:
        if False:
            print('_test_Con = ' + _Test_Con)
        query = ('==', 'pid', _Test_Con)
        res = dkb.find(query, only_these = ['name', 'prefix', 'state'], ignore_discarded = True)
        assert len(res) == 1, 'exactly one instance of a concept TestCon'
        res = res.pop(0)          # unpack it
        assert len(res) == 3, 'three attributes of TestCon were asked for'
        assert res['name'] == 'TestCon', "Expected the concept name to be TestCon"
        assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context
        assert res['state'] == 'discarded', 'state not set to discarded for concept TestCon'			
        for nom in expected:
            query = ('==', 'name', nom)
            res = dkb.find(query, only_these = ['name', 'prefix', 'state', 'pid'], ignore_discarded = True)
            assert len(res) == 1, 'exactly one instance with that name ' + nom
            res = res.pop(0)
            assert len(res) == 4, "Four attributes of each instance were asked for"
            assert res['name'] == nom, "Expected the instance name to be " + nom
            assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context + ' for instance ' + nom
            assert res['state'] == 'discarded', 'state not set to discarded for instance ' + nom
            assert res['pid'] == expected[nom], nom + ' did not have the expected pid'
        query = ['OR', [
                 ("==", 'y', 5), ("==", 'y', 4), ("==", 'y', 3), ("==", 'y', 2),
                 ("==", 'y', 1), ("==", 'y', 0), ("==", 'y', -1)
                 ]]
        res = dkb.find(query, ignore_discarded = True)
        assert len(res) == 7, "Expected 7 pids in the list returned by " + str(query)
        for nom in expected:
            assert expected[nom] in res, "expected to find all instances with " + str(query)
    else:                    # here explicit set expected and ignore_discarded is False
        if _find_working_for_concepts:
            query = ('==', 'name', 'TestCon')
            if False:
                print('_test_Con = ' + _Test_Con + ' query = ' + str(query))
            res = dkb.find(query, only_these = ['name', 'prefix', 'state'])
            assert len(res) == 1, 'there is exactly one concept TestCon'
            query = ('==', 'pid', _Test_Con)
            if False:
                print('_test_Con = ' + _Test_Con + ' query = ' + str(query))
            res = dkb.find(query, only_these = ['name', 'prefix', 'state'])
            assert len(res) == 1, 'there is exactly one concept TestCon'
            res = res[0]
            assert len(res) == 3, 'expected three attributes for concept TestCon'
            assert res['name'] == 'TestCon', "Expected the concept name to be TestCon"
            assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context
            assert res['state'] == 'new', 'state not set to new for concept TestCon'			
        for nom in expected:
            query = ('==', 'name', nom)
            res = dkb.find(query, only_these = ['name', 'prefix', 'state', 'pid'])
            assert len(res) == 1, "should find exactly one instance for each name"
            res = res[0]
            assert len(res) == 4, 'expected three attributes for instance ' + nom
            assert res['name'] == nom, "Expected the instance name to be " + nom
            assert res["prefix"] == _test_context, "Expected the context prefix to be " + _test_context + ' for instance ' + nom
            assert res['state'] == 'new', 'state not set to new for instance ' + nom
            assert res['pid'] == expected[nom], nom + ' did not have the expected pid'
        query = ['OR', [
                 ("==", 'y', 5), ("==", 'y', 4), ("==", 'y', 3), ("==", 'y', 2),
                 ("==", 'y', 1), ("==", 'y', 0), ("==", 'y', -1)
                 ]]
        if False:
            print('in try_reset_find testing OR query ' + str(query))
        res = dkb.find(query)
        assert len(res) == 7, "Expected 7 pids in the list returned by " + str(query)
        for nom in expected:
            assert expected[nom] in res, "expected to find all instances with " + str(query)

def check_context_reset():
    global _test_context
    if not _context_reset_working:
        return
    _test_context = dkb.new_context(_concurrency_pad + 'test_context_reset', title = "populated to be reset, re-created and re-populated")
    create_and_populate(_test_context)
    try_context(_test_context, {
        'identifier': 'ex3KB:' + _test_context,
        'owner': 'mpa',
        'search_path': [_concurrency_pad + 'mpa7', _concurrency_pad + 'mal7', _concurrency_pad + 'malcolm5'],
        'state': 'active',
        'title': "populated to be reset, re-created and re-populated",
        'mode': 'W',
        'concepts': {'TestCon': _Test_Con  },
        'instances': _reset_instances} )
    try_reset_get(_reset_instances)
    try_reset_find(_reset_instances)
    dkb.context_reset()
    try_context(_test_context, {
        'identifier': 'ex3KB:' + _test_context,
        'owner': 'mpa',
        'search_path': [_concurrency_pad + 'mpa7', _concurrency_pad + 'mal7', _concurrency_pad + 'malcolm5'],
        'state': 'discarded',
        'title': "populated to be reset, re-created and re-populated",
        'mode': 'W',
        'concepts': {},
        'instances': {}} )
    #_test_context = dkb.new_context(_test_context, title = "been reset, to be re-created and re-populated")
    try_reset_get(None)
    try_reset_find([])
    try_reset_get(_reset_instances, ignore_discarded = True)
    try_reset_find(_reset_instances, ignore_discarded = True)
    create_and_populate(_test_context)
    try_context(_test_context, {
        'identifier': 'ex3KB:' + _test_context,
        'owner': 'mpa',
        'search_path': [_concurrency_pad + 'mpa7', _concurrency_pad + 'mal7', _concurrency_pad + 'malcolm5'],
        'state': 'active',
        'title': "populated to be reset, re-created and re-populated",
        'mode': 'W',
        'concepts': {'TestCon': _Test_Con  },
        'instances': _reset_instances} )
    try_reset_get(_reset_instances)
    try_reset_find(_reset_instances)

_targetRate = 0.15           # time to perform one cycle of Context operations in seconds
_context_reps = 20
def check_context_repititions():
    # run a repeated cycle of context operations to assess their speed.
    startTime = time.process_time()
    expectedContexts = []
    previousContext = _concurrency_pad + 'mpa'
    # build the right sized testing ground
    for i in range(_context_reps):
        dkb.enter(previousContext, 'W')
        newPrefix = _concurrency_pad + 'test' + str(i)
        expectedContexts.append(newPrefix)
        con = dkb.new_context(newPrefix,
                             title = 'Stress testing context functions ' + str(i))
        assert con == newPrefix, "new_context failed to return " + newPrefix
        dkb.enter(con, 'W')
        sp = dkb.get_search_path()
        assert eq_list(sp, [previousContext])
        previousContext = con
        sp.extend([_concurrency_pad + 'mal','kb'])
        dkb.set_search_path(sp)
        dkb.leave()
    # simulate mix 4 parts read to 1 part write at this scale
    for i in range(2):			# two lots of reads
        for p in expectedContexts:
            dkb.enter(p, 'R')
            sp2 = dkb.get_search_path()
    for p in expectedContexts:
        dkb.enter(p, 'W')
        sp3 = dkb.get_search_path()
        sp3.extend([_concurrency_pad + 'mpa2', _concurrency_pad + 'mal4'])
        dkb.set_search_path(sp3)
    for i in range(2):			# two lots of reads
        for p in expectedContexts:
            dkb.enter(p, 'R')
            sp2 = dkb.get_search_path()
    endTime = time.process_time()
    elapsedTime = endTime - startTime
    assert elapsedTime <= _context_reps*_targetRate, "Unit time too slow"		# ensure fast rate of working
    

def check_context_fns():
    # test all the functions that apply to a context
    check_new_context()
    check_enter()
    check_leave()
    check_get_search_path()
    check_set_search_path()
    check_context_status()
    check_context_freeze()
    check_context_deprecate()
    check_context_reset()
    check_context_repititions()
      
def check_context_exceptions():
    # test whether expected exceptions are raised
    #by new_context
    with pytest.raises(Exceptions.ExistingContextError):
        dkb.new_context(_concurrency_pad + 'mpa')
    with pytest.raises(Exceptions.ContextNotFoundError):
        con5 = dkb.new_context( _concurrency_pad + 'faulty1', title = 'Should not work',
                    search_path = ['nemo'] )	# cause ContextNotFoundError
    with pytest.raises(Exceptions.ContextNotFoundError):
       con6 = dkb.new_context(_concurrency_pad + 'faulty2', title = 'Should not work cyclically', 
                    search_path = ['faulty2'] )	# cause ContextNotFoundError
    with pytest.raises(Exceptions.IdentifierWrongError):   
        con7 = dkb.new_context('', "empty string test should fail" )	# empty string not a prefix
                       # IdentifierWrongError exception not raised for empty title
    with pytest.raises(Exceptions.IdentifierWrongError):
        con8 = dkb.new_context('*#€@&^' )	# disallowed characters in prefix
    #by enter
    with pytest.raises(Exceptions.ContextNotFoundError):
        dkb.enter( 'nemo', 'R' )	  # cause ContextNotFoundError
    with pytest.raises(TypeError):
        dkb.enter( _concurrency_pad + 'mpa5', 'X' )	  # Not R or W causes TypeError!
    #those for set_search_path
    dkb.enter(_concurrency_pad + 'mpa', 'W')
    with pytest.raises(Exceptions.ContextNotFoundError):
        dkb.set_search_path( ['nemo'] ) # cause ContextNotFoundError
    with pytest.raises(Exceptions.WritingPermissionDeniedError):
        dkb.enter(_concurrency_pad + 'mal4', 'R')
        dkb.set_search_path( [_concurrency_pad + 'malcolm4'] )
    
def check_status():
    # test whether status gives the correct information about the site
    status_dic = dkb.status()        # get a dictionary covering the status
    assert isinstance(status_dic, dict) # Should be a dictionary    
    assert 'contexts_available' in status_dic, "expected contexts available"  
    contexts_available = status_dic['contexts_available']
    assert len(contexts_available) >= 17, "expected at least 17 contexts from testing above"
    for context in contexts_available:
        assert isinstance(context, str), "each context should be represented by a string"
        dkb.enter(context, 'R')       # they should all be enterable in read mode
    assert 'current_context' in status_dic, "expect the current context as part of status"
    assert status_dic['current_context'] in contexts_available, "It should be one of those available"
    assert 'session_id' in status_dic, "expect a session_id in status result but not there"
    assert status_dic['session_id'] == _session_id, "The expected session_id \'" + _session_id +"\' was not returned"
    assert 'username' in status_dic, "status should say who the user is"
    assert status_dic['username'] == _user, "status should say the user was " + _user
    assert 'site_name' in status_dic, "status should tell us the site name"
    assert status_dic['site_name'] == _site_id, "status should say the site id is " + _site_id 

    # save a record of Concepts created in a form that is easily pickled and restored
_new_conceptsDict = dict()		# a dictionary mapping names to tuples
def remember_concept(
  nom,							# string its user-supplied name
  superPID,					    # the PID of the Concept it specialises
  req_dic,						# a dictionary of the required name: Concept
  recDic,						# a dictionary of the recommended attributes
  optionalDic, 					# a dictionary of the optional attributes
  translation,                  # a dictionary of attribute name translations
  pid,							# the identifier returned by new_concept
  descr = '',                   # the description if there is one
  ):
    global _new_conceptsLDict   	# accumulate 8-tuples there
    tup = (nom, superPID, req_dic, recDic, optionalDic, translation, pid, descr)
    _new_conceptsDict.update({nom: tup}) 
    
_name = 0		# index for name
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by new_concept
_descr = 7		# the description of the Concept

_verbose = True				# make True for debug diagnostics

_context_try = 0
def set_context():
    global _the_context, _context_try
    _the_context = _concurrency_pad + _prefix_base
    while True:
        try: 
            dkb.enter(_the_context, 'R')
            _the_context = _prefix_base + str(_context_try)  # if enter succeeds already used
            _context_try += 1
            continue
        except Exceptions.ContextNotFoundError:  # we are trying to get here!
            return              # it has not been used before

    # save a record of Concepts created in a form that is easily pickled and restored
_new_concepts_dict = dict()		# a dictionary mapping names to tuples
def remember_concept(
  nom: str,							# its user-supplied name
  superPID: str,					# the PID of the Concept it specialises
  req_dic: Dict[str,str],			# a dictionary of the required name: Concept
  recDic: Dict[str, str],			# a dictionary of the recommended attributes
  optionalDic: Dict[str, str], 		# a dictionary of the optional attributes
  translation: Dict[str, str],      # a dictionary of attribute name translations
  pid: str,							# the identifier returned by new_concept
  context: str,                     # the context where it is specified
  py_class: str,                    # a class that represents this Concept in Python
  methods: Dict[str, str],          # a mapping from method name to a Python method
  description: str = '',                  # the description if there is one
  ):
    global _new_conceptsLDict   	# accumulate 8-tuples there
    tup = (nom, superPID, req_dic, recDic, optionalDic, translation, pid, description, context,
    py_class, methods)
    _new_concepts_dict.update({nom: tup}) 
    
_name = 0		# index for name
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by new_concept
_descr = 7		# the description of the Concept
_context = 8    # where it was specified
_py_class = 9   # the corresponding Python class
_methods = 10   # a dictionary mapping method names to Python methods

_default_py_class = None

def create_concepts():
    # fully test the new_concept function
    dkb.enter(_upper, 'W')
    # try simple examples first
    CA = dkb.new_concept('CA') 		#default to top level specialisations of Concept
    CB = dkb.new_concept('CB')       # all 4 dictionaries default to empty
    CC = dkb.new_concept('CC')       # description defaults to the empty string
    # remember those in local list
    remember_concept('CA', _ConceptPID, mtDic, mtDic, mtDic, mtDic, CA, _upper, _default_py_class, mtDic)
    remember_concept('CB', _ConceptPID, mtDic, mtDic, mtDic, mtDic, CB, _upper, _default_py_class, mtDic)
    remember_concept('CC', _ConceptPID, mtDic, mtDic, mtDic, mtDic, CC, _upper, _default_py_class, mtDic)
    dkb.enter(_middle, 'W')
    # build specialisations to prepare for isa tests, etc.
    CAA = dkb.new_concept('CAA',
        specialises = 'CA') 		# specialisations of CA
    CAB = dkb.new_concept('CAB',
        specialises = 'CA')
    CAC = dkb.new_concept('CAC',
        specialises = 'CA')
    # remember those in local list
    remember_concept('CAA', CA, mtDic, mtDic, mtDic, mtDic, CAA, _middle, _default_py_class, mtDic)
    remember_concept('CAB', CA, mtDic, mtDic, mtDic, mtDic, CAB, _middle, _default_py_class, mtDic)
    remember_concept('CAC', CA, mtDic, mtDic, mtDic, mtDic, CAC, _middle, _default_py_class, mtDic)
    dkb.enter(_the_context, 'W')
    # build specialisations of those specialisations
    CAAA = dkb.new_concept('CAAA', # and again for testing isa inclusion later
        specialises = 'CAA')
    # remember that
    remember_concept('CAAA', CAA, mtDic, mtDic, mtDic, mtDic, CAAA, _the_context, _default_py_class, mtDic)    
    # now explore use of parameter required
    CAAAf = dkb.new_concept('CAAAf', # with one required attribute
        specialises = 'CAAA',
        required = {'f': 'CA'})	# one property f can be CA or any of its specialisations
    CAAAgh = dkb.new_concept('CAAAgh', # with two required attributes
        specialises = 'CAAA',
        required = {'g':'CA',	# one property g can be CA or any of its specialisations
          'h': 'CAC'})		# one property h can be CAC 
    CAAAij = dkb.new_concept('CAAAij', # test recursive structures
        specialises = 'CAAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
         'j': 'CAAAij'})		# and another
    # remember those in local list
    remember_concept('CAAAf', CAAA, {'f': CA}, mtDic, mtDic, mtDic, CAAAf, _the_context, _default_py_class, mtDic)
    remember_concept('CAAAgh', CAAA, {'g': CA, 'h': CAC}, 
                     mtDic, mtDic, mtDic, CAAAgh, _the_context, _default_py_class, mtDic)
    remember_concept('CAAAij', CAAA, {'i':CAAAij, 'j':CAAAij}, 
                     mtDic, mtDic, mtDic, CAAAij, _the_context, _default_py_class, mtDic)  
    # check use of parameter recommended
    CAAAijk = dkb.new_concept('CAAAijk', # add recommendations
        specialises = 'CAAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
        'j': 'CAAAij'},			# and another
        recommended = {'k':'CA'}) # and a recommended attribute 
    # remember that one
    remember_concept('CAAAijk', CAAA, {'i':CAAAij, 'j':CAAAij}, 
                     {'k':CA}, mtDic, mtDic, CAAAijk, _the_context, _default_py_class, mtDic)  
    # check use of parameter optional
    CAAAijkxxzz = dkb.new_concept('CAAAijkxxzz', # add optional
        specialises = 'CAAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
        'j': 'CAAAij'},			    # and another
        recommended = {'k':'CA'},   # and a recommended attribute 
        optional = {'xx': 'String', 'yy': 'Integer'}) #try builtin Concepts for base types
    # remember that one
    remember_concept('CAAAijkxxzz', CAAA, {'i':CAAAij, 'j':CAAAij}, 
                     {'k':CA}, {'xx': 'String', 'yy': 'Integer'}, 
                     mtDic, CAAAijkxxzz, _the_context, _default_py_class, mtDic)   
    # check use of parameter translation
    CAAAijkxxzzyy = dkb.new_concept('CAAAijkxxzzyy', # add optional
        specialises = 'CAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
        'j': 'CAAAij'},			    # and another
        recommended = {'k':'CA'},   # and a recommended attribute 
        optional = {'xx': 'String', 'yy': 'Integer'}, #try builtin Concepts for base types
        translation = {'name': 'skos:label'} )
    # remember that one
    remember_concept('CAAAijkxxzzyy', CAA, {'i':CAAAij, 'j':CAAAij}, 
                     {'k':CA}, {'xx': 'String', 'yy': 'Integer'}, 
                     {'name': 'skos:label'}, CAAAijkxxzzyy, _the_context, 
                     _default_py_class, mtDic)
    # check optional parameter description
    CAAl = dkb.new_concept(
      'CAAl',
      specialises = 'CAA',
      description = 'testing description as an optional parameter'
    )
    # remember that one
    remember_concept('CAAl', CAA, mtDic, 
                     mtDic, mtDic, mtDic, CAAl, 
                     _the_context, _default_py_class, mtDic,
                     description = 'testing description as an optional parameter', )   
    # check optional parameter methods
    CAAm = dkb.new_concept(
      'CAAm',
      specialises = 'CAA',
      methods = {'f1': 'malcolm/f1.py'},
      description = 'testing methods as an optional parameter'
    )
    # remember that one
    remember_concept('CAAm', CAA, mtDic, 
                     mtDic, mtDic, mtDic, CAAm, _the_context, 
                     _default_py_class, {'f1': 'malcolm/f1.py'},
                     description = 'testing methods as an optional parameter', )
    # check optional parameter class
    CAAn = dkb.new_concept(
      'CAAn',
      specialises = 'CAA',
      py_class = 'malcolm/a_class_example.py',
      description = 'testing class as an optional parameter'
    )
    # remember that one
    remember_concept('CAAn', CAA, mtDic, 
                     mtDic, mtDic, mtDic, CAAn, _the_context, 'malcolm/a_class_example.py', mtDic,
                     description = 'testing class as an optional parameter', )

def isa_concept(res: Attributes):  # given a dictionary returned by a get test if it is
                                # a specialisation directly or indirectly of Concept
    return res['instance_of'] == _ConceptPID
               
def eq_dic(dic1: Dict[str, Any], dic2:Dict[str, Any],     # true iff the two supplied dictionaries are equal
          name:str) -> bool:
    assert len(dic1) == len(dic2), 'The two dictionaries, \'' + str(dic1) + '\' and \'' + \
                      str(dic2) + '\' have different lengths for ' + name 
    for key in dic1:
        assert key in dic2, '\'' + key + '\' in ' + str(dic1) + \
                        ' but not in ' + str(dic2)
        assert dic1[key] == dic2[key], 'The values for \'' + key + '\' differ for ' + name
    return True

def valid_concept_attributes(
    res: Attributes,
    tup: Tuple[str, str,        # name, supertype
               Dict[str, str],  # required
               Dict[str, str],  # recommended
               Dict[str, str],  # optional
               Dict[str, str],  # translation
               str, str, str],  # pid, description, context
               only_these: List[str] = None): # specifying a specific subset of attributes
    if only_these != None:      # required subset specified
        assert isinstance(only_these, list), "internal fault in test, only_these should be a list"
        assert len(res) == len(only_these), "result does not contain the expected number of attributes"
        for attr_id in only_these:
            assert attr_id in res, "result does not contain attribute " + attr_id
        return True
    assert isa_concept(res), 'Not a Concept because \'instance_of\' != Concept Concept PID'
    assert 'name' in res, "No name atribute"
    if False:
        print('res = ' + str(res))
        print('tup = ' + str(tup))
        print('_name = ' + str(_name))
    assert res["name"] == tup[_name], "Name " + tup[_name] + " expected"
    assert 'specialises' in res, 'Attribute specialises missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['specialises'] == tup[_super], 'It specialises \'' + res['specialises'] + \
           '\' whereas it should specialise \'' + tup[_super] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'instance_of' in res, 'Attribute instance_of missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['instance_of'] == _ConceptPID, 'It is an instance of \'' + res['instance_of'] + \
           '\' whereas it should an instance of Concept, i.e., \''  + _ConceptPID + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'prefix' in res, 'Attribute prefix missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['prefix'] == tup[_context], 'It has prefix \'' + res['prefix'] + \
           '\' whereas it should say it was made in \'' + tup[_context] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'pid' in res, 'Attribute pid missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['pid'] == tup[_pid], 'PID \'' + res['pid'] + '\' not \'' + tup[_pid] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'state' in res, 'Attribute state missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['state'] == 'new', 'state \'' + res['state'] + '\' is not \'' + 'new' + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'timestamp' in res, 'No timestamp returned' + \
           ' for \'' + tup[_name] + '\''
    assert isinstance(res['timestamp'], datetime.datetime), "timestamp not a datetime" + \
           ' for \'' + tup[_name] + '\''
    assert 'mutability' in res, 'Attribute mutability missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['mutability'] == 'mutable', 'Attribute \'mutable\' not set tp \'mutable\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'required' in res, 'Attribute required missing' + \
           ' for \'' + tup[_name] + '\''
    assert eq_dic(res['required'], tup[_reqs], tup[_name] + '.required'), 'required is ' + str(res['required']) + \
                    ' but it should be ' + str(tup[_reqs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'recommended' in res, 'Attribute recommended missing' + \
           ' for \'' + tup[_name] + '\''
    assert eq_dic(res['recommended'], tup[_recs], tup[_name] + '.recommended'), 'recommended is ' + str(res['recommended']) + \
                    ' but it should be ' + str(tup[_recs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'optional' in res, 'Attribute optional missing' + \
           ' for \'' + tup[_name] + '\''
    assert eq_dic(res['optional'], tup[_opts], tup[_name] + '.optional'), 'optional is ' + str(res['optional']) + \
                    ' but it should be ' + str(tup[_opts]) + \
           ' for \'' + tup[_name] + '\''
    assert 'translation' in res, 'Attribute translation missing' + \
           ' for \'' + tup[_name] + '\''
    assert eq_dic(res['translation'], tup[_trans], tup[_name]), 'translation is ' + str(res['translation']) + \
                          ' but it should be ' + str(tup[_trans])
    assert 'description' in res, 'Attribute description missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['description'] == tup[_descr], 'description is \n' + str(res['description']) + \
                    '\nbut it should be\n' + str(tup[_descr]) + \
           ' for \'' + tup[_name] + '\''
    assert 'py_class' in res, 'Attribute py_class missing in ' + tup[_name]
    assert res["py_class"] == tup[_py_class], 'py_class value not as expected in ' + tup[_name]
    assert 'methods' in res, 'Attribute methods missing in ' + tup[_name]
    assert res['methods'] == tup[_methods], "Methods not correct for concept " + tup[_name]
    return True

def list_concatenate(l1: List[str], l2: List[str]) -> List[str]:
    res = []
    for item in l1:
        res.append(item)
    for item in l2:
        res.append(item)
    return res

_common_ids = ['pid', 'prefix', 'instance_of', 'name', 'timestamp', 'state', 'mutability']
_concept_ids = list_concatenate(_common_ids, ['specialises', 'required', 'recommended', 'optional',
                                   'translation', 'description', 'py_class', 'methods'])
def get_those_concepts():
    # fully test get on concepts except for disregarded cases
    # for all of them try all three forms of Identity and check the result
    if not _get_working:
        return
    for nom in _new_concepts_dict:
        expected_tup = _new_concepts_dict[nom]
        expected_prefix = expected_tup[_context]
        expected_pid = expected_tup[_pid]
        dkb.enter(expected_prefix, 'R')
        if False:
            current_context = dkb.context_status()
            print_context_status(current_context)
        res_by_name = dkb.get(nom)
        assert valid_concept_attributes(res_by_name, expected_tup), "Concept " + \
                                nom + ' incorrect'
        identity = expected_prefix + ':' + nom
        res_by_name_in_context = dkb.get(identity)
        assert valid_concept_attributes(res_by_name_in_context, expected_tup), "Concept " + \
                                identity + ' incorrect'
        if False:
            print( 'PID = ' + expected_pid )
        res_by_pid = dkb.get(expected_pid)
        assert valid_concept_attributes(res_by_pid, expected_tup), "Concept " + \
                                expected_pid + ' incorrect'
        wanted_ids = []         # try the empty only_these existence test
        if not _only_these_working:
            continue
        res_none = dkb.get(nom, only_these = wanted_ids)
        assert valid_concept_attributes(res_none, expected_tup, only_these = wanted_ids)
        for attr_id in _concept_ids:
            wanted_ids.append(attr_id)
            res_some = dkb.get(nom, only_these = wanted_ids)
            assert valid_concept_attributes(res_some, expected_tup, only_these = wanted_ids)

_new_instances_dict = dict()
def remember_instance(
        name: str, pid: str, attribs: Attributes,
        concept_name: str):
    _new_instances_dict.update({name: (pid, attribs, concept_name)})

_inst_pid = 0
_inst_attribs = 1
_inst_of = 2

def unused(name: str) -> str:
    while name in _new_instances_dict:
        name = 'z' + name
    return name

def merge_attributes(must_have: Attributes, more: Attributes) -> Attributes:
    for item in more:
        if not item in must_have:
            must_have.update({item: more[item]})
    return must_have

def make_instances(concept_name:str,
    attribs: Attributes):
    aname = unused('a' + concept_name)
    properties = merge_attributes({'name': aname}, attribs)
    if False:
        print('properties = ' + str(properties))
    aPID = dkb.new_instance(concept_name, **properties)
    remember_instance(aname, aPID, attribs, concept_name)
    bname = unused('b' + concept_name)
    properties = merge_attributes({'name': bname}, attribs)
    if False:
        print('properties = ' + str(properties))
    bPID = dkb.new_instance(concept_name, **properties)
    remember_instance(bname, bPID, attribs, concept_name)
    cname = unused('c' + concept_name)
    properties = merge_attributes({'name': cname}, attribs)
    if False:
        print('properties = ' + str(properties))
    cPID = dkb.new_instance(concept_name, **properties)
    remember_instance(cname, cPID, attribs, concept_name)

_instances_home = ''
def create_instances():
    global _instances_home, _concurrency_pad
    # fully test new_instance function
    # build multiple instances of multiple concepts to explore the variety of options
    try: 
        _instances_home = dkb.new_context(_concurrency_pad + 'instances', 'make new instances here ready to test get try ' + str(_context_try))
    except Exceptions.ExistingContextError:  # need to clear up after the previous run
        dkb.context_reset(_concurrency_pad + 'instances')
        #_instances_home = dkb.new_context(_concurrency_pad + 'instances', 'make new instances here after reset ready to test get try ' + str(_context_try))
    dkb.enter(_instances_home, 'W')
    dkb.set_search_path([_the_context])  # to test the search for names
    make_instances('CA', dict())
    make_instances('CB', dict())
    make_instances('CC', dict())
    make_instances('CAA', dict())
    make_instances('CAB', dict())
    make_instances('CAC', dict())
    make_instances('CAAA', dict())        # check isa constraint correctly checked
    make_instances('CAAAf', {'f': _new_instances_dict['aCA'][_inst_pid]})
    make_instances('CAAAf', {'f': _new_instances_dict['aCAA'][_inst_pid]})
    make_instances('CAAAf', {'f': _new_instances_dict['aCAB'][_inst_pid]})
    make_instances('CAAAf', {'f': _new_instances_dict['aCAC'][_inst_pid]})
    make_instances('CAAAf', {'f': _new_instances_dict['aCAAA'][_inst_pid]})
    if _value_as_a_name_working:
        make_instances('CAAAf', {'f': 'aCAAAf'})  # identified by name as well as by PID
        make_instances('CAAAgh', {'g': 'aCA', 'h': 'bCAC'})
    make_instances('CAAAij', {'i': _Nil, 'j': _Nil})
    if _value_as_a_name_working:
        make_instances('CAAAijk', {'i': 'aCAAAij', 'j': 'bCAAAij', 'k': 'aCAA'})
        make_instances('CAAAijkxxzz', {'i': 'aCAAAij', 'j': 'bCAAAij', 'k': 'aCAA',
                                'xx': 'a simple string',
                                'yy': -512376})
    make_instances('CAAA', dict())

def attrib_union(supplied: Attributes,
                 others: List[str]) -> List[str]:
    res = []
    for id in supplied:
        res.append(id)
    for id in others:
        if not id in res:
            res.append(id)
    return res

def check_attribute(id: str,
            res: Attributes,
            supplied: Attributes,
            instance_name: str) -> bool:
    assert id in res, 'Attribute ' + id + ' expected in instance ' + instance_name
    if id in supplied:
        assert res[id] == supplied[id], 'Supplied attribute ' + id + ' does not have expected value in ' + instance_name
        return True
    if id == 'instance_of':
        concept = _new_instances_dict[instance_name][_inst_of]
        tup = _new_concepts_dict[concept]
        pid = tup[_pid]
        assert res[id] == pid, instance_name + ' should be an instance of ' + concept[_name]
    elif id == 'prefix':
        assert res[id] == _concurrency_pad + 'instances', 'For instance ' + instance_name + ' the context should be instances'
    elif id == 'pid':
        assert res[id] == _new_instances_dict[instance_name][_inst_pid], 'The PID for ' + instance_name + ' is incorrect'
    elif id == 'state':
        assert res[id] == 'new', 'The state for ' + instance_name + ' was not new'
    elif id == 'timestamp':
        assert isinstance(res[id], datetime.datetime), "timestamp not a datetime for " + instance_name
    elif id == 'mutability':
        assert res[id] == 'mutable', instance_name + " is not marked mutable"
    return True

def valid_instance(
        res: Attributes,
        supplied: Attributes,
        name: str,
        only_these: List[str] = None) -> bool:
    if only_these != None:
        assert len(res) == len(only_these), 'get result not of the length specified by only_these'
        for id in only_these:
            assert check_attribute(id, res, supplied, name), "Attribute " + id + " in " + name + " is wrong"
        return True
    attrib_set = attrib_union(supplied, _common_ids)
    if False:
        print('attrib_set = ' + str(attrib_set))
        print('res = ' + str(res))
    assert len(res) == len(attrib_set), 'wrong number of attributes returned for ' + name
    for id in attrib_set:
        assert check_attribute(id, res, supplied, name), "Attribute " + id + " in " + name + " is wrong"
    return True

def get_those_instances():
    # fully test get on instances except for disregarded cases
    if not _get_working:
        return
    if False:
        current_context = dkb.context_status()
        print_context_status(current_context)
    for nom in _new_instances_dict:
        res_by_name = dkb.get(nom)
        the_tup = _new_instances_dict[nom]
        assert valid_instance(res_by_name, the_tup[_inst_attribs], nom), 'problem with get by name result for ' + nom   
        res_by_instance_colon_name = dkb.get('instances:' + nom)
        assert valid_instance(res_by_instance_colon_name, the_tup[_inst_attribs], nom), 'problem with get by instance:name result for ' + nom   
        res_by_pid = dkb.get(the_tup[_inst_pid])
        assert valid_instance(res_by_pid, the_tup[_inst_attribs], nom), 'problem with get result by pid for ' + nom
        attrib_set = attrib_union(the_tup[_inst_attribs], _common_ids)
        wanted = []
        if not _only_these_working:
            continue
        if _empty_only_these_working:
            res_none = dkb.get(nom, only_these = wanted)
            if False:
                print('res_none = ' + str(res_none))
                print('wanted = ' + str(wanted))
            assert valid_instance(res_none, the_tup[_inst_attribs], nom, only_these = wanted), 'problem with get result by pid for ' + nom +\
                                      ' with only_these = ' + str(wanted)
        for attrib in attrib_set:
            wanted.append(attrib)
            res_some = dkb.get(nom, only_these = wanted)
            if False:                                # switch on with if False:
                print('res_some = ' + str(res_some))
                print('wanted = ' + str(wanted))
            assert valid_instance(res_some, the_tup[_inst_attribs], nom, only_these = wanted), 'problem with get result by pid for ' + nom +\
                                      ' with only_these = ' + str(wanted)
_line_width = 100
_sep = ', '
def vprint_mc( hdr: str, body: Union[list, dict],
               sep: str = _sep, line_width = _line_width):
    _sep_len = len(_sep)
    if not _verbose:
        return
    used = len(hdr) + 3
    line = hdr + ' '
    todo = len(body)
    if type(body) == list:
        line += '[ '
        for nxt in body:
            nxt_item = str(nxt)
            todo -= 1
            if todo > 0:
                nxt_item += sep
            uses = len(nxt_item)
            if used + uses > line_width:
                print(line)
                used = uses
                line = nxt_item
            else:
                line += nxt_item
                used += uses
        line += ' ]'
        print(line)
    elif type(body) == dict:
        line += '{ '
        for nxt in body:
            val = str(body[nxt])
            todo -= 1
            nxt_item = str(nxt) + ': ' + val
            if todo > 0:
                nxt_item += sep
            uses = len(nxt_item)
            if used + uses > line_width:
                print(line)
                used = uses
                line = nxt_item
            else:
                line += nxt_item
                used += uses
        line += ' }'
        print(line)
    else:
        print(line + ' body is ' + str(type(body)))
        print(str(body))

def print_context_status(stat: Dict[str, Any]):
    print('\nStatus for ' + stat["prefix"])
    print('identifier: ' + stat["identifier"])
    print('owner: ' + stat["owner"])
    print('search_path: ' + str(stat["search_path"]))
    print('state: ' + stat["state"])
    print('title: ' + stat["title"])
    print('mode: ' + str(stat["mode"]))
    vprint_mc('concepts:', stat["concepts"])
    vprint_mc('instances:', stat["instances"])

def try_concept_group_find(
    query: Query,
    target: List[str],                # the names of the expected concepts
    pid_only: bool = True,            # return list of pids, False => return Attributes for each instance
    only_these: List[str] = None,     # the ids of the required attributes
    ignore_discards: bool = True	
    ):
    if False:
        print('in try_concept_group_find: query = ' + str(query) + ' target = ' +str(target))
        print('pid_only = ' + str(pid_only) + ' only_these = ' + str(only_these) + ' ignore_discards = ' +str(ignore_discards))
    expected_len = len(target)
    if only_these != None:
        if not _only_these_working:
            return
        res_list = dkb.find(query, only_these = only_these)
        assert len(in_upper) == expected_len, 'find with query ' + str(query) + ' should return ' + \
                                               str(expected_len) + ' results with only_these as ' + str(only_these)
        res_index = 0                     # assume in same order
        for item in target:
            tup = _new_concepts_dict[item]
            res = res_list[res_index]
            res_index += 1
            valid_concept_attributes(res, tup, only_these = only_these)            
    elif pid_only:
        res = dkb.find(query)
        assert len(res) == expected_len, 'find with query ' + str(query) + ' should return ' + str(expected_len) + ' results'
        for item in target:
            assert _new_concepts_dict[item][_pid] in res, 'Concept ' + item + ' was expected as a result of ' + str(query)
    else:
        res_list = dkb.find(query, pid_only = False) 
        assert len(in_upper) == expected_len, 'find with query ' + str(query) + ' should return ' + \
                                               str(expected_len) + ' results with pid_only = False'
        for res in res_list:
            nom = res['name']
            assert nom in target, 'find with query ' + str(query) + ' and pid_only = False returned attributes for concept ' + \
                                  nom + ' which was not expected'
            tup = _new_concepts_dict[nom]
            valid_concept_attributes(res, tup)

def try_concept_group_in_context(context_prefix: str,
    target: List[str]):                   # the names of the concepts that should be found
    query = ('==', 'prefix', context_prefix)   # should get 3 concepts: ['CA', 'CB', 'CC'], etc.
    if False:
        print('in try_concept_group_in_context query = ' + str(query))
    try_concept_group_find(query, target)
    query = ['AND', [('==', 'prefix', context_prefix), ('isa', 'Concept')]]   # should get 3 concepts: ['CA', 'CB', 'CC']
    try_concept_group_find(query, target)
    query = ['AND', [('==', 'prefix', context_prefix), ('isa_exactly', 'Concept')]]   # should get 3 concepts: ['CA', 'CB', 'CC']
    try_concept_group_find(query, target)
    
def try_concepts():
    if not _find_working_for_concepts:
        return
    for nom in _new_concepts_dict:
        tup = _new_concepts_dict[nom]
        pid = tup[_pid]
        nom_saved = tup[_name]      # the saved Concept name
        prefix = tup[_context]
        assert nom == nom_saved, 'inexplicable inconsitency in saved Concept data'
        query = ('==', 'name', nom)   # should get one in each case
        if False:
            print( '\nTrying query ' + str(query) + ' with nom = ' + nom + ' and prefix = ' + prefix )
        res = dkb.find(query)
        assert type(res) == list, 'Expected a list of PIDs for name ' + nom
        assert len(res) == 1, nom + ' did not retrieve exactly one Concept'
        assert res[0] == pid, 'Returned PID did not match remembered PID for ' + nom
        res = dkb.find(query, pid_only = False)
        assert len(res) == 1, nom + ' did not retrieve exactly one Concept for pid_only = False'
        res_spec = res[0]        # list of dictionaries like the ones returned by get expected
        assert valid_concept_attributes(res_spec, tup), 'get result for \'' + nom + '\' not as expected'
        wanted = ['name', 'pid', 'prefix', 'specialises', 'description']
        otlen = len(wanted)
        res = dkb.find(query, only_these = wanted)
        assert len(res) == 1, nom + ' did not retrieve exactly one Concept for only_these = ' + str(wanted)
        res_spec = res[0]
        assert len(res_spec) == otlen, nom + ' did not retrieve a dictionary of the right length'
        for prop in wanted:
            assert prop in res_spec, 'Property ' + prop + " not found in " + nom
        assert res_spec['name'] == nom, nom + " did not retrieve a correct name"
        assert res_spec['pid'] == pid, nom + "did not contain the right PID"
        assert res_spec['prefix'] == prefix, nom + "did not contain the right prefix " + prefix
        assert res_spec['specialises'] == tup[_super], nom + ' should specialise ' + tup[_super]
        assert res_spec['description'] == tup[_descr], nom + ' desciption not ' + tup[_descr]
    # now find groups of concepts
    target = ['CA', 'CB', 'CC']
    try_concept_group_in_context(_upper, target)
    target = ['CAA', 'CAB', 'CAC']
    try_concept_group_in_context(_middle, target)
    target = ['CAAA', 'CAAAf', 'CAAAgh', 'CAAAij', 'CAAAijk', 'CAAAijkxxzz', 
              'CAAAijkxxzzyy', 'CAAl', 'CAAm', 'CAAn']
    try_concept_group_in_context(_the_context, target)

def try_all_in_instances():
    query = ("==", 'prefix', _concurrency_pad + 'instances')
    res_list = dkb.find(query)
    assert len(res_list) == len(_new_instances_dict), "expected every instance to be found by query " + \
                                                      str(query)
    for nom in _new_instances_dict:
        assert _new_instances_dict[nom][_inst_pid] in res_list, "The pid for " + nom + \
                     " does not appear in result from query " + str(query)
    # add here pis_only = false and only_these

def select_instance_eq(field: Union[int, str], value: Any, already_have: List[str]) -> List[str]:
    if False:
        print('in select_instance_eq field = ' + str(field) + ' value = ' + str(value))
        print('already_have = ' + str(already_have))
    if isinstance(field, int):
        for nom in _new_instances_dict:
            tup = _new_instances_dict[nom]
            if False:
                print('tup = ' + str(tup))
            if tup[field] == value:
                already_have.append(tup[_inst_pid])
    else:
        for nom in _new_instances_dict:
            tup = _new_instances_dict[nom]
            attribs = tup[_inst_attribs]
            if field in attribs and attribs[field] == value:
                already_have.append(tup[_inst_pid])
    return already_have

def validate_group(query: Query, expected: List[str]):
    if False:
        print('in validate_group query = ' + str(query))
        print('expected = ' + str(expected))
    res = dkb.find(query)
    if False:
        print('res = ' + str(res))
    assert eq_list(res, expected, ordered = False), "result " + str(res) + " != " + str(expected)

def union_lists(l1, l2: List[Any]) -> List[Any]:
    for elem in l2:
        if not elem in l1:
            l1.append(elem)
    return l1

def make_everything() -> List[PID]:
    # generate a list of all the PIDs of every instance including concepts.
    _target_contexts = ['kb', _concurrency_pad + 'instances', _concurrency_pad + 'test_context_reset',
                         _concurrency_pad + 'test_get', _concurrency_pad + 'test_get_middle',
                         _concurrency_pad + 'test_get_upper']
    res = []
    for context in _target_contexts:
        con_stat = dkb.context_status(context)
        concepts_dic = con_stat['concepts']
        for nom in concepts_dic:
            res.append(concepts_dic[nom])
        instances_dic = con_stat['instances']
        for nom in instances_dic:
            res.append(instances_dic[nom])
    return res

def diagnostic_test_f(suspects: List[str]):
    # investigate f values in suspects
    print('\n testing the attribute f in suspects ' + '\n' +
        'pid\t\t\t\tf\n')
    for suspect in suspects:
        res = dkb.get(suspect, only_these = ['name', 'pid', 'f'])
        print(res['pid'] + '\t' + res['f'])

def try_groups_of_instances():
    for id in _new_concepts_dict:
        pid = _new_concepts_dict[id][_pid]
        query = ('isa_exactly', pid )
        expected = select_instance_eq(_inst_of, id, [])
        validate_group(query, expected)
    # isa_exctly tested - try redundant extra conditions 
    CA_pid = _new_concepts_dict['CA'][_pid]
    CA_set = select_instance_eq(_inst_of, 'CA', [])
    query = ['AND', [('isa_exactly', CA_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CA_set)
    #CA_set = select_instance_eq(_inst_of, 'CA', [])
    query = ['AND', [('==', 'prefix', _instances_home), ('isa_exactly', CA_pid )]]
    validate_group(query, CA_set)
    #CA_set = select_instance_eq(_inst_of, 'CA', [])
    query = ['AND', [('isa_exactly', CA_pid ), ('==', 'prefix', _instances_home), ('==', 'state', 'new')]]
    validate_group(query, CA_set)
    CAA_pid = _new_concepts_dict['CAA'][_pid]
    CAA_set = select_instance_eq(_inst_of, 'CAA', [])
    query = ['AND', [('isa_exactly', CAA_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CAA_set)
    CA_plus_CAA_set = list_concatenate(CA_set, CAA_set)
    CAAA_pid = _new_concepts_dict['CAAA'][_pid]
    CAAA_set = select_instance_eq(_inst_of, 'CAAA', [])
    query = ['AND', [('isa_exactly', CAAA_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CAAA_set)
    CA_plus_CAA_plus_CAAA_set = list_concatenate(CA_plus_CAA_set, CAAA_set)
    CAAAf_pid = _new_concepts_dict['CAAAf'][_pid]
    CAAAf_set = select_instance_eq(_inst_of, 'CAAAf', [])
    query = ['AND', [('isa_exactly', CAAAf_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CAAAf_set)
    CA_plus_CAA_plus_CAAA_plus_CAAAf_set = list_concatenate(CA_plus_CAA_plus_CAAA_set, CAAAf_set)
    CAAAgh_pid = _new_concepts_dict['CAAAgh'][_pid]
    CAAAgh_set = select_instance_eq(_inst_of, 'CAAAgh', [])
    query = ['AND', [('isa_exactly', CAAAgh_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CAAAgh_set)
    CA_plus_CAA_plus_CAAA_plus_CAAAf_plus_CAAAgh_set = list_concatenate(CA_plus_CAA_plus_CAAA_plus_CAAAf_set, CAAAgh_set)
    CAAAij_pid = _new_concepts_dict['CAAAij'][_pid]
    CAAAij_set = select_instance_eq(_inst_of, 'CAAAij', [])
    query = ['AND', [('isa_exactly', CAAAij_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CAAAij_set)
    ij_set = list_concatenate(CA_plus_CAA_plus_CAAA_plus_CAAAf_plus_CAAAgh_set, CAAAij_set)
    CAAAijk_pid = _new_concepts_dict['CAAAijk'][_pid]
    ijk_set = select_instance_eq(_inst_of, 'CAAAijk', [])
    query = ['AND', [('isa_exactly', CAAAijk_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, ijk_set)
    all_set = list_concatenate(ij_set, ijk_set)
    CAAAijkxxzz_pid = _new_concepts_dict['CAAAijkxxzz'][_pid]
    ijkxxzz_set = select_instance_eq(_inst_of, 'CAAAijkxxzz', [])
    query = ['AND', [('isa_exactly', CAAAijkxxzz_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, ijkxxzz_set)
    all_set = list_concatenate(all_set, ijkxxzz_set)
    CAB_pid = _new_concepts_dict['CAB'][_pid]
    CAB_set = select_instance_eq(_inst_of, 'CAB', [])
    query = ['AND', [('isa_exactly', CAB_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CAB_set)
    all_set = list_concatenate(all_set, CAB_set)
    CAC_pid = _new_concepts_dict['CAC'][_pid]
    CAC_set = select_instance_eq(_inst_of, 'CAC', [])
    query = ['AND', [('isa_exactly', CAC_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, CAC_set)
    all_set = list_concatenate(all_set, CAC_set)
    query = ['AND', [('isa', CA_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, all_set)
    query = ['OR', [('isa', CA_pid )]]
    validate_group(query, all_set)
    everything = make_everything()
    query = ['OR', [('isa', CA_pid ), ('==', 'prefix', _instances_home), ('==', 'state', 'new')]]
    validate_group(query, everything)
    query = ['OR', [('==', 'state', 'new'), ('isa', CA_pid ), ('==', 'prefix', _instances_home)]]
    validate_group(query, everything)
    query = ['OR', [('==', 'prefix', _instances_home), ('==', 'state', 'new'), ('isa', CA_pid )]]
    validate_group(query, everything)
    query = ['OR', [('isa_exactly', CA_pid ), ('isa_exactly', CAA_pid ), ('isa_exactly', CAAA_pid ), 
                   ('isa_exactly', CAAAf_pid ), ('isa_exactly', CAAAgh_pid ), ('isa_exactly', CAAAij_pid ),
                   ('isa_exactly', CAAAijk_pid ), ('isa_exactly', CAAAijkxxzz_pid ),
                   ('isa_exactly', CAB_pid ), ('isa_exactly', CAC_pid )]]
    validate_group(query, all_set)
    query = ['OR', [('==', 'f', _new_instances_dict['aCA'][_inst_pid]),
                   ('==', 'f', _new_instances_dict['aCAA'][_inst_pid]),
                   ('==', 'f', _new_instances_dict['aCAB'][_inst_pid]),
                   ('==', 'f', _new_instances_dict['aCAC'][_inst_pid]),
                   ('==', 'f', _new_instances_dict['aCAAA'][_inst_pid]),
                   ('==', 'f', _new_instances_dict['aCAAAf'][_inst_pid])
                   ]]
    if False:
        diagnostic_test_f(CAAAf_set)
    validate_group(query, CAAAf_set)

def try_instances():
    for nom in _new_instances_dict:
        query = ("==", 'name', nom)
        try:
            res_list = dkb.find(query)
            assert len(res_list) == 1, 'Expected 1 result from query ' + str(query)
            pid = _new_instances_dict[nom][_inst_pid]
            assert pid in res_list, "Did not find correct pid with query " + str(query)
            res_list = dkb.find(query, pid_only = False)
            tup = _new_instances_dict[nom]
            attribs = tup[_inst_attribs]
            assert len(res_list) == 1, 'Expected 1 result from query ' + str(query) + \
                                   ' with pid_only = False'
            valid_instance(res_list[0], attribs, nom)
            if not _only_these_working:
                continue
            wanted = []
            for attrib in _common_ids:
                wanted.append(attrib)
                res_list = dkb.find(query, only_these = wanted)
                assert len(res_list) == 1, 'Expected 1 result from query ' + str(query) + \
                                   ' with only_these = ' + str(wanted)
                valid_instance(res_list[0], attribs, nom, only_these = wanted)
        except exceptions.DeprecationWarning:
            print('find(' + str(query) + ') retrieved a deprecated instance ' + str(res))
    try_all_in_instances()
    # add here the finding groups of _instances_home
    try_groups_of_instances()

def find_those_instances_and_concepts():
    # fully test the find function except for discarded cases
    if not _find_working:
        return
    try_concepts()                 # separated so can be commented out
    try_instances()    

def make_Nil():
    global _Nil
    # create a value that indicates an unfilled reference compatible with all concept types
    _Nil = ''

_update_context = ''
_update_pop_size = 10
_update_initial_instances = {}
_update_versions = ['zero', 'one', 'two', 'three', 'four', 'five' ]
def try_update():
    global _update_context, _update_initial_instances
    _update_context = dkb.new_context(_concurrency_pad + 'updates', "Used to test update functionality")
    UC = dkb.new_concept('UC', required = {'n': 'Integer', 'version': 'String', 'q': 'Integer'})
    for i in range(_update_pop_size):
        name = 'uc' + str(i)
        inst_pid = dkb.new_instance(UC, **{'name': name, 'n': 2**i, 'version': 'zero', 'q': i})
        _update_initial_instances.update({name: inst_pid})
    previous_instances = _update_initial_instances
    for v in range(5):
        this_version = _update_versions[v]
        its_instances = {}
        i = 0
        for nom in _update_initial_instances:
            prev_attribs = dkb.get(nom, only_these = ['n'])
            old_n = prev_attribs['n']
            if _verbose:
                print('in try_update v = ' + str(v) + ' old_n = ' + str(old_n) + ' this_version = ' + this_version )
            new_pid = dkb.update(nom, n = old_n + 1, version = this_version)
            its_instances.update({nom: new_pid})
            new_attribs = dkb.get(nom)        #only_these = ['n', 'version', 'state', 'predecessor'])
            if _verbose:
                print('new_attribs = ' + str(new_attribs))
            assert new_attribs['n'] == old_n + 1, "n not updated correctly"
            assert new_attribs['version'] == this_version, "version not updated correctly"
            assert new_attribs['state'] == 'updated', "state not set to updated"
            assert new_attribs["predecessor"] == previous_instances[nom]
            prev_instance = dkb.get(previous_instances[nom], only_these = ['successor'])
            assert prev_instance["successor"] == new_pid, "The successor of the previous version not set correctly"
            i += 1
        previous_instances = its_instances
        for j in range(_update_pop_size):
            name = 'uc' + str(j)
            query = ('==', 'name', name)
            if _verbose:
                print('query = ' + str(query) + ' name = ' + name + ' this_version = ' + this_version )
            res = dkb.find(query, only_these = ['name', 'state', 'version', 'q'])
            if _verbose:
                print('res = ' + str(res))
            assert len(res) == 1, "There should be only one instance with name = " + name
            res = res[0]
            assert res['name'] == name, "returned instance did not have name " + name
            assert res['version'] == this_version, "returned instance did not have version = " + this_version
            if _updated_still_new:
                assert res['state'] == 'new', "state not set to new"
            else:
                assert res['state'] == 'updated', "state not set to updated"
            assert res['q'] == j, "q was expecte to have value " + str(j)
    i = 0
    for nom in _update_initial_instances:
        pid = _update_initial_instances[nom]
        original_attribs = dkb.get(pid, only_these = ['n', 'version', 'state'])
        if _verbose:
            print('original_attribs = ' + str(original_attribs))
        assert original_attribs['n'] == 2**i, "Original value for n not retained"
        assert original_attribs['version'] == 'zero', "Original version not retained"
        assert original_attribs['state'] == 'new', "Original state not retained"
        i += 1
    return
    
def test_all():
    global dkb, _upper, _middle, _session_id
                # login to a DKB in its current state and start to use it
    _session_id = 'Malcolm.Atkinson@ed.ac.uk testing contexts at ' + str(datetime.datetime.now())
    dkb = DKB.login(_site_id,
            username = _user,
            session_id = _session_id ) 
    check_context_fns()
    check_context_exceptions()
    check_status()
    dkb.close()
    time.sleep(random.randint(0, 5))          # random pause to expose concurrency effects
                # login to a DKB in its current state and start to use it
    _session_id = 'Malcolm.Atkinson@ed.ac.uk testing concept functions at ' + str(datetime.datetime.now())
    dkb = DKB.login(_site_id,
            username = _user,
            session_id = _session_id )   
    dkb.leave() # disconnect from the state of previous runs  
    # work in _the_context, with related contexts in its search_path
    set_context()  # set the name of the lowest context for most specialised concepts and instances of all
    _upper = dkb.new_context( _the_context + '_upper', 'To test dkb.get run number ' + str(_context_try) )
    dkb.enter(_upper, 'W')
    _middle = dkb.new_context( _the_context + '_middle', 'To test dkb.get run number ' + str(_context_try) )
    dkb.enter(_middle, 'W')
    con = dkb.new_context( _the_context, 'To test dkb.get run number ' + str(_context_try) )
    create_concepts()
    if False:
        current_context = dkb.context_status()
        print('current context')
        print_context_status(current_context)
        middle_context = dkb.context_status(_middle)
        print('middle context')
        print_context_status(middle_context)
        upper_context = dkb.context_status(_upper)
        print('upper context')
        print_context_status(upper_context)
    get_those_concepts()
    make_Nil()
    create_instances()
    if False:
        current_context = dkb.context_status()
        print('\n        current context')
        print_context_status(current_context)
    get_those_instances()
    find_those_instances_and_concepts()
    try_update()
    dkb.close()
    time.sleep(random.randint(0, 10))          # random pause to expose concurrency effects
    _session_id = 'Malcolm.Atkinson@ed.ac.uk re-testing after dkb.close at ' + str(datetime.datetime.now())
    dkb = DKB.login(_site_id,
            username = _user,
            session_id = _session_id ) 
    dkb.enter(_the_context, 'R')
    if False:
        current_status = dkb.context_status()
        print_context_status(current_context)
    get_those_concepts()
    dkb.enter(_concurrency_pad + 'instances', 'R')
    if False:
        current_status = dkb.context_status()
        print_context_status(current_context)
    get_those_instances()
    find_those_instances_and_concepts()

def test_exceptions():       # These exception names need correcting
    with pytest.raises(Exceptions.InstanceNotFoundError):
        err1 = dkb.get( 'CAZZZZZZ')	                  # unknown name
    with pytest.raises(Exceptions.InstanceNotFoundError):
        err1 = dkb.get( _the_context + ':CAZZZZZZ')	  # unknown name
    with pytest.raises(Exceptions.InstanceNotFoundError):
        CA2 = dkb.get( 'ex3:' + _the_context + ':1299:CA')	  # PID to a non-existent entry
    dkb.close()
    with pytest.raises(Exceptions.DKBclosedError):
        CA2 = dkb.get( 'CA')
                                                      # finished this get test
    
            
    
    