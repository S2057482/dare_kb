# Ex3 pytsest test functions 
# Test retention of info left by previous Context test runs
 
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import datetime
from typing import List

dkb = None


def eqList(list1: list,list2: list) -> bool:
    'Return True iff the two arguments are identical lists'
    assert isinstance(list1,list), 'value returned by getSearchpath is not a list'
    if not isinstance(list1,list):
       return False
    assert len(list1) == len(list2),\
        'search path not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if len(list1) != len(list2):
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        assert isinstance(elem1, str),\
            'search path contains a non-string: ' + str(elem1)
        if not isinstance(elem1, str):
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        assert elem1 == elem2,\
        	'Mismatch of orefixes at step ' + str(posn) +\
                        ' ' + elem2 + ' expected but ' + elem1 + ' found'
        if elem1 != elem2:
            return False
        posn += 1
    return True
       
    
    
def checkContext( prefix: str,            # a string identifying the Context
                  search_path: List[str] ):      # a list of strings expected
    global dkb
    dkb.enter(prefix, 'R')           # visit the context
    sp = dkb.get_search_path()
    assert eqList(sp, search_path), prefix + '''\'s search path not ''' + str(searchpath)
    
def test_retentionOfContextData():
    global dkb
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            username = 'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now())) 
      
    # left by newContext testing
    checkContext('mpa', ['kb'])
    checkContext('mal', ['mpa'])
    checkContext('malcolm', ['mpa', 'mal'])
    checkContext('malcolm2', ['mal'])
    
    # from getSearchpath testing
    checkContext('mpa2', ['mal7'])
    checkContext('mal2', ['mpa2'])
    checkContext('malcolm3', ['mpa2', 'mal2'])
    
    # from setSearchpath testing
    checkContext('mpa4', ['malcolm4'])
    checkContext('mal4', ['kb', 'malcolm4'])
    checkContext('malcolm4', [])
    
    # from enter testing
    checkContext('mpa5', ['malcolm5'])
    checkContext('mal5', ['mpa5'])
    checkContext('malcolm5', ['mpa5', 'mal5'])
    
    # from leave testing
    checkContext('mpa7', ['mpa'])
    checkContext('mal7', ['kb'])    
      
    dkb.close()	#finished this test
        

