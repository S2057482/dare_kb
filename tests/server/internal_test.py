import pytest

from dare_kb.server import internal

@pytest.mark.parametrize('identi', [
    'a',
    'aa',
    'aa1',
    'aa123',
    'aa123a',
    '測試'  # "test"
    ])
def test_name_spec_identifiers_legal(identi):
    assert internal.name_spec.my_regex_for_identifiers(identi)

@pytest.mark.parametrize('identi', [
    '*',
    'a*d',
    '2',
    ])
def test_name_spec_identifiers_illegal(identi):
    assert not internal.name_spec.my_regex_for_identifiers(identi)
