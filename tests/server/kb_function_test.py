from create_kb_test import (
        KB,
        test_new_darekb_initialisation as initialise_kb,
        )

@KB
def test_new_darekb_default_concept(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    assert dkb.resolve('Concept')
    root_concept = dkb.get_concept('Concept')
    assert root_concept

@KB
def test_darekb_new_context(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    dkb.new_context('ran', 'RandomContext', ['kb'])
    return dkb

@KB
def test_darekb_new_concept(tmp_path, datafiles):
    dkb = test_darekb_new_context(tmp_path, datafiles)
    # dkb.enter('ran', 'W')
    second = dkb.new_concept('ran', 'SecondConcept')
    dkb.new_concept('ran', 'ThirdConcept', 'SecondConcept')
    dkb.new_concept('ran', 'FourthConcept', required={'myprop': 'Number'})
    concept3 = dkb.get_concept('ThirdConcept', base='ran')
    assert concept3
    assert concept3.label == 'ThirdConcept'
    assert concept3.sub_class == dkb.resolve('SecondConcept', base='ran')[0]

@KB
def test_darekb_new_instance(tmp_path, datafiles):
    dkb = test_darekb_new_context(tmp_path, datafiles)
    # dkb.enter('ran', 'W')
    second = dkb.new_concept('ran', 'SecondConcept')
    inst_pid = dkb.new_inst(
            'ran',
            'SecondConcept',
            'SecondConceptInst',
            )
    inst = dkb.get_instance('ran', 'SecondConceptInst')
    assert inst

