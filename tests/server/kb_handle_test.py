from create_kb_test import (
        KB,
        test_new_darekb_initialisation as initialise_kb,
        )

from dare_kb.server.session import Session

@KB
def init_kb_handle(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    session = Session('zr')
    dkb_handle = dkb.new_handle(session)
    assert dkb_handle
    return dkb, dkb_handle

@KB
def test_dkb_handle_default_concept(tmp_path, datafiles):
    _, dkb_handle = init_kb_handle(tmp_path, datafiles)
    root_concept = dkb_handle.get_concept('Concept')
    assert root_concept

@KB
def test_dkb_handle_new_context(tmp_path, datafiles):
    _, dkb_handle = init_kb_handle(tmp_path, datafiles)
    dkb_handle.new_context('ran', 'RandomContext')

@KB
def test_dkb_handle_new_concept(tmp_path, datafiles):
    dkb, dkb_handle = init_kb_handle(tmp_path, datafiles)
    dkb_handle.new_context('ran', 'RandomContext', ['kb'])
    dkb_handle.enter('ran', 'W')
    second = dkb_handle.new_concept('SecondConcept')
    dkb_handle.new_concept('ThirdConcept', 'SecondConcept')
    dkb_handle.new_concept('FourthConcept', required={'myprop': 'Number'})
    concept3 = dkb_handle.get_concept('ThirdConcept')
    assert concept3
    assert concept3.label == 'ThirdConcept'
    assert concept3.sub_class == dkb.resolve('SecondConcept', 'ran')[0]

@KB
def test_darekb_new_instance(tmp_path, datafiles):
    _, dkb_handle = init_kb_handle(tmp_path, datafiles)
    dkb_handle.new_context('ran', 'RandomContext')
    dkb_handle.enter('ran', 'W')
    second = dkb_handle.new_concept('SecondConcept')
    inst_pid = dkb_handle.new_inst(
            'SecondConcept',
            'SecondConceptInst',
            )
    inst = dkb_handle.get_instance('SecondConceptInst')
    assert inst

