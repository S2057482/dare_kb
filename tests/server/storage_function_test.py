from dare_kb.server.instance import Instance

from create_kb_test import (
        KB,
        test_new_darekb_initialisation as initialise_kb,
        )

@KB
def test_new_darekb_storage(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    assert dkb
    assert dkb._storage
    return dkb, dkb._storage

@KB
def test_new_instance(tmp_path, datafiles):
    dkb, storage = test_new_darekb_storage(tmp_path, datafiles)
    dkb.new_context('ran', 'RandomContext', ['kb'])
    second = dkb.new_concept('ran', 'SecondConcept')
    assert not storage.has_instance('ran', 'SecondConceptInst')
    inst = dkb.new_instance(name='SecondConceptInst', prefix='ran', cls='SecondConcept')
    assert storage.has_instance('ran', 'SecondConceptInst')
    inst2 = storage.get_instance('ran', 'SecondConceptInst')
    assert inst2
    assert inst2.name == 'SecondConceptInst'
    assert inst2.prefix == 'ran'
    assert inst2.cls == second

